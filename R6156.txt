       01  R6156-PRSN-PRVNM.
           02  DB-PROC-ID-6156.
               03  USER-CODE-6156                       PIC X(8).
               03  LAST-ACTVY-DATE-6156                 PIC X(5).
               03  TRMNL-ID-6156                        PIC X(8).
               03  PURGE-FLAG-6156                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  PRSN-NAME-KEY-6156.
               03  UNVRS-CODE-6156                      PIC X(2).
               03  NAME-KEY-6156                        PIC X(35).
           02  NAME-CODE-6156                           PIC X(1).
           02  CHANGE-DATE-6156               COMP-3    PIC 9(8).
