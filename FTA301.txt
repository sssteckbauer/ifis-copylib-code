      *======================= START  FTA301 ======================*    01180010
      *         OUTSTANDING TRAVEL ADVANCES & DIRECT BILLINGS        *  01180010
      *==============================================================*  01180010
       01  FTA301-EXTRACT-O.                                            02360036
           03  FTA301-SORT-KEY.                                         02360036
               05  FTA301-FIXED-KEY.                                    02360036
                   07  FTA301-UNVRS-ID          PICTURE X(02).          02370036
                   07  FTA301-USER-ID           PICTURE X(08).          02390036
                   07  FTA301-REPORT-ID         PICTURE X(08).          02390036
                   07  FTA301-REQUEST-NO        PICTURE 9(04).          02380036
               05  FTA301-VARIABLE-KEY.                                 02400036
                   07  FTA301-ACCT-NAME-KEY     PICTURE X(35).          02360036
                   07  FTA301-TRVL-ACCT-PID.                            02380036
                       09  FTA301-TRVL-ACCT-DIGIT-ONE                   02380036
                                                PICTURE X(01).          02400036
                       09  FTA301-TRVL-ACCT-LAST-NINE                   02380036
                                                PICTURE X(09).          02400036
                   07  FTA301-EVENT-NMBR        PICTURE X(08).          02360036
                   07  FTA301-REC-NMBR          PICTURE X(01).          02360036
                   07  FTA301-SORT-NMBR         PICTURE X(08).          02360036
                   07  FTA301-SEQ-NMBR          PICTURE 9(04).          02360036
                   07  FILLER                   PICTURE X(01).          02360036
               05  FTA301-REC-TYPE              PICTURE X(01).          02400036
       03  FTA301-DATA.                                                 02410036
           05  FTA301-VARIABLE-DATA             PICTURE X(350).         02410036
           05  FTA301-REC-A      REDEFINES FTA301-VARIABLE-DATA.        02410036
               07  FTA301-INST-TITLE            PICTURE X(35).
               07  FTA301-AS-OF-DATE            PICTURE X(08).
               07  FTA301-USER-NAME             PICTURE X(35).
               07  FILLER                       PICTURE X(272).
           05  FTA301-REC-B      REDEFINES FTA301-VARIABLE-DATA.        02410036
               07  FTA301-NAME-KEY              PICTURE X(35).          02410036
               07  FTA301-RCNCLTN-DUE-DATE      PICTURE X(08).
               07  FTA301-EVENT-BAL             PICTURE S9(10)V99.
               07  FILLER                       PICTURE X(295).
       01  FTA301-COUNT                         PICTURE S9(08).         02360036
      *======================= END    FTA301 ======================*    01180010
