      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM02                              04/24/00  12:38  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM02.
           02  ACTN-CODE-PU02                           PIC X(1).
           02  HDR-ERROR-IND-4070-PU02                  PIC X(1).
           02  HDR-CNTR-DTL-4070-PU02                   PIC X(4).
           02  HDR-CNTR-ACCT-4070-PU02                  PIC X(4).
           02  TRANS-DATE-4070-PU02                     PIC X(6).
           02  ACTVY-DATE-4070-PU02                     PIC X(6).
           02  RQST-NAME-4070-PU02                      PIC X(35).
           02  TLPHN-ID-4070-PU02.
               03  BASIC-TLPHN-ID-4070-PU02.
                   04  TLPHN-AREA-CODE-4070-PU02        PIC X(3).
                   04  TLPHN-XCHNG-ID-4070-PU02         PIC X(3).
                   04  TLPHN-SEQ-ID-4070-PU02           PIC X(4).
               03  TLPHN-XTNSN-ID-4070-PU02             PIC X(4).
           02  EMAIL-ADR-4070-PU02                      PIC X(20).
           02  COA-CODE-4070-PU02                       PIC X(1).
           02  ORGZN-CODE-4070-PU02                     PIC X(6).
           02  ORGZN-TITLE-4010-PU02                    PIC X(35).
           02  VNDR-CODE-4073-PU02.
               03  VNDR-ID-DIGIT-ONE-PU02               PIC X(1).
               03  VNDR-ID-LAST-NINE-PU02               PIC X(9).
           02  VNDR-NAME-KEY-PU02                       PIC X(35).
           02  PREV-PO-NBR-4070-PU02                    PIC X(8).
           02  FAX-TLPHN-ID-4070-PU02.
               03  BASIC-FAX-TLPHN-ID-4070-PU02.
                   04  FAX-TLPHN-AREA-CODE-4070-PU02    PIC X(3).
                   04  FAX-TLPHN-XCHNG-ID-4070-PU02     PIC X(3).
                   04  FAX-TLPHN-SEQ-ID-4070-PU02       PIC X(4).
           02  VDR-EMAIL-ADR-4070-PU02                  PIC X(37).
           02  CNCL-IND-4070-PU02                       PIC X(1).
           02  DLVRY-DATE-4070-PU02                     PIC X(6).
           02  RQST-TOTAL-4070-PU02                     PIC X(16).
           02  CNCL-DATE-4070-PU02                      PIC X(6).
           02  RQST-CMPLT-IND-4070-PU02                 PIC X(1).
           02  APRVL-IND-4070-PU02                      PIC X(1).
           02  PRINT-RQST-4070-PU02                     PIC X(1).
           02  PRINTER-PU02                             PIC X(4).
           02  TEXT-FLAG-PU02                           PIC X(1).
           02  MAILCODE-PU02                            PIC X(6).
           02  APRVL-TMPLT-CODE-PU02                    PIC X(3).
