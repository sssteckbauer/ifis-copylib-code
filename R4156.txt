       01  R4156-ADJMT.
           02  DB-PROC-ID-4156.
               03  USER-CODE-4156                       PIC X(8).
               03  LAST-ACTVY-DATE-4156                 PIC X(5).
               03  TRMNL-ID-4156                        PIC X(8).
               03  PURGE-FLAG-4156                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ADJMT-KEY-4156.
               03  UNVRS-CODE-4156                      PIC X(2).
               03  ADJMT-CODE-4156                      PIC X(2).
           02  ADJMT-DESC-4156                          PIC X(35).
           02  START-DATE-4156                COMP-3    PIC S9(8).
           02  END-DATE-4156                  COMP-3    PIC S9(8).
           02  ACTVY-DATE-4156                COMP-3    PIC S9(8).
