      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD24A                             04/24/00  13:01  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD24A.
           02  CHCK-DIGIT-ARGMNT-CH24.
               03  CHCK-BASE-NMBR-CH24                  PIC 9(10).
               03  CHCK-DIGIT-CH24                      PIC 9(1).
               03  CHCK-RTRN-CODE-CH24                  PIC X(1).
