      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXPIN                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   B. ORTIZ       02/25/98   MODIFIED FOR GFAU (REL027)   */
      * /*                                                          */
      * /************************************************************/  $
      *01  XPIN-PROVISION-INPUT.                                        CPWSXPIN
           05  XPIN-DOCUMENT-NO                PIC X(06).               CPWSXPIN
           05  XPIN-ACTION-CODE                PIC X(01).               CPWSXPIN
      *                                                     * = DELETE  CPWSXPIN
      *                                                     A = ADD     CPWSXPIN
      *                                                     C = CHANGE  CPWSXPIN
           05  XPIN-KEY.                                                CPWSXPIN
      ****     10  XPIN-LOCATION               PIC X(02).               32020027
      ****     10  XPIN-SAU                    PIC X(01).               32020027
      ****     10  XPIN-SUB-CAMPUS             PIC X(01).               32020027
      ****     10  XPIN-ACCOUNT                PIC X(07).               32020027
      ****     10  XPIN-FUND                   PIC X(06).               32020027
      ****     10  XPIN-SUB-BUDGET             PIC X(02).               32020027
               10  XPIN-FAU                    PIC X(31).               32020027
               10  XPIN-TITLE                  PIC X(04).               CPWSXPIN
               10  FILLER                      PIC X(01).               CPWSXPIN
               10  XPIN-PROVISION-NO           PIC X(06).               CPWSXPIN
           05  XPIN-PROVISION-TYPE             PIC X(01).               CPWSXPIN
           05  XPIN-PROVISION-DESCR            PIC X(30).               CPWSXPIN
           05  XPIN-EFFECTIVE-DATE.                                     CPWSXPIN
               10  XPIN-EFF-MONTH              PIC X(02).               CPWSXPIN
               10  XPIN-EFF-DAY                PIC X(02).               CPWSXPIN
               10  XPIN-EFF-YEAR               PIC X(02).               CPWSXPIN
           05  XPIN-MONTHLY-RATE               PIC 9(05)V99.            CPWSXPIN
           05  XPIN-MONTHLY-RATE-CHAR  REDEFINES                        CPWSXPIN
               XPIN-MONTHLY-RATE               PIC X(07).               CPWSXPIN
           05  XPIN-MONTHLY-RATE-SIGN          PIC X(01).               CPWSXPIN
           05  XPIN-FTE                        PIC 9(04)V99.            CPWSXPIN
           05  XPIN-FTE-CHAR           REDEFINES                        CPWSXPIN
               XPIN-FTE                        PIC X(06).               CPWSXPIN
           05  XPIN-FTE-SIGN                   PIC X(01).               CPWSXPIN
           05  XPIN-ANNUAL-AMT                 PIC 9(07).               CPWSXPIN
           05  XPIN-ANNUAL-AMT-CHAR    REDEFINES                        CPWSXPIN
               XPIN-ANNUAL-AMT                 PIC X(07).               CPWSXPIN
           05  XPIN-ANNUAL-AMT-SIGN            PIC X(01).               CPWSXPIN
           05  XPIN-PERB-CODE                  PIC X(02).               $
           05  XPIN-TITLE-SPECL-HANDLING-CODE  PIC X(01).               $
           05  XPIN-APPT-REPRESENTATION-CODE   PIC X(01).               $
           05  XPIN-RANGE-ADJ-DISTR-UNIT-CODE  PIC X(01).               $
           05  FILLER                          PIC X(18).               $
           05  XPIN-IFIS-FUND                  PIC X(06).               CPWSXPIN
           05  XPIN-IFIS-ORG                   PIC X(06).               CPWSXPIN
           05  XPIN-IFIS-ACCOUNT               PIC X(06).               CPWSXPIN
           05  XPIN-IFIS-PROGRAM               PIC X(06).               CPWSXPIN
           05  XPIN-IFIS-ACTIVITY              PIC X(06).               CPWSXPIN
           05  XPIN-IFIS-LOCATION              PIC X(06).               CPWSXPIN
