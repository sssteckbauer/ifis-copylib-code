       01  R4095-RCVNG-DTL.
           02  DB-PROC-ID-4095.
               03  USER-CODE-4095                       PIC X(8).
               03  LAST-ACTVY-DATE-4095                 PIC X(5).
               03  TRMNL-ID-4095                        PIC X(8).
               03  PURGE-FLAG-4095                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  QTY-RCVD-4095                  COMP-3    PIC 9(6)V99.
           02  QTY-RJCTD-4095                 COMP-3    PIC 9(6)V99.
           02  ITEM-NMBR-4095                 COMP-3    PIC 9(4).
           02  CMDTY-CODE-4095                          PIC X(8).
           02  CMDTY-DESC-4095                          PIC X(35).
