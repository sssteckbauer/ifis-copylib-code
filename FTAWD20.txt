      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD20                              04/24/00  13:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD20.
           02  EVENT-BAL-TA20                 COMP-3    PIC S9(10)V99.
           02  SAVE-DBKEY-ID-4404-TA20
                                            OCCURS 5    PIC  X(8).
           02  SAVE-DBKEY-ID-4405-TA20
                                            OCCURS 5    PIC  X(8).
           02  SAVE-DBKEY-ID-4411-TA20
                                            OCCURS 5    PIC  X(11).
           02  SAVE-DBKEY-ID-4400-TA20                  PIC  X(8).
           02  SAVE-DBKEY-ID-4401-TA20                  PIC  X(8).
           02  PAGE-NO-4404-TA20              COMP-3    PIC 9(3).
           02  PAGE-NO-4405-TA20              COMP-3    PIC 9(3).
           02  PAGE-NO-4411-TA20              COMP-3    PIC 9(3).
           02  PAGE-NO-TA20                   COMP-3    PIC 9(3).
           02  BACK-SUB-4404-TA20             COMP-3    PIC 9(3).
           02  BACK-SUB-4405-TA20             COMP-3    PIC 9(3).
           02  BACK-SUB-4411-TA20             COMP-3    PIC 9(3).
