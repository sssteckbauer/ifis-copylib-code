      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM13                              04/24/00  12:48  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM13.
           02  LINE-ADRS-UT13 OCCURS 4.
               03  LINE-ADR-1-UT13                      PIC X(35).
               03  LINE-ADR-2-UT13                      PIC X(35).
               03  LINE-ADR-3-UT13                      PIC X(35).
               03  LINE-ADR-4-UT13                      PIC X(35).
           02  ADR-TYPE-CODE-UT13           OCCURS 4    PIC X(2).
           02  NAME-KEY-UT13                OCCURS 4    PIC X(35).
           02  ID-NMBR-UT13                 OCCURS 4    PIC X(9).
           02  ACTN-CODE-GROUP-UT13.
               03  ACTN-CODE-UT13           OCCURS 4    PIC X(1).
           02  SAVE-DBKEY-ID-UT13           OCCURS 4    PIC X(16).
           02  ACH-ADR-FLAG-UT13            OCCURS 4    PIC X(1).
           02  CITY-NAME-UT13               OCCURS 4    PIC X(18).
           02  STATE-CODE-UT13              OCCURS 4    PIC X(2).
           02  ZIP-CODE-UT13                OCCURS 4    PIC X(10).
