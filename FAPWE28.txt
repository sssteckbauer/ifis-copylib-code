      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWE28                              08/15/11         *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWE28.
           02 FAPWE28-SCREEN-KEY.
               03 SAVE-DBKEY-ID-AP28
                                                OCCURS 9 PIC X(59).
               03 ADD-FLAG-AP28                         PIC X(1).
               03 LISTVAL-FLAG-AP28                     PIC X(1).
               03 ACTN-ERR-FLAG-AP28                    PIC X(1).
               03 CONFIRM-DELETE-AP28                   PIC X(1).
           02  FAPWE28-SCROLL-KEY.
               03  SAVE-DBKEY-ID-PLUS-ONE-AP28.
                   05 SAVE-DBKEY-IREF-P1       PIC S9(7) COMP-3.        235 DOVH
                   05 SAVE-DBKEY-ENTPSN-IND-P1 PIC X(1).                235 DOVH
                   05 SAVE-DBKEY-DOC-P1        PIC X(8).                235 DOVH
                   05 SAVE-DBKEY-FROM-DT-P1    PIC X(10).
                   05 SAVE-DBKEY-TO-DT-P1      PIC X(10).
                   05 SAVE-DBKEY-FDR-SET-TS-P1 PIC X(26).
               03  SAVE-DBKEY-ID-MINUS-ONE-AP28.
                   05 SAVE-DBKEY-IREF-M1       PIC S9(7) COMP-3.        235 DOVH
                   05 SAVE-DBKEY-ENTPSN-IND-M1 PIC X(1).                235 DOVH
                   05 SAVE-DBKEY-DOC-M1        PIC X(8).                235 DOVH
                   05 SAVE-DBKEY-FROM-DT-M1    PIC X(10).
                   05 SAVE-DBKEY-TO-DT-M1      PIC X(10).
                   05 SAVE-DBKEY-FDR-SET-TS-M1 PIC X(26).
