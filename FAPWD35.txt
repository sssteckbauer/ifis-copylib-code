      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD35.                             04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD35.
           02  SAVE-DBKEYS.
               03 SAVE-DBKEY-ID-4157-AP35          PIC X(8).
               03 SAVE-DBKEY-ID-CRD-AP35.
                  05 SAVE-CRD-FK-CKR-CHECK-ISEQ    PIC S9(08) COMP-3.
                  05 SAVE-CRD-DOC-NBR              PIC X(08).
                  05 SAVE-CRD-ITEM-NBR             PIC S9(04) COMP-3.
                  05 SAVE-CRD-SEQ-NBR              PIC S9(04) COMP-3.
                  05 SAVED-CRD-CKR-SET-TS          PIC X(26).
               03 SAVE-LINE-ITEM-KEY-AP35 OCCURS 5 TIMES   PIC X(45).
           02  SYSTEM-DATE-AP35               COMP-3       PIC S9(8).
           02  WORK-FIELDS.
               03 CHECK-NMBR-AP35.
                  05 CHECK-NMBR-1ST-ONE-AP35            PIC X(01).
                  05 CHECK-NMBR-LAST-7-AP35             PIC X(07).
               03 START-DATE-AP35             COMP-3    PIC S9(8).
               03 TRANS-DATE-AP35             COMP-3    PIC S9(8).
               03 TRANS-DATE-DB2-FORMAT                 PIC X(10).
               03 SYSTEM-DATE-DB2-FORMAT                PIC X(10).
               03 WS-EDIT-JRNL-IND                      PIC X(1).
               03 REQD-FLD-ERR-AP35                     PIC X(1).
               03 REFRESH-RULE-CLASS-SW                 PIC X(1).
               03 FATAL-ERROR-FLAG.
                  05 ERR-FLAG-AP35       OCCURS 5 TIMES PIC X(1).
               03 ACCT-WARNING-FLAGS.
                  05 ACCT-WARNING-IND-AP35 OCCURS 5 TIMES PIC X(1).
               03 FUND-WARNING-FLAGS.
                  05 FUND-WARNING-IND-AP35 OCCURS 5 TIMES PIC X(1).
               03 TRAN-AMT-NUM-AP35        OCCURS 5 TIMES PIC 9(10)V99.
               03 ITEM-NMBR-NUM-AP35       OCCURS 5 TIMES PIC 9(04).
               03 SEQ-NMBR-NUM-AP35        OCCURS 5 TIMES PIC 9(04).
               03 TMPLT-CHG-FLAG-AP35                   PIC X(1).
               03 FSCL-YR-ENDING-FLAG-AP35              PIC X(1).
               03 PAGE-STATUS-CODE-AP35                 PIC X(1).
               03 WORK-AMT-AP35               COMP-3    PIC S9(10)V99.
               03 WARNING-IND-AP35                      PIC X(1).
               03 WARNING-IND-2-AP35                    PIC X(1).
               03 VNDR-INV-ERR-IND-AP35                 PIC X(1).
               03 ERR-IND-AP35                          PIC X(1).
               03 WARNING-IND-AP35-R                    PIC X(1).
               03 WARNING-MSG-AP35          OCCURS 2    PIC 9(6).
               03 WORK-TEXT-ENTY-CODE-AP35.
                   04 WORK-DCMNT-NMBR-4120-AP35         PIC X(8).
                   04 WORK-TEXT-TYPE-AP35               PIC X(7).
               03 NUMERIC-8-AP35                        PIC 9(8).
               03 ITEM-NMBR-4151-AP35                   PIC 9(4).
               03 SEQ-NMBR-4152-AP35                    PIC 9(4).
               03  FSCL-YR-AP35                         PIC X(2).
               03  ACTG-PRD-AP35                        PIC 9(2).
