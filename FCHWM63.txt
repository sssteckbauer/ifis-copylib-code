      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM63                              04/24/00  12:18  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM63.
           02  ACTN-CODE-GROUP-CH63.
               03  ACTN-CODE-CH63           OCCURS 11   PIC X(1).
           02  COA-CODE-4001-CH63           OCCURS 11   PIC X(1).
           02  FUND-CODE-4001-CH63          OCCURS 11   PIC X(6).
           02  FUND-TITLE-4005-CH63         OCCURS 11   PIC X(35).
           02  STATUS-4005-CH63             OCCURS 11   PIC X(1).
           02  START-DATE-4005-CH63         OCCURS 11   PIC X(6).
           02  END-DATE-4005-CH63           OCCURS 11   PIC X(6).
