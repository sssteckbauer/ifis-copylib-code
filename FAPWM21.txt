      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM21                              04/24/00  12:06  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM21.
           02  SEL-IND-AP21                             PIC X(1).
           02  ACTN-CODE-GROUP-AP21.
               03  ACTN-CODE-AP21           OCCURS 11   PIC X(1).
           02  DCMNT-NMBR-4150-AP21         OCCURS 11   PIC X(8).
           02  APRVD-AMT-4150-AP21          OCCURS 11   PIC X(12).
           02  INVD-AMT-4150-AP21           OCCURS 11   PIC X(12).
           02  LAST-ACTVY-DATE-4150-AP21    OCCURS 11   PIC X(6).
           02  APRVL-IND-4150-AP21          OCCURS 11   PIC X(1).
           02  CRDT-MEMO-4150-AP21          OCCURS 11   PIC X(1).
           02  OPEN-PAID-IND-4150-AP21      OCCURS 11   PIC X(1).
