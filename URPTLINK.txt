000100*----------------------------------------------------------------*00010000
000200*    LAYOUT FOR: URPTLINK - INSTITUTION CODE AND DICTIONARY NAME *00020000
000300*                           PASSED IN LINKAGE BY FIMS BATCH      *00030000
000400*                           REPORT PROGRAMS.                     *00040000
000410*  04/13/93 - NO LONGER PASSED IN LINKAGE FROM OBSOLETE DRIVERS  *00041000
000500*----------------------------------------------------------------*00050000
000600 01  RPT-LINK.                                                    00060000
000700     03  RPT-INST-CODE                   PICTURE X(02).           00070000
000800     03  RPT-DICT-NAME                   PICTURE X(08).           00080000
