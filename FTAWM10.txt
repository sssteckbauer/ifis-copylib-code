      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWM10                              04/24/00  12:47  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWM10.
           02  ACTN-CODE-TA10                           PIC X.
           02  NAME-KEY-6117-TA10                       PIC X(35).
           02  MSG-DESC-TA10                            PIC X(17).
           02  EMPLY-IND-4400-TA10                      PIC X(1).
           02  DELETE-LIT-TA10                          PIC X(8).
           02  CONFIRM-DELETE-TA10                      PIC X(1).
           02  CHECK-LIT-TA10                           PIC X(7).
           02  CONFIRM-CHECK-TA10                       PIC X(1).
           02  EVENT-DESC-4401-TA10                     PIC X(35).
           02  EVENT-BAL-TA10                           PIC X(10).
           02  RCNCLTN-DUE-DATE-4401-TA10               PIC X(8).
           02  RULE-CLASS-CODE-4406-TA10                PIC X(4).
           02  RCNCLTN-DATE-4406-TA10                   PIC X(8).
           02  RCNCLTN-STATUS-4406-TA10                 PIC X(1).
           02  RCNCLTN-STATUS-DESC-TA10                 PIC X(10).
           02  BANK-ACCT-CODE-4406-TA10                 PIC X(2).
           02  RCNCLTN-AMT-4406-TA10                    PIC X(10).
           02  CHECK-NMBR-4406-TA10                     PIC X(8).
           02  RCNCLTN-TYPE-4415-TA10                   PIC X(4).
           02  SHORT-DESC-4415-TA10                     PIC X(10).
           02  CHECK-DATE-4406-TA10                     PIC X(8).
           02  COA-CODE-4406-TA10                       PIC X(1).
           02  ACCT-INDX-CODE-4406-TA10                 PIC X(10).
           02  FUND-CODE-4406-TA10                      PIC X(6).
           02  ORGZN-CODE-4406-TA10                     PIC X(6).
           02  ACCT-CODE-4406-TA10                      PIC X(6).
           02  PRGRM-CODE-4406-TA10                     PIC X(6).
           02  ACTVY-CODE-4406-TA10                     PIC X(6).
           02  LCTN-CODE-4406-TA10                      PIC X(6).
           02  ADR-TYPE-CODE-6139-TA10                  PIC X(2).
           02  ACH-ADR-TYPE-TA10                        PIC X(2).
           02  SHORT-DESC-6137-TA10                     PIC X(10).
           02  CMPLT-IND-4406-TA10                      PIC X(1).
           02  LINE-ADR-6139-TA10           OCCURS 4    PIC X(35).
           02  APRVL-IND-4406-TA10                      PIC X(1).
           02  CNCL-IND-4406-TA10                       PIC X(1).
           02  CNCL-DATE-4406-TA10                      PIC X(8).
           02  HDR-ERROR-IND-4406-TA10                  PIC X(1).
           02  CITY-NAME-6139-TA10                      PIC X(18).
           02  STATE-CODE-6151-TA10                     PIC X(2).
           02  ZIP-CODE-6152-TA10                       PIC X(10).
           02  CNTRY-CODE-6153-TA10                     PIC X(2).
           02  EVENT-BAL-DESC-TA10                      PIC X(24).
           02  APRVL-TMPLT-CODE-TA10                    PIC X(3).
           02  CHECK-TEXT-IND-TA10                      PIC X(1).
           02  DOCT-TEXT-IND-TA10                       PIC X(1).
