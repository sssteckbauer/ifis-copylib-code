      ******************************************************************
      *    **** COPYBOOK WUTI01L - USED FOR LOADTXD WEB SERVICE ****   *
      *                                                                *
      *    WEB/CICS COMMAREA TO LOAD TXD_TXT_DTL TABLE FROM SCIQUEST   *
      *    AND ALSO LOAD THE TXH_TXT_HDR TABLE                         *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      ******************************************************************

       01  LOADTXD-PARM-AREA.

         02  LOADTXD-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  LOADTXD-CHANNEL-ID             PIC X(08).
      *        *********************************************************
      *        * MUST BE 'LOADTXD '                                    *
      *        *********************************************************

           05  LOADTXD-VERS-RESP-CD           PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WFUT(WEB/IFIS UT SYSTEM)*
      *        *********************************************************
               88  LOADTXD-RESPONSE-SUCCESSFUL         VALUE 'WFUT000I'.
               88  LOADTXD-RESPONSE-ERROR              VALUE 'WFUT999E'.
           05  LOADTXD-SERVICE-NAME           PIC X(08).
           05  LOADTXD-USER-CD                PIC X(08).

         02  LOADTXD-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

           05  LOADTXD-FUNCTION               PIC X(08).
      *        *********************************************************
      *        * ADD      = INSERT TXD ROW(S) AND TXH ROW              *
      *        * CHGTEXT  = UPDATE TXD (DELETE AND RE-ADD)             *
      *        *********************************************************

NOSEND*****05  LOADTXD-USER-CD                PIC X(08).
NOSEND*****05  LOADTXD-LAST-ACTVY-DT          PIC X(05).
           05  LOADTXD-CLAUSE-CD              PIC X(08).
           05  LOADTXD-PRINT-FLAG             PIC X(01).
           05  LOADTXD-DOC-TYP-SEQ-NBR        PIC +9(4).
           05  LOADTXD-TEXT-ENTY-CD           PIC X(15).
           05  LOADTXD-CHNG-SEQ-NBR           PIC X(03).
           05  LOADTXD-ITEM-NBR               PIC +9(4).
NOSEND*****05  LOADTXD-TXH-SET-TS             PIC X(26).
           05  LOADTXD-CMNT-TEXT              PIC X(9999).

         02  LOADTXD-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  LOADTXD-RETURN-STATUS-CODE     PIC X(01).
      *        *********************************************************
      *        * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR    *
      *        *********************************************************

           05  LOADTXD-RETURN-MSG-COUNT       PIC 9(01).
      *        *********************************************************
      *        * 1 = NUMBER OF RETURN-MSG-AREA(S)                      *
      *        *********************************************************

           05  LOADTXD-RETURN-MSG-AREA.
      *        *********************************************************
      *        * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT         *
      *        * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT        *
      *        * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH       *
      *        *                 PARAGRAPH NAME, & ASSOC DB2 TABLE     *
      *        *********************************************************
               10  LOADTXD-RETURN-MSG-NUM     PIC X(06).
               10  LOADTXD-RETURN-MSG-FIELD   PIC X(30).
               10  LOADTXD-RETURN-MSG-TEXT    PIC X(40).

