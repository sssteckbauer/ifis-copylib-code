      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWD50                              04/24/00  13:12  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWD50.
           02  TOTAL-PO-AMT-WK-PU50           COMP-3    PIC S9(10)V99.
           02  TOTAL-INV-AMT-WK-PU50          COMP-3    PIC S9(10)V99.
           02  INV-AMT-WK-PU50                COMP-3    PIC S9(10)V99.
           02  INV-TAX-AMT-WK-PU50            COMP-3    PIC S9(10)V99.
           02  PO-TAX-AMT-WK-PU50             COMP-3    PIC S9(10)V99.
           02  INV-ADDL-AMT-WK-PU50           COMP-3    PIC S9(10)V99.
           02  INV-DSCNT-AMT-WK-PU50          COMP-3    PIC S9(10)V99.
DEVCGI     02  INV-TRD-IN-AMT-WK-PU50         COMP-3    PIC S9(10)V99.
