      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK22                              04/24/00  12:23  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK22.
           02  COA-CODE-4000-CH22                       PIC X(1).
           02  XTRNL-ENTY-CODE-4052-CH22                PIC X(4).
           02  INTRL-ENTY-CODE-4053-CH22                PIC X(4).
           02  XTRNL-CODE-4054-CH22                     PIC X(10).
           02  INTRL-CODE-4055-CH22                     PIC X(10).
           02  START-DATE-4055-CH22                     PIC X(6).
