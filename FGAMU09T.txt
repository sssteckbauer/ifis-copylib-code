       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 11/09/00 at 16:00***

           02  FGAMU09-MAP-CONTROL.
           03  FGAMU09T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FGAMU09I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 8656.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 729.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-OPTN-GA091-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA091-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA091-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA091-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA091-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA091       PIC X(1)
                                               VALUE SPACES.
               10  WK01-OPTN-GA092-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA092-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA092-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA092-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA092-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA092       PIC X(1)
                                               VALUE SPACES.
               10  WK01-OPTN-GA093-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA093-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA093-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA093-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA093-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA093       PIC X(1)
                                               VALUE SPACES.
               10  WK01-OPTN-GA094-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA094-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA094-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA094-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA094-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-OPTN-GA094       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA09       PIC X(1)
                                               VALUE SPACES.
               10  WK01-YR-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-YR-GA09       PIC X(2)
                                               VALUE SPACES.
               10  WK01-PRD-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PRD-GA09       PIC X(2)
                                               VALUE SPACES.
               10  WK01-START-DATE-4024-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-4024-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-4024-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-4024-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-4024-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-START-DATE-4024-GA09       PIC X(6)
                                               VALUE SPACES.
               10  WK01-END-DATE-4024-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-4024-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-4024-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-4024-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-4024-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-END-DATE-4024-GA09       PIC X(6)
                                               VALUE SPACES.
               10  WK01-INDX-CODE-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-CODE-GA09       PIC X(10)
                                               VALUE SPACES.
               10  WK01-INDX-TITLE-4067-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-TITLE-4067-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-TITLE-4067-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-TITLE-4067-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-TITLE-4067-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-INDX-TITLE-4067-GA09       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-GA0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TITLE-4005-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4005-GA09       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-GA0B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0B       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TITLE-4010-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4010-GA09       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-GA0C-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0C-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0C-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0C-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0C-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0C       PIC X(6)
                                               VALUE SPACES.
               10  WK01-CODE-TITLE-4034-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TITLE-4034-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TITLE-4034-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TITLE-4034-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TITLE-4034-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TITLE-4034-GA09       PIC X(35)
                                               VALUE SPACES.
               10  WK01-CODE-GA0D-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0D-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0D-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0D-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0D-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-GA0D       PIC X(6)
                                               VALUE SPACES.
               10  WK01-TITLE-4105-GA09-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4105-GA09-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4105-GA09-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4105-GA09-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4105-GA09-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TITLE-4105-GA09       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
           04  WK01-SUBSCRIPTED-WORK-AREA.
               05  FILLER                      PIC X(1).
               05  WK01-OPTN-GA09-F             PIC X(1)
                                               OCCURS 4.
