      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXPV1                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   BROWN          08/15/85  MODIFIED FOR COLLECTIVE BARG. */  $
      * /*   B. ORTIZ       05/07/98  MODIFIED FOR GFAU (REL0027)   */  $
      * /************************************************************/  $
      *01  XPV1-PROVISION-SEGMENT.                                      CPWSXPV1
           05  XPV1-FILE-KEY.                                           CPWSXPV1
      ****     10  XPV1-LOCATION               PIC X(02).               3202RRRR
      ****     10  XPV1-SAU                    PIC X(01).               3202RRRR
      ****     10  XPV1-SUB-CAMPUS             PIC X(01).               3202RRRR
      ****     10  XPV1-ACCOUNT                PIC X(07).               3202RRRR
      ****     10  XPV1-FUND                   PIC X(06).               3202RRRR
      ****     10  XPV1-SUB-BUDGET             PIC X(02).               3202RRRR
001410         10  XPV1-FAU.                                            3202RRRR
001420             15  XPV1-LOCATION           PIC X(02).               CPWSXPV1
001430             15  XPV1-SAU                PIC X(01).               CPWSXPV1
001440             15  XPV1-SUB-CAMPUS         PIC X(01).               CPWSXPV1
001450             15  XPV1-ACCOUNT            PIC X(07).               3202RRRR
001460             15  XPV1-FUND               PIC X(06).               3202RRRR
001470             15  XPV1-SUB-BUDGET         PIC X(02).               3202RRRR
               10  XPV1-SEGMENT-SORT-TYPE      PIC X(01).               CPWSXPV1
           05  XPV1-SEGMENT-KEY.                                        CPWSXPV1
               10  XPV1-TITLE                  PIC X(04).               CPWSXPV1
               10  XPV1-SORT-CONSTANT          PIC X(01).               CPWSXPV1
      *                               ALWAYS HIGH-VALUES, TO SORT       CPWSXPV1
      *                               PROVISION RECS AFTER PAYROLL      CPWSXPV1
               10  XPV1-PROVISION-NO           PIC X(06).               CPWSXPV1
           05  FILLER                          PIC X(01).               CPWSXPV1
           05  XPV1-PROVISION-TYPE             PIC X(01).               CPWSXPV1
           05  XPV1-EFFECTIVE-DATE.                                     CPWSXPV1
               10  XPV1-EFFECTIVE-YEAR         PIC X(02).               CPWSXPV1
               10  XPV1-EFFECTIVE-MONTH        PIC X(02).               CPWSXPV1
               10  XPV1-EFFECTIVE-DAY          PIC X(02).               CPWSXPV1
           05  XPV1-PROVISION-DESCR            PIC X(30).               CPWSXPV1
           05  XPV1-PROVISION-RATE             PIC S9(5)V99.            CPWSXPV1
           05  XPV1-PROVISION-FTE              PIC S9(4)V99.            CPWSXPV1
           05  XPV1-PROVISION-AMT              PIC S9(7).               CPWSXPV1
           05  XPV1-PERB-CODE                  PIC X(02).               $
           05  XPV1-TITLE-SPECL-HANDLING-CODE  PIC X(01).               $
           05  XPV1-APPT-REPRESENTATION-CODE   PIC X(01).               $
           05  XPV1-RANGE-ADJ-DISTR-UNIT-CODE  PIC X(01).               $
           05  FILLER                          PIC X(20).               $PWSXPV1
           05  XPV1-PROVISION-SEGMENT-TYPE     PIC X(01).               CPWSXPV1
           05  XPV1-IFIS-IFOAPAL.
               10  XPV1-IFIS-INDEX             PIC X(10).
               10  XPV1-IFIS-FUND              PIC X(06).
               10  XPV1-IFIS-ORG               PIC X(06).
               10  XPV1-IFIS-ACCOUNT           PIC X(06).
               10  XPV1-IFIS-PROGRAM           PIC X(06).
               10  XPV1-IFIS-ACTIVITY          PIC X(06).
               10  XPV1-IFIS-LOCATION          PIC X(06).
           05  XPV1-IFIS-TITLES.
               10  XPV1-IFIS-INDEX-TITLE       PIC X(35).
               10  XPV1-IFIS-FUND-TITLE        PIC X(35).
               10  XPV1-IFIS-ORG-TITLE         PIC X(35).
               10  XPV1-IFIS-ACCOUNT-TITLE     PIC X(35).
               10  XPV1-IFIS-PROGRAM-TITLE     PIC X(35).
               10  XPV1-IFIS-ACTIVITY-TITLE    PIC X(35).
               10  XPV1-IFIS-LOCATION-TITLE    PIC X(35).
