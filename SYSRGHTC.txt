      ******************************************************************
      *    PARM AREA USED BY SUBROUTINE FPECURID TO RECEIVE AN INPUT   *
      *    ID AND RETURN THE CURRENT ID FOR THE PERSON OR ENTITY.      *
      *    THE INP-PE-IND MUST BE 'P' OR 'E' TO IDENTIFY THE TABLE     *
      *    LOOKUP PRIORITY SINCE THE ID COULD BE ON PPI, ENI, OR BOTH. *
      *    IF INP-PE-SPECIFIC IS 'Y', TABLE LOOKUP WILL OCCUR ONLY     *
      *    FOR THE TABLE IDENTIFIED IN INP-PE-IND.                     *
      *    THE ASSOCIATED CURRENT ID, P/E INDICATOR, AND THE LISTED    *
      *    FIELDS ARE RETURNED IN THE OUTPUT AREA.                     *
      ******************************************************************
       01  FPECURID-PARM-AREA                          VALUE SPACES.

           05  FPECURID-INPUT-PARMS.
               10  FPECURID-INP-ID-KEY.
                   15  FPECURID-INP-DGT-ONE PIC X(01).
                   15  FPECURID-INP-LST-NINE
                                            PIC X(09).
               10  FPECURID-INP-PE-IND      PIC X(01).
                 88  FPECURID-INP-PE-IS-PERSON         VALUE 'P'.
                 88  FPECURID-INP-PE-IS-ENTITY         VALUE 'E'.

               10  FPECURID-INP-PE-SPECIFIC PIC X(01).
                 88  FPECURID-INP-PE-IS-SPECIFIC       VALUE 'Y'.

           05  FPECURID-OUTPUT-PARMS.
               10  FPECURID-OUT-ID-KEY.
                   15  FPECURID-OUT-DGT-ONE PIC X(01).
                   15  FPECURID-OUT-LST-NINE
                                            PIC X(09).
               10  FPECURID-OUT-PE-IND      PIC X(01).
               10  FPECURID-OUT-FULL-NAME   PIC X(35).
               10  FPECURID-OUT-IREF-ID     PIC 9(07).
               10  FPECURID-OUT-SSN         PIC X(09).
               10  FPECURID-OUT-ALT-ID      PIC X(09).
               10  FPECURID-OUT-EMP-ID      PIC X(09).

               10  FPECURID-REC-COUNT       PIC 9(02).
               10  FPECURID-RETURN-STATUS   PIC X(01).
                 88  FPECURID-FOUND                    VALUE 'Y'.
                 88  FPECURID-NOTFOUND                 VALUE 'N'.
                 88  FPECURID-ERROR                    VALUE 'E'.
                 88  FPECURID-DUP                      VALUE 'D'.

               10  FPECURID-ERROR-MSG.
                   15 FPECURID-DUP-ERROR OCCURS 3 TIMES.
                      20 FPECURID-DUP-ID   PIC X(09).
                      20 FPECURID-DUP-NAME PIC X(35).
                      20 FPECURID-DUP-TYPE PIC X(03).
                      20 FILLER            PIC X(03).

       01  FPECURID-PROGRAM                 PIC X(08)  VALUE 'FPECURID'.
       01  FPECURID-DUMMY-COMMAREA          PIC X(01)  VALUE SPACES.

      ******************************************************************
      *    COMMON AREA USED TO RIGHT-JUSTIFY AND ZERO-FILL A PERSON    *
      *    ID (PID) OR ENTITY ID (EID), IDENTIFIED BY 'P' OR 'E' IN    *
      *    THE RIGHTMOST POSITION.                                     *
      ******************************************************************
       01  WK-SYS-ID-WORK-AREA.
           05  WK-SYS-ID-PASSED             PIC X(09).
           05  WK-SYS-ID-LST-NINE           PIC 9(09).
           05  WK-SYS-ID-LST-NINE-X REDEFINES
               WK-SYS-ID-LST-NINE.
               10  FILLER                   PIC X(08).
               10  WK-SYS-ID-LAST-BYTE      PIC X(01).
                 88  VALID-SYS-ID-SUFFIX               VALUES 'E'
                                                              'P'.
           05  WS-LEADING-SPACES            PIC S9(4) COMP.
           05  WS-LEADING-ZEROES            PIC S9(4) COMP.
           05  WS-ACCUM-SYS-ID-LNG          PIC S9(4) COMP.

      ******************************************************************
      *    COMMON AREA USED TO ADD THE LAST 4 DIGITS OF THE SSN OR     *
      *    FEIN-ID TO THE END OF 35 BYTE NAME LINE.                    *
      ******************************************************************
       01  WS-NAME-FIELD.
           03 WS-NAME-KEY             PIC X(28) VALUE SPACES.
           03 FILLER                  PIC X(01) VALUE SPACES.
           03 WS-NAME-KEY-FILL1       PIC X(01) VALUE '('.
           03 WS-ID-LAST-4            PIC X(04) VALUE SPACES.
           03 WS-NAME-KEY-FILL2       PIC X(01) VALUE ')'.

