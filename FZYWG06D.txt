      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FZYWG06D                             04/24/00  13:04  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FZYWG06D.
           02  CNTNT-VLTN-FLAG-SY06                     PIC X(1).
           02  CNTNT-ACCESS-FLAG-SY06                   PIC X(1).
           02  CNTNT-ELMNT-NAME-SY06                    PIC X(8).
           02  CNTNT-DATA-VALUE-SY06                    PIC X(8).
           02  DLG-CODE-SY06                            PIC X(8).
           02  SCRN-CODE-SY06                           PIC X(8).
           02  ADD-PRMSN-FLAG-SY06                      PIC X(1).
           02  CHNG-PRMSN-FLAG-SY06                     PIC X(1).
           02  DLT-PRMSN-FLAG-SY06                      PIC X(1).
