      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWK32                              04/24/00  12:32  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWK32.
           02  COA-CODE-GA32                            PIC X(1).
           02  FIMS-ENTY-CODE-GA32                      PIC X(8).
           02  ELMNT-NAME-GA32                          PIC X(30).
           02  RQST-LEVEL-NMBR-GA32                     PIC X(2).
           02  RQST-OPTN-1-CODE-GA32                    PIC X(8).
           02  RQST-OPTN-2-CODE-GA32                    PIC X(8).
           02  SET-SORT-KEY-4012-GA32.
               03  SORT-OPTN-1-CODE-GA32                PIC X(8).
               03  SORT-OPTN-2-CODE-GA32                PIC X(8).
               03  SORT-LEVEL-NMBR-GA32                 PIC 9(2).
DEVBWS*    02  SAVE-DBKEY-ID-4011-GA32        COMP      PIC S9(8).
DEVBWS     02  SAVE-DBKEY-ID-4011-GA32                  PIC X(38).
