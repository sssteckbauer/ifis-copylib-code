      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCOWG00D                             04/24/00  13:04  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCOWG00D.
           02  COMM-VIEW-6257-CO00                      PIC X(8).
           02  COMM-TYPE-6258-CO00                      PIC X(4).
           02  COMM-PLAN-CODE-6259-CO00                 PIC X(4).
           02  TEXT-CODE-6260-CO00                      PIC X(4).
           02  PARA-CODE-6268-CO00                      PIC X(4).
           02  OFC-CODE-2155-CO00                       PIC X(4).
           02  UNVRS-CODE-6001-CO00                     PIC X(2).
           02  CMBNTN-ID-CO00.
               03  CMBNTN-ID-DIGIT-ONE-CO00             PIC X(1).
               03  CMBNTN-ID-LAST-NINE-CO00             PIC X(9).
           02  MAP-DATE-CO00                            PIC X(6).
           02  REF-NUMBER-CO00                          PIC X(6).
           02  PRSN-ID-6157-CO00.
               03  PRSN-ID-DIGIT-ONE-6157-CO00          PIC X(1).
               03  PRSN-ID-LAST-NINE-6157-CO00          PIC X(9).
           02  ENTY-ID-6186-CO00.
               03  ENTY-ID-DIGIT-ONE-6186-CO00          PIC X(1).
               03  ENTY-ID-LAST-NINE-6186-CO00          PIC X(9).
