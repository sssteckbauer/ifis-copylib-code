      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM42                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM42.
           02  ACTN-CODE-GROUP-CH42.
               03  ACTN-CODE-CH42                       PIC X(1).
           02  NAME-KEY-6117-CH42                       PIC X(35).
           02  LINE-ADR-6139-CH42           OCCURS 4    PIC X(35).
           02  CITY-NAME-6139-CH42                      PIC X(18).
           02  STATE-CODE-6151-CH42                     PIC X(2).
           02  ZIP-CODE-6152-CH42                       PIC X(10).
           02  CNTRY-CODE-6153-CH42                     PIC X(2).
           02  TLPHN-ID-CH42.
               03  BASIC-TLPHN-ID-CH42.
                   04  TLPHN-AREA-CODE-CH42             PIC X(3).
                   04  TLPHN-XCHNG-ID-CH42              PIC X(3).
                   04  TLPHN-SEQ-ID-CH42                PIC X(4).
               03  TLPHN-XTNSN-ID-CH42                  PIC X(4).
           02  MAP-START-DATE-CH42                      PIC X(6).
           02  MAP-END-DATE-CH42                        PIC X(6).
           02  LONG-DESC-6137-CH42                      PIC X(30).
           02  END-DATE-6139-CH42                       PIC X(6).
