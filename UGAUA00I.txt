      *-------------------------------------------------------------------------
      *01  UGAUA00I-RECORD.                                             UGAUA00I
           03  UGAUA00I-SORT-KEY.                                       UGAUA00I
               05  UGAUA00I-SUBSYSTEM-ID        PIC X(08).              UGAUA00I
               05  UGAUA00I-UNVRS-CODE          PIC X(02).              UGAUA00I
               05  UGAUA00I-DCMNT-NMBR          PIC X(08).              UGAUA00I
               05  UGAUA00I-RCRD-TYPE           PIC X(01).              UGAUA00I
           03  UGAUA00I-DATA.                                           UGAUA00I
               05  UGAUA00I-VARIABLE-DATA       PIC X(200).             UGAUA00I
               05  UGAUA00I-HDR-DATA            REDEFINES               UGAUA00I
                   UGAUA00I-VARIABLE-DATA.                              UGAUA00I
                   07  UGAUA00I-DCMNT-DESC      PIC X(35).              UGAUA00I
                   07  UGAUA00I-TRANS-DATE      PIC 9(08).              UGAUA00I
                   07  UGAUA00I-TRANS-DATE-RDF  REDEFINES
                       UGAUA00I-TRANS-DATE.
                       09  UGAUA00I-TRANS-DATE-CC PIC 99.
                       09  UGAUA00I-TRANS-DATE-YY PIC 99.
                       09  UGAUA00I-TRANS-DATE-MM PIC 99.
                       09  UGAUA00I-TRANS-DATE-DD PIC 99.
                   07  UGAUA00I-DCMNT-AMT       PIC 9(10)V99.           UGAUA00I
                   07  UGAUA00I-GNRT-DCMNT-NMBR-IND                     UGAUA00I
                                                PIC X(01).              UGAUA00I
                   07  FILLER                   PIC X(144).             UGAUA00I
               05  UGAUA00I-DTL-DATA            REDEFINES               UGAUA00I
                   UGAUA00I-VARIABLE-DATA.                              UGAUA00I
                   07  UGAUA00I-SEQ-NMBR        PIC 9(04).              UGAUA00I
                   07  UGAUA00I-JRNL-TYPE       PIC X(04).              UGAUA00I
                   07  UGAUA00I-TRANS-AMT       PIC 9(10)V99.           UGAUA00I
                   07  UGAUA00I-TRANS-DESC      PIC X(35).              UGAUA00I
                   07  UGAUA00I-DEBIT-CRDT-IND  PIC X(01).              UGAUA00I
                   07  UGAUA00I-COA-CODE        PIC X(01).              UGAUA00I
                   07  UGAUA00I-FUND-CODE       PIC X(06).              UGAUA00I
                   07  UGAUA00I-ORGZN-CODE      PIC X(06).              UGAUA00I
                   07  UGAUA00I-ACCT-CODE       PIC X(06).              UGAUA00I
                   07  UGAUA00I-PRGRM-CODE      PIC X(06).              UGAUA00I
                   07  UGAUA00I-ACTVY-CODE      PIC X(06).              UGAUA00I
                   07  UGAUA00I-LCTN-CODE       PIC X(06).              UGAUA00I
                   07  UGAUA00I-ACCT-INDX-CODE  PIC X(10).              UGAUA00I
                   07  UGAUA00I-BANK-ACCT-CODE  PIC X(02).              UGAUA00I
                   07  UGAUA00I-DCMNT-REF-NMBR  PIC X(10).              UGAUA00I
                   07  UGAUA00I-VNDR-CODE       PIC X(09).              UGAUA00I
                   07  UGAUA00I-ENCMBRNC-NMBR   PIC X(08).              UGAUA00I
                   07  UGAUA00I-ENCMBRNC-TYPE   PIC X(01).              UGAUA00I
                   07  UGAUA00I-BDGT-DSPSN      PIC X(01).              UGAUA00I
                   07  UGAUA00I-CMTMNT-TYPE     PIC X(01).              UGAUA00I
                   07  UGAUA00I-CMTMNT-PCT-X    PIC X(06).              UGAUA00I
                   07  UGAUA00I-CMTMNT-PCT REDEFINES                    UGAUA00I
                       UGAUA00I-CMTMNT-PCT-X    PIC 9(03)V999.          UGAUA00I
                   07  UGAUA00I-DPST-NMBR       PIC X(08).              UGAUA00I
                   07  UGAUA00I-ENCMBRNC-ACTN-IND                       UGAUA00I
                                                PIC X(01).              UGAUA00I
                   07  UGAUA00I-PRJCT-CODE      PIC X(08).              UGAUA00I
                   07  UGAUA00I-DSTBN-PCT-X     PIC X(06).              UGAUA00I
                   07  UGAUA00I-DSTBN-PCT REDEFINES                     UGAUA00I
                       UGAUA00I-DSTBN-PCT-X     PIC 9(03)V999.          UGAUA00I
                   07  UGAUA00I-TIME-STAMP      PIC X(06).              UGAUA00I
                   07  UGAUA00I-ACRL-IND        PIC X(01).              UGAUA00I
                   07  UGAUA00I-NSF-OVRDE       PIC X(01).              UGAUA00I
                   07  UGAUA00I-BDGT-PRD        PIC X(02).              UGAUA00I
                   07  UGAUA00I-ENCMBRNC-ITEM-NMBR-X                    UGAUA00I
                                                PIC X(04).              UGAUA00I
                   07  UGAUA00I-ENCMBRNC-ITEM-NMBR REDEFINES            UGAUA00I
                       UGAUA00I-ENCMBRNC-ITEM-NMBR-X                    UGAUA00I
                                                PIC 9(04).              UGAUA00I
                   07  UGAUA00I-ENCMBRNC-SEQ-NMBR-X                     UGAUA00I
                                                PIC X(04).              UGAUA00I
                   07  UGAUA00I-ENCMBRNC-SEQ-NMBR REDEFINES             UGAUA00I
                       UGAUA00I-ENCMBRNC-SEQ-NMBR-X                     UGAUA00I
                                                PIC 9(04).              UGAUA00I
                   07  UGAUA00I-AR-RCRD-NMBR    PIC X(04).              UGAUA00I
                   07  UGAUA00I-ENC-DCMNT-TYPE-SEQ-X                    UGAUA00I
                                                PIC X(04).              UGAUA00I
                   07  UGAUA00I-ENC-DCMNT-TYPE-SEQ REDEFINES            UGAUA00I
                       UGAUA00I-ENC-DCMNT-TYPE-SEQ-X                    UGAUA00I
                                                PIC 9(04).              UGAUA00I
                   07  FILLER                   PIC X(10).              UGAUA00I
           03  FILLER                           PIC X(31).              UGAUA00I
      *-------------------------------------------------------------------------
