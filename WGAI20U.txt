      ******************************************************************
      *    **** COPYBOOK WGAI20U - USED FOR ACCTINDX WEB SERVICE ****  *
      *                                                                *
      *    WEB/CICS COMMAREA FOR ACCOUNT INDEX MAINTENANCE             *
      *                                                                *
      *    NOTE: THE FORMAT FOR ALL DATES IS YYYY-MM-DD OR MM/DD/YYYY  *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      ******************************************************************

       01  ACCTINDX-PARM-AREA.

           05  ACCTINDX-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

               10  ACCTINDX-CHANNEL-ID        PIC X(08).
      *            *****************************************************
      *            * MUST BE 'ACCTINDX'                                *
      *            *****************************************************

               10  ACCTINDX-VERS-RESP-CD      PIC X(08).
      *            *****************************************************
      *            * INCOMING: COMMAREA VERSION = 01.00.00             *
      *            * OUTGOING: RETURN RESPONSE FOR WFGA                *
      *            *                          (WEB/IFIS GA SUBSYSTEM)  *
      *            *****************************************************
                   88  ACCTINDX-RESPONSE-SUCCESSFUL    VALUE 'WFGA000I'.
                   88  ACCTINDX-RESPONSE-ERROR         VALUE 'WFGA999E'.

           05  ACCTINDX-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

               10  ACCTINDX-SOURCE-SYSTEM      PIC X(03).
      *            *****************************************************
      *            * WEB = CALLED FROM THE WEB                         *
      *            * FIS = CALLED FROM THE MAINFRAME IFIS SYSTEM       *
      *            *****************************************************

               10  ACCTINDX-FUNCTION           PIC X(08).
      *            *****************************************************
      *            * COPYINDX - COPY ACCT-INDX-CD TO NEW-ACCT-INDX-CD  *
      *            * UPDTITLE - UPDATE TITLE FOR ACCT-INDX-CD          *
      *            * UPDEIDTE - UPDATE EARLY-INCTV-DT FOR ACCT-INDX-CD *
      *            * REACTIVE - REACTIVATE THE ACCT-INDX-CD            *
      *            * VALINDEX - VALIDATE ACCT-INDX-CD/IFOAPAL DISTRIB. *
      *            *****************************************************

               10  ACCTINDX-USER-CD            PIC X(08).
      *            *****************************************************
      *            * REQUIRED FOR ALL UPDATE FUNCTIONS                 *
      *            *****************************************************

               10  ACCTINDX-ACCT-INDX-CD       PIC X(10).
      *            *****************************************************
      *            * REQUIRED FOR ALL FUNCTIONS                        *
      *            *****************************************************

               10  ACCTINDX-START-DT           PIC X(10).
      *            *****************************************************
      *            * OPTIONAL FOR ALL FUNCTIONS (DEFAULT CURRENT DATE) *
      *            *****************************************************

               10  ACCTINDX-NEW-ACCT-INDX-CD   PIC X(10).
      *            *****************************************************
      *            * REQUIRED FOR COPYINDX                             *
      *            *****************************************************

               10  ACCTINDX-ACCT-INDX-TTL      PIC X(35).
      *            *****************************************************
      *            * REQUIRED FOR COPYINDX, UPDTITLE                   *
      *            *****************************************************

               10  ACCTINDX-EARLY-INCTV-DT     PIC X(10).
      *            *****************************************************
      *            * OPTIONAL FOR COPYINDX (DEFAULT = HIGH DATE)       *
      *            * REQUIRED FOR UPDEIDTE                             *
      *            *****************************************************

               10  ACCTINDX-LOCATION           PIC X(06).
      *            *****************************************************
      *            * OPTIONAL FOR COPYINDX                             *
      *            *****************************************************

           05  ACCTINDX-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

               10  ACCTINDX-RETURN-STATUS-CODE PIC X(01).
      *            *****************************************************
      *            * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR*
      *            *****************************************************

               10  ACCTINDX-RETURN-MSG-AREA.
      *            *****************************************************
      *            * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT     *
      *            * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT    *
      *            * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH   *
      *            *                 PARAGRAPH NAME, & ASSOC DB2 TABLE *
      *            *****************************************************
                   15  ACCTINDX-RETURN-MSG-NUM PIC X(06).
                   15  ACCTINDX-RETURN-MSG-FIELD
                                               PIC X(30).
                   15  ACCTINDX-RETURN-MSG-TEXT
                                               PIC X(40).

