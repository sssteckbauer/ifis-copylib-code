      ******************************************************************
      *    COPYBOOK WUT010N - USED FOR WEB3270 NAVIGATION SERVICES     *
      ******************************************************************
      ******************************************************************
      * CHANGE LOG:                                                    *
FP6406*    MODIFIED BY ..... DEVMR0                                    *
FP6406*    DATE MODIFIED ... 10/20/14                                  *
FP6406*    MODIFICATION .... TO ACCOMODATE WUT011N COEUS4FUNDSETUP,    *
FP6406*                      INCREASE CA-SOAP-RESP-AREA-TEXT FROM 605  *
FP6406*                      TO 948 BYTES.                             *
      *                                                                *
HVW001*    MODIFIED BY ..... H.VANDERWEIT, JIRA#CARCT-350              *
HVW001*    DATE MODIFIED ... 12/08/17                                  *
HVW001*    MODIFICATION .... RECOMPILE-ONLY TO ACCOMODATE WUT011N      *
HVW001*                      COEUS4FUNDSETUP/CA-SOAP-RESP-AREA-TEXT    *
HVW001*                      LENGTH CHANGES                            *
      ******************************************************************

       01  WEB3270N-COMMAREA.

           05  CA-CHANNEL-ID           PIC X(08).
      *        *********************************************************
      *        * MUST BE 'WEB3270N '                                   *
      *        *********************************************************

           05  CA-VERS-RESP-CD         PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WWUT (WEB UT SYSTEM)    *
      *        *********************************************************
               88  CA-RESPONSE-SUCCESSFUL              VALUE 'WWUT010I'.
               88  CA-RESPONSE-ERROR                   VALUE 'WWUT999E'.

      *    *************************************************************
      *    * SERVICE-NAME/RECORD-KEY IDENTIFY A UNIQUE SERVICE REQUEST *
      *    *************************************************************
           05  CA-SERVICE-NAME         PIC X(24).
           05  CA-RECORD-KEY           PIC X(32).

           05  CA-SCREEN-STEP          PIC X(16).
      *        *********************************************************
      *        * INCOMING VALUES:                                      *
      *        *   1) 'START' = WEB SERVICE REQUEST STARTED            *
      *        *   2) '?????' = SCREEN-STEP (NAME?) COMPLETED          *
      *        *   3) 'END  ' = WEB SERVICE REQUEST COMPLETED          *
      *        * OUTGOING VALUES:                                      *
      *        *   1) IF A WEB SERVICE REQUEST IS BEING RESTARTED,     *
      *        *      WNV.LAST_STEP_COMPLETE IS RETURNED HERE          *
      *        *********************************************************

           05  CA-BEF-AFT-IND          PIC X(01).
      *        *********************************************************
      *        *   'B' = INVOKED BEFORE SCREEN-STEP STARTED            *
      *        *   'A' = INVOKED AFTER  SCREEN-STEP COMPLETED          *
      *        *********************************************************

           05  CA-USER-CD              PIC X(08).
      *        *********************************************************
      *        * LOGON USERID OF THE WEB SERVICE REQUESTOR             *
      *        *********************************************************

           05  CA-FILLER               PIC X(01).

           05  CA-SOAP-RESP-AREA.
      *        *********************************************************
FP6406*        *           VARIABLE LENGTH = VARCHAR(948)              *
      *        * INCOMING: SOAP INPUT AREA (PASSED WHEN SCREEN-STEP =  *
      *        *           'START'; REDEFINED FOR EACH SERVICE-NAME)   *
      *        * OUTGOING: TEXT ERROR MESSAGE WHEN CA-RESPONSE-ERROR   *
      *        *           IS RETURNED IN CA-VERS-RESP-CD              *
      *        *********************************************************
               10  CA-SOAP-RESP-AREA-LEN
                                       PIC S9(4) USAGE COMP.
               10  CA-SOAP-RESP-AREA-TEXT
FP6406                                 PIC X(948).

      ******************************************************************
      * TOTAL COMMAREA LENGTH = 100 TO 1048, DEPENDING ON SOAP-RESP-AREA
      ******************************************************************
