      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD31                              04/24/00  13:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD31.
DEVBWS     02  SAVE-PMKEY-ID-TA31           OCCURS 11   PIC X(8).
           02  ADD-FLAG-TA31                            PIC X(1).
           02  LISTVAL-FLAG-TA31                        PIC X(1).
           02  ACTN-ERR-FLAG-TA31                       PIC X(1).
           02  WK-APRVD-AMT-TA31              COMP-3    PIC S9(10)V99.
           02  WK-APRVD-AMT-TOTAL-TA31        COMP-3    PIC S9(10)V99.
           02  X-DATECHG-DATE-TA31.
               03  X-DATECHG-MM-TA31                    PIC 9(2).
               03  X-DATECHG-DD-TA31                    PIC 9(2).
               03  X-DATECHG-CC-TA31                    PIC 9(2).
               03  X-DATECHG-YY-TA31                    PIC 9(2).
           02  NUMERIC-8-TA31                           PIC 9(8).
           02  CAL-DATE-TA31.
               03  CAL-MM-TA31                          PIC 9(2).
               03  CAL-DD-TA31                          PIC 9(2).
               03  CAL-YY-TA31                          PIC 9(2).
