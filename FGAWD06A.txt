      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD06A                             04/24/00  13:07  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWD06A.
           02  DATA-REQUEST-FLAG-GA06                   PIC X(1).
           02  RCD-CNTR-IND-GA06                        PIC X(1).
           02  IO-CALLS-IND-GA06                        PIC X(1).
           02  SEL-DATA-CODE-GA06           OCCURS 4    PIC X(6).
           02  CUR-DATA-CODE-GA06           OCCURS 4    PIC X(6).
           02  START-SEARCH-FLAG-GA06                   PIC 9(1).
           02  RCD-CNTR-GA06                  COMP-3    PIC 9(6).
           02  RCD-READ-LIMIT-4012-GA06       COMP-3    PIC 9(6).
