       01  R4193-CLOSE-LDGR.
           02  DB-PROC-ID-4193.
               03  USER-CODE-4193                       PIC X(8).
               03  LAST-ACTVY-DATE-4193                 PIC X(5).
               03  TRMNL-ID-4193                        PIC X(8).
               03  PURGE-FLAG-4193                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CLOSE-LDGR-KEY-4193.
               03  UNVRS-CODE-4193                      PIC X(2).
               03  COA-CODE-4193                        PIC X(1).
               03  FSCL-YR-4193                         PIC X(2).
           02  ACTVY-DATE-4193                COMP-3    PIC S9(8).
           02  BAL-FRWRD-SCHDL-DATE-4193      COMP-3    PIC S9(8).
           02  BAL-FRWRD-PRFMD-DATE-4193      COMP-3    PIC S9(8).
           02  ENCMBRNC-SCHDL-DATE-4193       COMP-3    PIC S9(8).
           02  ENCMBRNC-PRFMD-DATE-4193       COMP-3    PIC S9(8).
           02  BDGT-SCHDL-DATE-4193           COMP-3    PIC S9(8).
           02  BDGT-PRFMD-DATE-4193           COMP-3    PIC S9(8).
           02  CLOSE-OAL-SCHDL-DATE-4193      COMP-3    PIC S9(8).
           02  CLOSE-OAL-PRFMD-DATE-4193      COMP-3    PIC S9(8).
           02  BAL-FRWRD-DCMNT-NMBR-4193                PIC X(8).
