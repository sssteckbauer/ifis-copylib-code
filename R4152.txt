       01  R4152-INV-ACCT.
           02  DB-PROC-ID-4152.
               03  USER-CODE-4152                       PIC X(8).
               03  LAST-ACTVY-DATE-4152                 PIC X(5).
               03  TRMNL-ID-4152                        PIC X(8).
               03  PURGE-FLAG-4152                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  SEQ-NMBR-4152                            PIC 9(4).
           02  NSF-OVRDE-4152                           PIC X(1).
           02  ADDL-CHRG-PCT-4152             COMP-3    PIC 9(3)V999.
           02  INVD-AMT-4152                  COMP-3    PIC 9(10)V99.
           02  DSCNT-AMT-4152                 COMP-3    PIC S9(10)V99.
           02  LIQDTN-IND-4152                          PIC X(1).
           02  COA-CODE-4152                            PIC X(1).
           02  ACCT-INDX-CODE-4152                      PIC X(10).
           02  FUND-CODE-4152                           PIC X(6).
           02  ORGZN-CODE-4152                          PIC X(6).
           02  ACCT-CODE-4152                           PIC X(6).
           02  PRGRM-CODE-4152                          PIC X(6).
           02  ACTVY-CODE-4152                          PIC X(6).
           02  LCTN-CODE-4152                           PIC X(6).
           02  ACCT-ERROR-IND-4152                      PIC X(1).
           02  FSCL-YR-4152                             PIC X(2).
           02  BANK-ACCT-CODE-4152                      PIC X(2).
           02  APRVD-AMT-4152                 COMP-3    PIC S9(10)V99.
           02  TAX-AMT-4152                   COMP-3    PIC S9(10)V99.
           02  INVD-PCT-4152                  COMP-3    PIC 9(3)V999.
           02  DSCNT-PCT-4152                 COMP-3    PIC 9(3)V999.
           02  ADDL-CHRG-4152                 COMP-3    PIC S9(10)V99.
           02  INCM-TYPE-SEQ-NMBR-4152                  PIC 9(4).
           02  PAID-AMT-4152                  COMP-3    PIC S9(10)V99.
           02  RULE-CLASS-CODE-4152                     PIC X(4).
           02  DSCNT-RULE-CLASS-4152                    PIC X(4).
           02  TAX-RULE-CLASS-4152                      PIC X(4).
           02  ADDL-CHRG-RULE-CLASS-4152                PIC X(4).
           02  PSTNG-PRD-4152                           PIC X(2).
           02  PRJCT-CODE-4152                          PIC X(8).
           02  PO-NMBR-4152                             PIC X(8).
           02  PO-ITEM-NMBR-4152              COMP-3    PIC 9(4).
           02  PO-SEQ-NMBR-4152                         PIC 9(4).
           02  FILLER02                                 PIC X(10).
