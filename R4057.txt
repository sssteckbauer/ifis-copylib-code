       01  R4057-RQST-ID.
           02  DB-PROC-ID-4057.
               03  USER-CODE-4057                       PIC X(8).
               03  LAST-ACTVY-DATE-4057                 PIC X(5).
               03  TRMNL-ID-4057                        PIC X(8).
               03  PURGE-FLAG-4057                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  RQST-ID-KEY-4057.
               03  UNVRS-CODE-4057                      PIC X(2).
               03  RPRT-RQST-ID-4057                    PIC X(4).
           02  ACTVY-DATE-4057                COMP-3    PIC S9(8).
           02  MGR-PID-4057.
               03  MGR-DIGIT-ONE-4057                   PIC X(1).
               03  MGR-LAST-NINE-4057                   PIC X(9).
           02  RPRT-LCTN-4057                           PIC X(6).
           02  RPRT-RQST-DESC-4057                      PIC X(35).
           02  COA-CODE-4057                            PIC X(1).
           02  FROM-ACCT-INDX-4057                      PIC X(10).
           02  TO-ACCT-INDX-4057                        PIC X(10).
           02  FROM-FUND-4057                           PIC X(6).
           02  TO-FUND-4057                             PIC X(6).
           02  FROM-ORGZN-4057                          PIC X(6).
           02  TO-ORGZN-4057                            PIC X(6).
           02  FROM-SUB-ACCT-4057                       PIC X(6).
           02  TO-SUB-ACCT-4057                         PIC X(6).
           02  FROM-PRGRM-4057                          PIC X(6).
           02  TO-PRGRM-4057                            PIC X(6).
           02  FROM-ACTVY-4057                          PIC X(6).
           02  TO-ACTVY-4057                            PIC X(6).
           02  FROM-LCTN-4057                           PIC X(6).
           02  TO-LCTN-4057                             PIC X(6).
           02  EXCL-IND-4057                            PIC X(1).
           02  NMBR-OF-COPIES-4057                      PIC 9(3).
