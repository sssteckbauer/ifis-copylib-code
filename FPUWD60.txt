      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWD60                              04/24/00  13:13  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWD60.
           02  SAVE-DBKEY-ID-4210-PU60
DEVBWS                                      OCCURS 11   PIC X(34).
DEVBWS     02  SAVE-DBKEY-ID-4209-PU60                  PIC X(8).
           02  SAVE-DBKEY-LAST-RECORD-PU60    COMP      PIC S9(8).
           02  SAVE-DBKEY-LAST-ADDED-PU60     COMP      PIC S9(8).
           02  SAVE-MAIN-SUB-PU60             COMP-3    PIC 9(3).
           02  START-DATE-PU60                COMP-3    PIC S9(8).
           02  SYSTEM-DATE-PU60               COMP-3    PIC S9(8).
           02  END-DATE-PU60                  COMP-3    PIC S9(8).
