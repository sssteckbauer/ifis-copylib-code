      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWM04                              04/24/00  12:32  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWM04.
           02  COA-TITLE-4076-GA04                      PIC X(35).
           02  ACTVY-DATE-4193-GA04                     PIC X(6).
           02  ACTN-CODE-GA04                           PIC X(1).
           02  BAL-FRWRD-SCHDL-DATE-4193-GA04           PIC X(6).
           02  BAL-FRWRD-PRFMD-DATE-4193-GA04           PIC X(6).
           02  ENCMBRNC-SCHDL-DATE-4193-GA04            PIC X(6).
           02  ENCMBRNC-PRFMD-DATE-4193-GA04            PIC X(6).
           02  BDGT-SCHDL-DATE-4193-GA04                PIC X(6).
           02  BDGT-PRFMD-DATE-4193-GA04                PIC X(6).
           02  BAL-FRWRD-DCMNT-NMBR-4193-GA04           PIC X(8).
           02  CLOSE-OAL-SCHDL-DATE-4193-GA04           PIC X(6).
           02  CLOSE-OAL-PRFMD-DATE-4193-GA04           PIC X(6).
