       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 12/05/00 at 15:00***

           02  FTAMU02-MAP-CONTROL.
           03  FTAMU02T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FTAMU02I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 10234.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 887.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ACCT-LST-NN-4400-T02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ACCT-LST-NN-4400-T02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ACCT-LST-NN-4400-T02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ACCT-LST-NN-4400-T02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ACCT-LST-NN-4400-T02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ACCT-LST-NN-4400-T02       PIC X(9)
                                               VALUE SPACES.
               10  WK01-KEY-6117-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6117-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6117-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6117-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6117-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-KEY-6117-TA02       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TEXT-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-TA02       PIC X(17)
                                               VALUE SPACES.
               10  WK01-TYPE-CODE-6137-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TYPE-CODE-6137-TA02       PIC X(2)
                                               VALUE SPACES.
               10  WK01-LONG-DESC-6137-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LONG-DESC-6137-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LONG-DESC-6137-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LONG-DESC-6137-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LONG-DESC-6137-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-LONG-DESC-6137-TA02       PIC X(30)
                                               VALUE SPACES.
               10  WK01-IND-4400-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4400-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4400-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4400-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4400-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-IND-4400-TA02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-RQST-DATE-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RQST-DATE-TA02       PIC X(8)
                                               VALUE SPACES.
DEVMJM* ACR46019
               10  WK01-UPDT-USER-ID-TA02-Z           PIC X(1)
                                               VALUE SPACE.
               10  WK01-UPDT-USER-ID-TA02-F           PIC X(1)
                                               VALUE SPACE.
               10  WK01-UPDT-USER-ID-TA02-A           PIC X(1)
                                               VALUE SPACE.
               10  WK01-UPDT-USER-ID-TA02-V           PIC X(1)
                                               VALUE SPACE.
               10  WK01-UPDT-USER-ID-TA02-O           PIC X(1)
                                               VALUE SPACE.
               10  WK01-UPDT-USER-ID-TA02    PIC X(8)
                                               VALUE SPACES.
DEVMJM* ACR46019
               10  WK01-CODE-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-TA02       PIC X(1)
                                               VALUE SPACES.
               10  WK01-ADR-6139-TA021-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA021-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA021-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA021-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA021-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA021       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ADR-6139-TA022-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA022-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA022-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA022-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA022-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA022       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ADR-6139-TA023-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA023-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA023-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA023-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA023-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA023       PIC X(35)
                                               VALUE SPACES.
               10  WK01-ADR-6139-TA024-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA024-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA024-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA024-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA024-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ADR-6139-TA024       PIC X(35)
                                               VALUE SPACES.
               10  WK01-NAME-6139-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6139-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6139-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6139-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6139-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6139-TA02       PIC X(18)
                                               VALUE SPACES.
               10  WK01-CODE-6151-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6151-TA02       PIC X(2)
                                               VALUE SPACES.
               10  WK01-CODE-6152-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6152-TA02       PIC X(10)
                                               VALUE SPACES.
               10  WK01-CODE-6153-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-6153-TA02       PIC X(2)
                                               VALUE SPACES.
DEVMJM*
               10  WK01-FRGN-PROV-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-TA02       PIC X(2)
                                               VALUE SPACES.
               10  WK01-FRGN-PROV-NAME-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-NAME-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-NAME-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-NAME-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-NAME-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FRGN-PROV-NAME-TA02  PIC X(35)
                                               VALUE SPACES.
DEVMJM*
               10  WK01-AREA-CODE-6139-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-6139-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-6139-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-6139-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-6139-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-AREA-CODE-6139-TA02       PIC X(3)
                                               VALUE SPACES.
               10  WK01-XCHNG-ID-6139-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-6139-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-6139-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-6139-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-6139-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XCHNG-ID-6139-TA02       PIC X(3)
                                               VALUE SPACES.
               10  WK01-SEQ-ID-6139-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-6139-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-6139-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-6139-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-6139-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-SEQ-ID-6139-TA02       PIC X(4)
                                               VALUE SPACES.
               10  WK01-XTNSN-ID-6139-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-6139-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-6139-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-6139-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-6139-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-XTNSN-ID-6139-TA02       PIC X(4)
                                               VALUE SPACES.
               10  WK01-DATE-6139-TA02-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA02-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA02-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA02-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA02-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA02       PIC X(8)
                                               VALUE SPACES.
               10  WK01-DATE-6139-TA0A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA0A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA0A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA0A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA0A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-6139-TA0A       PIC X(8)
                                               VALUE SPACES.
               10  WK01-EMAIL-ADDR-6139-TA02-Z       PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADDR-6139-TA02-F       PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADDR-6139-TA02-A       PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADDR-6139-TA02-V       PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADDR-6139-TA02-O       PIC X(1)
                                               VALUE SPACE.
               10  WK01-EMAIL-ADDR-6139-TA02 PIC X(35)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
           04  WK01-SUBSCRIPTED-WORK-AREA.
               05  FILLER                      PIC X(1).
               05  WK01-ADR-6139-TA02-F             PIC X(1)
                                               OCCURS 4.
