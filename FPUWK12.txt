      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWK12                              04/24/00  12:39  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWK12.
           02  PO-NMBR-4083-PU12                        PIC X(8).
           02  CHNG-SEQ-NMBR-4096-PU12                  PIC X(3).
           02  EFCTV-RCRD-KEY-4154-PU12.
               03  KEY-TAX-TYPE-4154-PU12               PIC X(1).
               03  KEY-TAX-CODE-IND-4154-PU12           PIC X(1).
               03  KEY-START-DATE-4154-PU12   COMP-3    PIC S9(8).
               03  KEY-TIME-STAMP-4154-PU12             PIC X(6).
