000100*-------------------------------------------------------------------------
000200 01  GENERAL-LEDGER-INPUT-RECORD-WS.                              FISGLIWS
000300     03  TYPE-ENTRY              PIC X(02) VALUE SPACES.          FISGLIWS
000400         88  VALID-TYPE VALUE '11' THRU '15'                      FISGLIWS
000500                              '32' THRU '37'                      FISGLIWS
000600                              '42'                                FISGLIWS
000700                              '52' THRU '54'                      FISGLIWS
000800                              '60' THRU '61'.                     FISGLIWS
000900     03  LOCATION                PIC X(01) VALUE SPACES.          FISGLIWS
001000         88  VALID-LOCATION VALUE '6' 'O'.                        FISGLIWS
001100     03  ACCOUNT                 PIC 9(06) VALUE 0.               FISGLIWS
001200     03  FUND                    PIC X(05) VALUE SPACES.          FISGLIWS
001300     03  SUB                     PIC X(01) VALUE SPACES.          FISGLIWS
001400     03  OBJECT-CODE             PIC X(04) VALUE SPACES.          FISGLIWS
001500     03  DOCUMENT-DATE           PIC 9(05) VALUE 0.               FISGLIWS
001600     03  DOCUMENT-DATE-REDF REDEFINES DOCUMENT-DATE.              FISGLIWS
001700          05  DOCUMENT-DATE-MM   PIC 9(02).                       FISGLIWS
001800          05  DOCUMENT-DATE-DD   PIC 9(02).                       FISGLIWS
001900          05  DOCUMENT-DATE-YY   PIC 9(01).                       FISGLIWS
002000     03  DESCRIPTION             PIC X(18) VALUE SPACES.          FISGLIWS
002100     03  REFERENCE-NUMBER        PIC X(06) VALUE SPACES.          FISGLIWS
002200     03  JOURNAL-NUMBER          PIC X(05) VALUE SPACES.          FISGLIWS
002300     03  JOURNAL-NUMBER-REDF REDEFINES JOURNAL-NUMBER.            FISGLIWS
002400         05  JOURNAL-MM          PIC 9(02).                       FISGLIWS
002500         05  JOURNAL-ID          PIC X(03).                       FISGLIWS
002600     03  FILLER                  PIC X(39) VALUE SPACES.          FISGLIWS
002700     03  SUMMARY-FLAG            PIC X(01) VALUE SPACES.          FISGLIWS
002800     03  SERVICE-DEPT            PIC X(03) VALUE SPACES.          FISGLIWS
002900     03  FILLER                  PIC X(06) VALUE SPACES.          FISGLIWS
003000     03  ORIGIN                  PIC X(09) VALUE SPACES.          FISGLIWS
003100     03  FILLER                  PIC X(10) VALUE SPACES.          FISGLIWS
003200     03  DATE-ENTERED            PIC 9(06) VALUE 0.               FISGLIWS
003300     03  DATE-ENTERED-REDF REDEFINES DATE-ENTERED.                FISGLIWS
003400         05  DATE-ENTERED-MM     PIC 9(02).                       FISGLIWS
003500         05  DATE-ENTERED-DD     PIC 9(02).                       FISGLIWS
003600         05  DATE-ENTERED-YY     PIC 9(02).                       FISGLIWS
003700     03  BUDGET-NUMBER           PIC 9(04) VALUE 0.               FISGLIWS
003800     03  TAXPAYER-ID             PIC X(12) VALUE SPACES           FISGLIWS
003900                                           JUSTIFIED RIGHT.       FISGLIWS
004000     03  FILLER                  PIC X(18) VALUE SPACES.          FISGLIWS
004100     03  NET-AMOUNT              PIC S9(10)V9(02) VALUE +0.       FISGLIWS
004200     03  CLASS-CODE              PIC X(03) VALUE SPACES.          FISGLIWS
004300     03  FILLER                  PIC X(04) VALUE SPACES.          FISGLIWS
004400                                                                  FISGLIWS
004500 01  GENERAL-LEDGER-INPUT-HEADER-WS REDEFINES                     FISGLIWS
004600     GENERAL-LEDGER-INPUT-RECORD-WS.                              FISGLIWS
004700     03  GL-RECORD-TYPE          PIC X(04).                       FISGLIWS
004800     03  FILLER                  PIC X(01).                       FISGLIWS
004900     03  DATASET-NAME            PIC X(17).                       FISGLIWS
005000     03  FILLER                  PIC X(01).                       FISGLIWS
005100     03  PARTITION-DATE          PIC 9(04).                       FISGLIWS
005200     03  FILLER                  PIC X(01).                       FISGLIWS
005300     03  CREATION-DATE           PIC 9(06).                       FISGLIWS
005400     03  FILLER                  PIC X(146).                      FISGLIWS
005500                                                                  FISGLIWS
005600 01  GENERAL-LEDGER-INPUT-TRAIL1-WS REDEFINES                     FISGLIWS
005700     GENERAL-LEDGER-INPUT-RECORD-WS.                              FISGLIWS
005800     03  GL-RECORD-TYPE          PIC X(04).                       FISGLIWS
005900     03  TRANSMITTAL-COUNT       PIC 9(05).                       FISGLIWS
006000     03  HASH-TOTAL-ACCOUNT      PIC 9(06).                       FISGLIWS
006100     03  HASH-TOTAL-FUND         PIC 9(05).                       FISGLIWS
006200     03  HASH-TOTAL-NET-AMOUNT   PIC 9(10)V9(02).                 FISGLIWS
006300     03  CONTROL-TYPE-ENTRY-GROUP                                 FISGLIWS
006400         OCCURS 10 TIMES.                                         FISGLIWS
006500         05  CONTROL-TYPE-ENTRY  PIC X(02).                       FISGLIWS
006600         05  CONTROL-NET-AMOUNT  PIC 9(10)V9(02).                 FISGLIWS
006700     03  FILLER                  PIC X(08).                       FISGLIWS
006800                                                                  FISGLIWS
006900 01  GENERAL-LEDGER-INPUT-TRAIL2-WS REDEFINES                     FISGLIWS
007000     GENERAL-LEDGER-INPUT-RECORD-WS.                              FISGLIWS
007100     03  GL-RECORD-TYPE          PIC X(04).                       FISGLIWS
007200     03  CONTROL-TYPE-ENTRY-GROUP                                 FISGLIWS
007300         OCCURS 12 TIMES.                                         FISGLIWS
007400         05  CONTROL-TYPE-ENTRY  PIC X(02).                       FISGLIWS
007500         05  CONTROL-NET-AMOUNT  PIC 9(10)V9(02).                 FISGLIWS
007600     03  FILLER                  PIC X(08).                       FISGLIWS
007700                                                                  FISGLIWS
007800*-------------------------------------------------------------------------
