       01  FGA003-RECORD.
           03  FGA003-SORT-KEY.
               05  FGA003-FIXED-KEY.
                   07  FGA003-UNVRS-ID               PIC X(02).
                   07  FGA003-USER-ID                PIC X(08).
                   07  FGA003-RPRT-CODE              PIC X(08).
                   07  FGA003-SEQ-NMBR               PIC 9(04).
               05  FGA003-VAR-KEY.
                   07  FGA003-COA-CODE               PIC X(01).
                   07  FGA003-FUND-OR-FUND-TYPE-CODE PIC X(06).
                   07  FGA003-RPRT-SCTN              PIC X(01).
                   07  FGA003-ACCT-OR-PRGRM-CODE     PIC X(06).
                   07  FGA003-FSCL-YR                PIC X(02).
                   07  FILLER                        PIC X(64).
               05  FGA003-RCRD-TYPE                  PIC X(01).
           03  FGA003-DATA                           PIC X(360).
           03  FGA003-ONE-TIME-DATA REDEFINES
               FGA003-DATA.
               05  FGA003-AS-OF-DATE                 PIC 9(08)  COMP-3.
               05  FGA003-UNVRS-TITLE                PIC X(35).
               05  FGA003-USER-NAME                  PIC X(35).
               05  FGA003-COA-DESC                   PIC X(35).
               05  FGA003-CRNT-PRD-END-DATE          PIC 9(08)  COMP-3.
               05  FGA003-PRIOR-YR-PRD-END-DATE      PIC 9(08)  COMP-3.
               05  FGA003-PARM-FSCL-YR               PIC X(02).
               05  FGA003-PRIOR-FSCL-YR              PIC X(02).
               05  FILLER                            PIC X(236).
           03  FGA003-DETAIL-DATA REDEFINES
               FGA003-DATA.
               05  FGA003-FUND-OR-FUND-TYPE-TITLE    PIC X(35).
               05  FGA003-ACCT-OR-PRGRM-TITLE        PIC X(35).
               05  FGA003-BAL-AMT            COMP-3  PIC S9(11)V99.
               05  FILLER                            PIC X(283).
