      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD19                              04/24/00  12:55  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD19.
           02  INDX-KEY-4158-AP19.
               03  UNVRS-CODE-AP19                      PIC X(2).
               03  BANK-ACCT-CODE-AP19                  PIC X(2).
               03  CHECK-NMBR-AP19                      PIC X(8).
           02  SET-SORT-KEY-4012-AP19.
               03  OPTN-1-CODE-4012-AP19                PIC X(8).
               03  OPTN-2-CODE-4012-AP19                PIC X(8).
               03  LEVEL-NMBR-4012-AP19                 PIC 9(2).
