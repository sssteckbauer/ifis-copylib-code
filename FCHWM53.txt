      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM53                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM53.
           02  ACTN-CODE-GROUP-CH53.
               03  ACTN-CODE-CH53           OCCURS 13   PIC X(1).
           02  COA-CODE-4013-CH53           OCCURS 13   PIC X(1).
           02  USER-ID-4192-CH53            OCCURS 13   PIC X(8).
           02  ORGZN-CODE-4013-CH53         OCCURS 13   PIC X(6).
           02  NSF-OVRDE-4013-CH53          OCCURS 13   PIC X(1).
           02  TLRNC-PCTG-4013-CH53         OCCURS 13   PIC X(7).
           02  TLRNC-AMT-4013-CH53          OCCURS 13   PIC X(16).
           02  BUYER-CODE-4013-CH53         OCCURS 13   PIC X(4).
