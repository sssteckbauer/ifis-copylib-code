      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD26                              04/24/00  13:01  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD26.
           02  SYSTEM-DATE-CH26               COMP-3    PIC S9(8).
           02  START-DATE-CH26                COMP-3    PIC S9(8).
           02  SAVE-DBKEY-ID-4009-CH26        COMP      PIC S9(8).
           02  END-DATE-CH26                  COMP-3    PIC S9(8).
           02  SYSDAT-KEY-CH26.
               03  OPTN-1-CODE-CH26                     PIC X(8).
               03  OPTN-2-CODE-CH26                     PIC X(8).
               03  LEVEL-NMBR-CH26                      PIC 9(2).
           02  RQST-FLAG-CH26                           PIC X(1).
