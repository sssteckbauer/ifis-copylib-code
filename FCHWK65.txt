      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWK65                              04/24/00  12:18  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWK65.
           02  SLCTN-DATE-CH65                          PIC X(6).
           02  SLCTN-STATUS-CH65                        PIC X(1).
           02  MGR-PID-CH65.
               03  MGR-DIGIT-ONE-CH65                   PIC X(1).
               03  RQST-MGR-LAST-NINE-CH65              PIC X(9).
