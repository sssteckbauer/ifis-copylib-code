       01  MAP-CONTROL.

    ***Created by Convert/DC version V8R03 on 11/20/00 at 12:00***

           02  FPUMU60-MAP-CONTROL.
           03  FPUMU60T.
           04  WK01-DCXMAPL-PARMS.
               10  WK01-MAPNAME                  PIC X(8)
                                                 VALUE 'FPUMU60I'.
               10  WK01-IKE-LENGTH               PIC S9(8)  COMP
                                                 VALUE 7867.
               10  WK01-TINA-LENGTH              PIC S9(4)  COMP
                                                 VALUE 1150.
           05  WK01-ATTRIBUTES                   PIC X(60)
                                                 VALUE SPACE.
           05  WK01-ATTRIBUTES-ERROR             PIC X(60) VALUE
               'U  BIF'.
           05  WK01-CURSOR-DFLD                  PIC X(36)
                                                 VALUE SPACES.
           05  WK01-CURSOR-DFLD-QUAL             PIC X(30)
                                                 VALUE SPACES.
           05  WK01-ROW                          PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-COLUMN                       PIC S9(4) COMP
                                                 VALUE ZERO.
           05  WK01-ERROR-MSG                    PIC X(80)
                                                 VALUE SPACES.
           05  WK01-MAP-DATA-LEN                 PIC S9(6)
                                                 VALUE ZERO.
           05  WK01-1ST-FLAG                     PIC X(1)
                                                 VALUE 'Y'.
           05  WK01-FLAG-OUTPUT-DATA             PIC X(1)
                                                 VALUE 'Y'.
      *    05  WK01-STATUS-RETURN-FLAG           PIC X(1)
      *                                          VALUE SPACES.
           05  WK01-ALL-BUT-FLAG                 PIC X(1)
                                                 VALUE SPACES.
           05  WK01-FLAG-ALL-FIELDS-CHANGED      PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-CHANGED      PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERROR        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERROR        PIC X(1)  VALUE 'N'.
           05  WK01-FLAG-ALL-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.
           05  WK01-FLAG-ANY-FIELDS-ERASE        PIC X(1)  VALUE 'Y'.

           05  WK01-VARDATA                      PIC X(11880)
                                                 VALUE SPACES.
           05  FILLER                            PIC X(100)
                                                 VALUE SPACES.
           04  WK01-ELEMENT-AREA.

               10  WK01-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-NAME-6001-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-NAME-6001-SY00       PIC X(35)
                                               VALUE SPACES.
               10  WK01-DATE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-HH-MM-TIME-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-HH-MM-TIME-SY00       PIC X(4)
                                               VALUE SPACES.
               10  WK01-PM-FLAG-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PM-FLAG-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-USER-ID-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-USER-ID       PIC X(32)
                                               VALUE SPACES.
               10  WK01-RSPNS-ID-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-RSPNS-ID-SY00       PIC X(8)
                                               VALUE SPACES.
               10  WK01-FUNC-DESCRIPTION-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-FUNC-DESCRIPTION       PIC X(28)
                                               VALUE SPACES.
               10  WK01-STATUS-CODE-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-STATUS-CODE-SY00       PIC X(1)
                                               VALUE SPACES.
               10  WK01-CODE-4209-PU60-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4209-PU60-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4209-PU60-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4209-PU60-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4209-PU60-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-CODE-4209-PU60       PIC X(8)
                                               VALUE SPACES.
               10  WK01-DESC-4209-PU60-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-4209-PU60-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-4209-PU60-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-4209-PU60-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-4209-PU60-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DESC-4209-PU60       PIC X(35)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU601-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU601-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU601-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU601-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU601-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU601       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU602-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU602-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU602-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU602-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU602-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU602       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU603-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU603-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU603-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU603-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU603-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU603       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU604-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU604-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU604-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU604-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU604-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU604       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU605-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU605-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU605-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU605-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU605-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU605       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU606-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU606-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU606-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU606-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU606-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU606       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU607-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU607-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU607-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU607-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU607-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU607       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU608-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU608-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU608-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU608-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU608-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU608       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU609-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU609-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU609-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU609-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU609-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU609       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU6010-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6010-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6010-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6010-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6010-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6010       PIC X(55)
                                               VALUE SPACES.
               10  WK01-TEXT-4210-PU6011-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6011-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6011-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6011-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6011-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-4210-PU6011       PIC X(55)
                                               VALUE SPACES.
               10  WK01-DATE-4209-PU60-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU60-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU60-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU60-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU60-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU60       PIC X(6)
                                               VALUE SPACES.
               10  WK01-DATE-4209-PU6A-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6A-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6A-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6A-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6A-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6A       PIC X(6)
                                               VALUE SPACES.
               10  WK01-DATE-4209-PU6B-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6B-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6B-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6B-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6B-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-DATE-4209-PU6B       PIC X(6)
                                               VALUE SPACES.
               10  WK01-ZZMESSAGE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-ZZMESSAGE       PIC X(160)
                                               VALUE SPACES.
               10  WK01-PASSED-ONE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-PASSED-ONE       PIC X(32)
                                               VALUE SPACES.
               10  WK01-MAP-RESPONSE-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-MAP-RESPONSE       PIC X(8)
                                               VALUE SPACES.
               10  WK01-TEXT-SY00-Z              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-F              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-A              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-V              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00-O              PIC X(1)
                                               VALUE SPACE.
               10  WK01-TEXT-SY00       PIC X(14)
                                               VALUE SPACES.
           04  WK01-SUBSCRIPTED-WORK-AREA.
               05  FILLER                      PIC X(1).
               05  WK01-TEXT-4210-PU60-F             PIC X(1)
                                               OCCURS 11.
