      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWK45                              04/24/00  12:37  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWK45.
           02  RQST-CMDTY-CODE-PU45                     PIC X(8).
           02  RQST-CMDTY-DESC-PU45                     PIC X(35).
DEVBWS     02  SAVE-DBKEY-ID-MINUS-ONE-PU45             PIC X(8).
DEVBWS     02  SAVE-DBKEY-ID-PLUS-ONE-PU45              PIC X(8).
           02  DISP-OPTION-PU45                         PIC X(01).
           02  EQUIP-PRICE-RNG-PU45                     PIC X(01).
