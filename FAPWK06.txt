      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWK06                              04/24/00  12:32  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWK06.
           02  VNDR-CODE-AP06.
               03  VNDR-ID-DIGIT-ONE-AP06               PIC X(1).
               03  VNDR-ID-LAST-NINE-AP06               PIC X(9).
           02  RQST-DCMNT-NMBR-AP06                     PIC X(8).
           02  WK-TOT-RPT-AMT-AP06            COMP-3    PIC 9(10)V99.
           02  WK-TOT-FDRL-WTHHLD-AP06        COMP-3    PIC 9(6)V99.
           02  WK-TOT-STATE-WTHHLD-AP06       COMP-3    PIC 9(6)V99.
           02  NAME-KEY-AP06                            PIC X(35).
           02  MAP-TOT-FDRL-WTHHLD-AP06       COMP-3    PIC 9(6)V99.
           02  MAP-TOT-STATE-WTHHLD-AP06      COMP-3    PIC 9(6)V99.
           02  MAP-TOT-RPT-AMT-AP06           COMP-3    PIC 9(10)V99.
