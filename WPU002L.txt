      ******************************************************************
      *    **** COPYBOOK WPU002L - USED FOR LOADPOS WEB SERVICE ****   *
      *                                                                *
      *    WEB/CICS COMMAREA TO LOAD POS_PO_HDR_SEQ TABLE FROM SCIQUEST*
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      ******************************************************************

       01  LOADPOS-PARM-AREA.

         02  LOADPOS-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  LOADPOS-CHANNEL-ID             PIC X(08).
      *        *********************************************************
      *        * MUST BE 'LOADPOS '                                    *
      *        *********************************************************

           05  LOADPOS-VERS-RESP-CD           PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WFPU(WEB/IFIS PU SYSTEM)*
      *        *********************************************************
               88  LOADPOS-RESPONSE-SUCCESSFUL         VALUE 'WFPU000I'.
               88  LOADPOS-RESPONSE-ERROR              VALUE 'WFPU999E'.

         02  LOADPOS-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

           05  LOADPOS-FUNCTION               PIC X(08).
      *        *********************************************************
      *        * ADD      = INSERT POS ROW                             *
      *        * CHGXXXXX = UPDATE POS FOR XXXXX                       *
      *        *********************************************************

           05  LOADPOS-USER-CD                PIC X(08).
NOSEND*****05  LOADPOS-LAST-ACTVY-DT          PIC X(05).
           05  LOADPOS-CHNG-SEQ-NBR           PIC X(03).
           05  LOADPOS-PO-CMPLT-IND           PIC X(01).
           05  LOADPOS-PRINT-DT               PIC X(10).
           05  LOADPOS-PO-PRINT-FLAG          PIC X(01).
           05  LOADPOS-DLVRY-BY-DT            PIC X(10).
           05  LOADPOS-ACKNWDG-DT             PIC X(10).
           05  LOADPOS-ORDER-DT               PIC X(10).
           05  LOADPOS-APRVL-IND              PIC X(01).
           05  LOADPOS-HDR-CNTR-DTL           PIC +9(04).
           05  LOADPOS-HDR-CNTR-ACCT          PIC +9(04).
           05  LOADPOS-HDR-ERROR-IND          PIC X(01).
           05  LOADPOS-TOTAL-AMT              PIC +9(10).99.
NOSEND*****05  LOADPOS-ACTVY-DT               PIC X(10).
           05  LOADPOS-CNCL-IND               PIC X(01).
           05  LOADPOS-CNCL-DT                PIC X(10).
           05  LOADPOS-ADDL-AMT               PIC +9(10).99.
           05  LOADPOS-DSCNT-AMT              PIC +9(10).99.
           05  LOADPOS-ITEM-COUNT             PIC +9(04).
           05  LOADPOS-BUYER-CD               PIC X(04).
           05  LOADPOS-VNDR-CNTCT-NM          PIC X(35).
           05  LOADPOS-MAILCODE               PIC X(06).
           05  LOADPOS-PO-DSCNT-AMT           PIC +9(10).99.
           05  LOADPOS-DSCNT-BFR-TX-IND       PIC X(01).
           05  LOADPOS-PO-DSCNT-PCT           PIC +9(03).999.
           05  LOADPOS-TAX-CD                 PIC X(03).
           05  LOADPOS-DSCNT-CD               PIC X(02).
           05  LOADPOS-PYMT-CD                PIC X(02).
           05  LOADPOS-SHIP-TO-CD             PIC X(06).
           05  LOADPOS-TRNST-RISK-CD          PIC X(02).
           05  LOADPOS-ACKNWDG-IND            PIC X(01).
           05  LOADPOS-ADR-TYP-CD             PIC X(02).
           05  LOADPOS-END-DT                 PIC X(10).
           05  LOADPOS-ACCT-INDX-CD           PIC X(10).
           05  LOADPOS-FK-POH-PO-NBR          PIC X(08).
           05  LOADPOS-IDX-PO-PRINT-SETF      PIC X(1).
           05  LOADPOS-IDX-PO-PRINT-TS        PIC X(26).

         02  LOADPOS-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  LOADPOS-RETURN-STATUS-CODE     PIC X(01).
      *        *********************************************************
      *        * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR    *
      *        *********************************************************

           05  LOADPOS-RETURN-MSG-COUNT       PIC 9(01).
      *        *********************************************************
      *        * 1 = NUMBER OF RETURN-MSG-AREA(S)                      *
      *        *********************************************************

           05  LOADPOS-RETURN-MSG-AREA.
      *        *********************************************************
      *        * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT         *
      *        * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT        *
      *        * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH       *
      *        *                 PARAGRAPH NAME, & ASSOC DB2 TABLE     *
      *        *********************************************************
               10  LOADPOS-RETURN-MSG-NUM     PIC X(06).
               10  LOADPOS-RETURN-MSG-FIELD   PIC X(30).
               10  LOADPOS-RETURN-MSG-TEXT    PIC X(40).
