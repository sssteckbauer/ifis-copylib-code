      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWD26                              04/24/00  13:23  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWD26.
           02  DUP-ALT-FOUND-UT26                       PIC X(1).
           02  GOT-ALT-MSG-UT26                         PIC X(1).
           02  GOT-DUP-MSG-UT26                         PIC X(1).
           02  END-OF-R4216-IND-UT26                    PIC X(1).
           02  REPL-APRV-IND-UT26                       PIC X(1).
           02  END-OF-R4204-IND-UT26                    PIC X(1).
           02  END-OF-R4215-IND-UT26                    PIC X(1).
           02  END-OF-R4205-IND-UT26                    PIC X(1).
           02  END-OF-R4211-IND-UT26                    PIC X(1).
           02  WK-APRVL-ID-KEY-UT26.
               03  APRVL-ID-UT26                        PIC X(8).
               03  DCMNT-TYPE-UT26                      PIC X(3).
           02  WK-DOCUMENT-KEY-UT26.
               03  UNVRS-CODE-UT26                      PIC X(2).
               03  WK-DCMNT-TYPE-UT26                   PIC X(3).
           02  MAX-REC-CHANGES-UT26                     PIC 9(4).
           02  R4216-REC-CHANGED-UT26                   PIC 9(4).
           02  SAVED-TARGET-ID-UT26                     PIC X(08).
           02  SAVED-REPLACE-ID-UT26                    PIC X(08).
           02  SAVED-DOC-TYPE-UT26                      PIC X(03).
           02  SAVED-TMPL-CODE-UT26                     PIC X(03).
           02  WS-BATCH-TO-USER-ID                      PIC X(08).
           02  WK-BATCH-REQUEST-QUE.
               03  WS-BATCH-SUBMIT-USER-ID              PIC X(08).
               03  WS-BATCH-TARGET-ID                   PIC X(08).
               03  WS-BATCH-REPLACEMENT-ID              PIC X(08).
               03  WS-OWNER-DOC-SEQ-NBR                 PIC 9(04).
               03  WS-OWNER-DOC-TYPE                    PIC X(03).
               03  WS-OWNER-TEMPL-CODE                  PIC X(03).
