000100* ARGUMENTS FOR UTACHK CALL                                       00010001
000110 01  UTACHK-ARGS.                                                 00011004
000200     03  UTACHK-TRVL-TO-CHECK-DBKEY PIC 9(08) COMP SYNC.          00020004
000300     03  UTACHK-BIND-STATUS      PIC X(10).                       00030004
000400     03  UTACHK-FINISH-STATUS    PIC X(10).                       00040004
000500     03  UTACHK-MESSAGES-INIT.                                    00050004
000600         05  FILLER               PIC 999 VALUE 0.                00060004
000700         05  FILLER               PIC 999 VALUE 99.               00070004
000800         05  FILLER               PIC 999 VALUE 0.                00080004
000900         05  FILLER               PIC X(13300) VALUE ' '.         00090004
001000     03  UTACHK-MESSAGES REDEFINES UTACHK-MESSAGES-INIT.          00100004
001100         05  UTACHK-MSG-CNT       PIC 999.                        00110004
001200         05  UTACHK-MSG-MAX       PIC 999.                        00120004
001300         05  UTACHK-MSG-NDX       PIC 999.                        00130004
001400         05  UTACHK-MSG-ARRAY.                                    00140004
001500             07  UTACHK-MSG OCCURS 100 PIC X(133).                00150004
001600     03  UTACHK-PROGRAM-FAILED   PIC X(01).                       00160004
001700     03  UTACHK-VALID-STRUCTURES PIC X(01).                       00170004
