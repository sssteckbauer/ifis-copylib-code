       01  ZZYWG08S.
FCSAH      02  MAP-ELMNT-NAME-SY08                      PIC X(30).
FCSAH      02  MAIN-SUB-SY08                  COMP-3    PIC 9(3).
FCSAH      02  MSG-ID-SY08                              PIC 9(6).
FCSAH      02  MSG-TEXT-SY08                            PIC X(40).
FCSAH      02  APLCN-CURSOR-ROW-SY08          COMP      PIC S9(4).
FCSAH      02  APLCN-CURSOR-CLMN-SY08         COMP      PIC S9(4).
