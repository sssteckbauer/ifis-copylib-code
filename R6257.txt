       01  R6257.
           02  DB-PROC-ID-6257.
               03  USER-CODE-6257                       PIC X(8).
               03  LAST-ACTVY-DATE-6257                 PIC X(5).
               03  TRMNL-ID-6257                        PIC X(8).
               03  PURGE-FLAG-6257                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  COMM-VIEW-KEY-6257.
               03  UNVRS-CODE-6257                      PIC X(2).
               03  COMM-VIEW-6257                       PIC X(8).
           02  LONG-DESC-6257                           PIC X(30).
           02  BATCH-FLAG-6257                          PIC X(1).
