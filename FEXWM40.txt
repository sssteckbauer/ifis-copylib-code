      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWM40                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWM40.
           02  ACTN-CODE-EX40                           PIC X(1).
           02  ACCT-INDX-CODE-EX40                      PIC X(10).
           02  MAIL-CODE-EX40                           PIC X(6).
           02  BUYER-CODE-EX40                          PIC X(4).
           02  BUYER-NAME-EX40                          PIC X(35).
           02  ORDERED-BY-EX40                          PIC X(35).
           02  ORDER-DATE-EX40                          PIC X(8).
           02  COA-CODE-EX40                            PIC X(1).
           02  FUND-CODE-EX40                           PIC X(6).
           02  ORGZN-CODE-EX40                          PIC X(6).
           02  HDR-ACCT-CODE-EX40                       PIC X(6).
           02  PRGRM-CODE-EX40                          PIC X(6).
           02  ACTVY-CODE-EX40                          PIC X(6).
           02  LCTN-CODE-EX40                           PIC X(6).
           02  AMT-EX40                       COMP-3    PIC S9(10)V99.
           02  DEBIT-CRDT-IND-EX40                      PIC X(1).
           02  VOLUME-AMT-EX40                COMP-3    PIC S9(10)V99.
           02  VOL-DEBIT-CRDT-IND-EX40                  PIC X(1).
           02  SUB-TOTAL-EX40                 COMP-3    PIC S9(10)V99.
           02  SUB-DEBIT-CRDT-IND-EX40                  PIC X(1).
           02  TAX-AMT-EX40                   COMP-3    PIC S9(10)V99.
           02  TAX-DEBIT-CRDT-IND-EX40                  PIC X(1).
           02  ADDL-CHRG-EX40                 COMP-3    PIC S9(10)V99.
           02  ADDL-DEBIT-CRDT-IND-EX40                 PIC X(1).
           02  DSCNT-AMT-EX40                 COMP-3    PIC S9(10)V99.
           02  DSCNT-DEBIT-CRDT-IND-EX40                PIC X(1).
           02  RELEASE-AMT-EX40               COMP-3    PIC S9(10)V99.
           02  REL-DEBIT-CRDT-IND-EX40                  PIC X(1).
           02  REL-HDR-ERROR-IND-EX40                   PIC X(1).
           02  REL-DTL-ERROR-IND-EX40                   PIC X(1).
           02  DCMNT-REF-NMBR-EX40                      PIC X(10).
           02  CHECK-NMBR-EX40                          PIC X(8).
           02  USE-TAX-LITERAL-EX40                     PIC X(12).
           02  USE-TAX-AMT-EX40               COMP-3    PIC S9(10)V99.
           02  USE-TAX-DEBIT-CRDT-IND-EX40              PIC X(1).
