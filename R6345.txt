       01  R6345-CMNT-CD.
           02  DB-PROC-ID-6345.
               03  USER-CODE-6345                       PIC X(8).
               03  LAST-ACTVY-DATE-6345                 PIC X(5).
               03  TRMNL-ID-6345                        PIC X(8).
               03  PURGE-FLAG-6345                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CMNT-KEY-6345.
               03  UNVRS-CODE-6345                      PIC X(2).
               03  CMNT-TYPE-CODE-6345                  PIC X(4).
               03  CMNT-CODE-6345                       PIC X(4).
           02  LONG-DESC-6345                           PIC X(30).
