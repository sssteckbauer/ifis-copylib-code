      *----------------------------------------------------------------*
      *    LAYOUT FOR: FRPTLINK - INSTITUTION CODE AND DICTIONARY NAME *
      *                           PASSED IN LINKAGE BY FIMS BATCH      *
      *                           REPORT PROGRAMS.                     *
      *----------------------------------------------------------------*
       01  RPT-LINK.
           03  RPT-INST-CODE                   PICTURE X(02).
           03  RPT-DICT-NAME                   PICTURE X(08).
