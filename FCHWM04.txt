      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHMUO4T.                            04/24/00  12:22  *
      *                                                               *
      *---------------------------------------------------------------*
       01  MAP-CONTROL.
          02  FCHMU04T.
           03  WK01-DCXMAPL-PARMS.
               10 WK01-MAPNAME                   PIC X(8)
                                                 VALUE 'FCHMU04'.
               05 WK01-ATTRIBUTES                PIC X(60)
                                                     VALUE SPACE.
               05 WK01-ATTRIBUTES-ERROR          PIC X(60) VALUE
                   'U  BIF'.
               05 WK01-CURSOR-DFLD               PIC X(36)
                                                     VALUE SPACES.
               05 WK01-CURSOR-DFLD-QUAL          PIC X(30)
                                                     VALUE SPACES.
               05 WK01-ROW                       PIC S9(4) COMP
                                                     VALUE ZERO.
               05 WK01-COLUMN                    PIC S9(4) COMP
                                                     VALUE ZERO.
               05 WK01-ERROR-MSG                 PIC X(80)
                                                     VALUE SPACES.
               05 WK01-MAP-DATA-LEN              PIC S9(6)
                                                     VALUE 1229.
               05 WK01-1ST-FLAG                  PIC X(1)
                                                     VALUE 'Y'.
               05 WK01-FLAG-OUTPUT-DATA          PIC X(1)
                                                     VALUE 'Y'.
               05 WK01-ALL-BUT-FLAG              PIC X(1)
                                                     VALUE SPACES.
               05 WK01-FLAG-ALL-FIELDS-CHANGED   PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-CHANGED   PIC X(1)  VALUE 'N'.
                  88 ANY-FIELDS-CHANGED          VALUE 'Y'.
               05 WK01-FLAG-ALL-FIELDS-ERROR     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-ERROR     PIC X(1)  VALUE 'N'.
               05 WK01-FLAG-ALL-FIELDS-ERASE     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-ANY-FIELDS-ERASE     PIC X(1)  VALUE 'Y'.
               05 WK01-FLAG-START-DATE-CHANGED   PIC X(1)  VALUE 'Y'.
               05 WK01-PASSED-ONE-F              PIC X(1)  VALUE SPACE.
               05 WK01-VARDATA                   PIC X(80)
                                                     VALUE SPACES.
          02 FCHWM04.
               03 UCOP-FUND-NUMBER                      PIC X(05).
               03 START-DATE-4005-CH04                  PIC X(6).
               03 FND-GRP-BUDGET-CD-SAVED               PIC X(01).
               03 FND-GRP-FND-RESTR-CD-SAVED            PIC X(01).
               03 FUND-CODE-4001-SAVED                  PIC X(6).
               03 FUND-TITLE-4005-SAVED                 PIC X(35).
               03 GIFT-FUND-FLAG-SAVED                  PIC X(1).
               03 START-DATE-4005-SAVED                 PIC X(8).
               03 MOST-CURRENT-FLAG-SAVED               PIC X(1).
               03 PREDCSR-TITLE-4005-SAVED              PIC X(35).
               03 LVL3-FUND-CODE-SAVED                  PIC X(6).
               03 BDGT-TRTMNT-CODE-SAVED                PIC X(4).
               03 MGR-LAST-NINE-4005-SAVED              PIC X(9).
               03 CROSS-CODE-SAVED                      PIC X(06).
               03 FUND-GRP-CODE-SAVED                   PIC X(06).
               03 SPNSR-CODE-SAVED                      PIC X(04).
               03 SPNSR-CATGRY-SAVED                    PIC X(02).
               03 AWARD-TYPE-SAVED                      PIC X(01).
               03 ON-OFF-CAMPUS-CD-SAVED                PIC X(01).
               03 FDRL-FLOW-THRGH-IND-SAVED             PIC X(02).
               03 VC-CODE-SAVED                         PIC X(02).
               03 VC-INDX-SAVED                         PIC X(07).
               03 FUND-INDX-SAVED                       PIC X(07).
               03 CMPLT-IND-SAVED                       PIC X(1).
            02 PROTECTED-FIELD-SAVED.
               03 NEXT-EFCTV-DATE-SAVED                 PIC X(10).      00003930
               03 PREDCSR-CODE-4005-SAVED               PIC X(6).
               03 STATUS-4005-SAVED                     PIC X(1).
               03 STATUS-DESC-SAVED                     PIC X(8).
               03 LVL3-FUND-DESC-SAVED                  PIC X(35).
               03 FUND-TYPE-CODE-4005-SAVED             PIC X(2).
               03 FUND-TYPE-DESC-4009-SAVED             PIC X(35).
               03 XTRNL-CODE-DESC-SAVED                 PIC X(35).
               03 BDGT-DESC-SAVED                       PIC X(25).
               03 BDGT-DEFAULT-MSG-SAVED                PIC X(27).
               03 NAME-KEY-6117-SAVED                   PIC X(35).
               03 FUND-GRP-DESC-SAVED                   PIC X(55).
               03 SPNSR-CAT-DESC-SAVED                  PIC X(30).
               03 TYP-AWD-DESC-SAVED                    PIC X(34).
