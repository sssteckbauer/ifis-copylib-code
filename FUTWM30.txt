      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM30                              04/24/00  12:50  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM30.
           02  ACTN-CODE-UT30                           PIC X(1).
           02  ACH-RCVNG-DFI-ID-UT30                    PIC X(8).
           02  ACH-CHECK-DIGIT-UT30                     PIC X(1).
           02  ACH-DFI-ACCT-NMBR-UT30                   PIC X(17).
           02  ACH-STATUS-UT30                          PIC X(1).
           02  ACH-ERROR-REASON-UT30                    PIC X(3).
           02  REASON-DESC-UT30                         PIC X(30).
           02  START-DATE-UT30                          PIC X(8).
           02  END-DATE-UT30                            PIC X(8).
           02  ACH-PRENOTE-DCMNT-NMBR-UT30              PIC 9(7).
           02  ACH-PRENOTE-DATE-UT30          COMP-3    PIC S9(8).
           02  ADR-TYPE-DESC-UT30                       PIC X(30).
           02  ACH-ACCT-TYPE-UT30                       PIC X(1).
