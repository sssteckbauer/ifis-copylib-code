      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWK40                              04/24/00  12:34  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWK40.
           02  NAME-KEY-PE40                            PIC X(35).
           02  SUB-3-PE40                     COMP-3    PIC 9(3).
           02  OPTN-FLAG-PE40                           PIC X(1).
