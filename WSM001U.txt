      ******************************************************************
      * COPYBOOK WSN001U - USED FOR SECURITY UPDATE WEB SVC            *
      *                                                                *
      *    WEB/CICS COMMAREA TO UPDATE IFIS SECURITY TABLES            *
      *    ACCESS                                                      *
      ******************************************************************

       01  FSECUPD1-PARM-AREA.

         02  FSECUPD1-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  FSECUPD1-CHANNEL-ID            PIC X(08).
      *        *********************************************************
      *        * MUST BE 'FSECUPD1'                                    *
      *        *********************************************************

           05  FSECUPD1-VERS-RESP-CD          PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WFSM(WEB/IFIS SECR SYST)*
      *        *********************************************************
               88  FSECUPD1-RESPONSE-SUCCESSFUL        VALUE 'WFSM000I'.
               88  FSECUPD1-RESPONSE-ERROR             VALUE 'WFSM999E'.

         02  FSECUPD1-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

           05  FSECUPD1-CONTEXCP-USER-ID      PIC X(08).
           05  FSECUPD1-CONTEXCP-MODULE       PIC X(08).
           05  FSECUPD1-CONTEXCP-SCREEN       PIC X(08).
           05  FSECUPD1-CONTEXCP-FIELD-CODE   PIC X(08).
           05  FSECUPD1-CONTEXCP-ACTION       PIC X(01).
           05  FSECUPD1-CONTEXCP-ACTION-TBL OCCURS 20 TIMES.
               10  FSECUPD1-CONTEXCP-OPERATOR   PIC X(02).
               10  FSECUPD1-CONTEXCP-LV         PIC X(08).
               10  FSECUPD1-CONTEXCP-HV         PIC X(08).
               10  FSECUPD1-CONTEXCP-ADD-PERMSN PIC X(01).
               10  FSECUPD1-CONTEXCP-CHG-PERMSN PIC X(01).
               10  FSECUPD1-CONTEXCP-DEL-PERMSN PIC X(01).
           05  FSECUPD1-USERSCRN-ACTION       PIC X(01).
           05  FSECUPD1-USERSCRN-AUTH-FLAG    PIC X(01).
           05  FSECUPD1-USERSCRN-ADD-PERMSN   PIC X(01).
           05  FSECUPD1-USERSCRN-CHG-PERMSN   PIC X(01).
           05  FSECUPD1-USERSCRN-DEL-PERMSN   PIC X(01).

         02  FSECUPD1-OUTPUT-PARMS.
           05  FSECUPD1-OUTPUT-USER-ID        PIC X(08).
           05  FSECUPD1-PREFIX-TBL OCCURS 20 TIMES.
               10  FSECUPD1-OUTPUT-PREFIX     PIC X(08).

         02  FSECUPD1-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  FSECUPD1-RETURN-STATUS-CODE    PIC X(01).
      *        *********************************************************
      *        * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR    *
      *        *********************************************************

           05  FSECUPD1-RETURN-MSG-COUNT      PIC 9(01).
      *        *********************************************************
      *        * 1 = NUMBER OF RETURN-MSG-AREA(S)                      *
      *        *********************************************************

           05  FSECUPD1-RETURN-MSG-AREA.
      *        *********************************************************
      *        * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT         *
      *        * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT        *
      *        * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH       *
      *        *                 PARAGRAPH NAME, & ASSOC DB2 TABLE     *
      *        *********************************************************
               10  FSECUPD1-RETURN-MSG-NUM    PIC X(06).
               10  FSECUPD1-RETURN-MSG-FIELD  PIC X(30).
               10  FSECUPD1-RETURN-MSG-TEXT   PIC X(40).

