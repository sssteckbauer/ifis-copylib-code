      ******************************************************************
      *    **** COPYBOOK WGA016D - USED FOR JV DELETION SERVICE ****   *
      *                                                                *
      *    WEB/CICS COMMAREA TO REMOVE ALL ASSOCIATED JOURNAL VOUCHER  *
      *    ROWS WHEN JV DOCUMENT IS PASSED FOR JV REMOVAL.             *
      ******************************************************************

      ******************************************************************
      *    AFTER CHANGES ARE MADE TO THIS COPYBOOK, IT MUST BE COPIED  *
      *    INTO A WEBSPHERE DEVELOPER PROJECT FOLDER, FROM WHICH THE   *
      *    WEB SERVICE INTERFACE PROGRAM, WSDL, & WSBIND ARE GENERATED.*
      ******************************************************************

       01  DELJRNL-PARM-AREA.

         02  DELJRNL-CHANNEL-HEADER.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND AND OUTBOUND  *
      *      ***********************************************************

           05  DELJRNL-CHANNEL-ID             PIC X(08).
      *        *********************************************************
      *        * MUST BE 'DELJRNL'                                     *
      *        *********************************************************

           05  DELJRNL-VERS-RESP-CD           PIC X(08).
      *        *********************************************************
      *        * INCOMING: COMMAREA VERSION = 01.00.00                 *
      *        * OUTGOING: RETURN RESPONSE FOR WFGA(WEB/IFIS GA SYSTEM)*
      *        *********************************************************
               88  DELJRNL-RESPONSE-SUCCESSFUL         VALUE 'WFGA000I'.
               88  DELJRNL-RESPONSE-ERROR              VALUE 'WFGA999E'.

         02  DELJRNL-INPUT-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: INBOUND ONLY          *
      *      ***********************************************************

           05  DELJRNL-USER-CD                PIC X(08).
           05  DELJRNL-DOC-NBR                PIC X(08).

         02  DELJRNL-RETURN-STATUS-PARMS.
      *      ***********************************************************
      *      * SELECTED WSDL LANGUAGE STRUCTURE: OUTBOUND ONLY         *
      *      ***********************************************************

           05  DELJRNL-RETURN-STATUS-CODE     PIC X(01).
      *        *********************************************************
      *        * S = SUCCESSFUL, E = DATA ERROR(S), F = FATAL ERROR    *
      *        *********************************************************

           05  DELJRNL-RETURN-MSG-COUNT       PIC 9(01).
      *        *********************************************************
      *        * 1 = NUMBER OF RETURN-MSG-AREA(S)                      *
      *        *********************************************************

           05  DELJRNL-RETURN-MSG-AREA.
      *        *********************************************************
      *        * FOR STATUS 'S', SUCCESS MESSAGE NUMBER & TEXT         *
      *        * FOR STATUS 'E', ERROR MSG NUMBER, FIELD & TEXT        *
      *        * FOR STATUS 'F', FATAL SQLCODE, CURR-IO-NUM WITH       *
      *        *                 PARAGRAPH NAME, & ASSOC DB2 TABLE     *
      *        *********************************************************
               10  DELJRNL-RETURN-MSG-NUM     PIC X(06).
               10  DELJRNL-RETURN-MSG-FIELD   PIC X(30).
               10  DELJRNL-RETURN-MSG-TEXT    PIC X(40).

