      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM76                              04/24/00  12:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM76.
           02  ACTN-CODE-GROUP-CH76.
               03  ACTN-CODE-CH76           OCCURS 11   PIC X(1).
           02  RULE-CLASS-CODE-4180-CH76    OCCURS 11   PIC X(4).
           02  RULE-CLASS-DESC-4181-CH76    OCCURS 11   PIC X(35).
           02  RULE-CLASS-TYPE-4181-CH76    OCCURS 11   PIC X(1).
           02  STATUS-4181-CH76             OCCURS 11   PIC X(1).
           02  START-DATE-4181-CH76         OCCURS 11   PIC X(6).
           02  END-DATE-4181-CH76           OCCURS 11   PIC X(6).
