       01  USYWC10.
           02  UNVRS-CODE-SY10                          PIC X(2).
           02  COA-CODE-SY10                            PIC X(1).
           02  ACCT-CODE-SY10                           PIC X(6).
           02  TRANS-DATE-SY10                COMP-3    PIC S9(8).
           02  DEBIT-CRDT-IND-SY10                      PIC X(1).
           02  ENCMBRNC-NMBR-SY10                       PIC X(8).
           02  CNTRL-ACCT-CODE-SY10                     PIC X(6).
           02  CNTRL-ACCT-IND-SY10                      PIC X(1).
           02  CNTRL-ACCT-SEQ-NMBR-SY10                 PIC X(2).
           02  CNTRL-SEQ-NMBR-SY10                      PIC X(2).
           02  CY-PY-CNTRL-IND-SY10                     PIC X(1).
           02  CY-PY-BDGT-CNTRL-IND-SY10                PIC X(1).
           02  ENCM-FIELD-CODE-SY10                     PIC X(2).
           02  ENCM-NRML-SIGN-SY10                      PIC X(1).
           02  ERROR-IND-SY10                           PIC X(1).
           02  GL-DEBIT-CRDT-IND-SY10                   PIC X(1).
           02  JRNL-TYPE-SY10                           PIC X(4).
           02  OPAL-SIGN-SY10                           PIC X(1).
           02  PRCS-CODE-SY10                           PIC X(4).
           02  PRCS-FOUND-IND-SY10                      PIC X(1).
