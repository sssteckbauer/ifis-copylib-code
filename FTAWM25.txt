      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWM25                              04/24/00  12:45  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWM25.
           02  ACTN-CODE-GROUP-TA25.
               03  ACTN-CODE-TA25           OCCURS 12   PIC X(1).
           02  RCNCLTN-TYPE-4415-TA25       OCCURS 12   PIC X(4).
           02  SHORT-DESC-4415-TA25         OCCURS 12   PIC X(10).
           02  STNDD-LONG-DESC-4415-TA25    OCCURS 12   PIC X(35).
