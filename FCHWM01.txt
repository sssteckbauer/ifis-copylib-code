      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM01                              04/24/00  12:21  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM01.
           02  ACTN-CODE-CH01                           PIC X(1).
           02  NEXT-CHNG-DATE-CH01                      PIC X(6).
           02  STATUS-DESC-CH01                         PIC X(8).
           02  END-DATE-4076-CH01                       PIC X(6).
           02  ACTVY-DATE-4000-CH01                     PIC X(6).
           02  TIME-STAMP-4076-CH01                     PIC X(4).
           02  AM-PM-FLAG-CH01                          PIC X(1).
           02  ACTG-MTHD-4076-CH01                      PIC X(1).
           02  FSCL-YR-START-PRD-4076-CH01              PIC X(4).
           02  FSCL-YR-END-PRD-4076-CH01                PIC X(4).
           02  ACCT-INDX-BDGT-CNTRL-4076-CH01           PIC X(1).
           02  ACCT-INDX-BDGT-DESC-CH01                 PIC X(30).
           02  FUND-BDGT-CNTRL-4076-CH01                PIC X(1).
           02  FUND-BDGT-DESC-CH01                      PIC X(30).
           02  ORGZN-BDGT-CNTRL-4076-CH01               PIC X(1).
           02  ORGZN-BDGT-DESC-CH01                     PIC X(30).
           02  ACCT-BDGT-CNTRL-4076-CH01                PIC X(1).
           02  ACCT-BDGT-DESC-CH01                      PIC X(30).
           02  PRGRM-BDGT-CNTRL-4076-CH01               PIC X(1).
           02  PRGRM-BDGT-DESC-CH01                     PIC X(30).
           02  CNTRL-PRD-CODE-4076-CH01                 PIC X(1).
           02  CNTRL-PRD-DESC-4012-CH01                 PIC X(35).
           02  CNTRL-SVRTY-CODE-4076-CH01               PIC X(1).
           02  CNTRL-SVRTY-DESC-4012-CH01               PIC X(35).
           02  CLOSE-LDGR-RULE-4076-CH01                PIC X(4).
           02  RULE-CLASS-DESC-4181-CH01                PIC X(35).
           02  CMPLT-IND-4076-CH01                      PIC X(1).
