      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD03                              04/24/00  13:19  *
      *                                                               *
DEVBWS*  BWS - 06/04/01 ELIMINATE USE OF FCC-/FCCDBKTB- TBL/FLDS      *
      *---------------------------------------------------------------*
       01  FTAWD03.
           02  WORK-DATE-TA03                 COMP-3    PIC S9(8).
DEVBWS     02  SAVE-DBKEY-ID-4400-TA03                  PIC X(200).
DEVBWS     02  SAVE-DBKEY-ID-4404-TA03                  PIC X(8).
DEVBWS     02  SAVE-DBKEY-ID-4401-TA03                  PIC X(8).
           02  SAVE-DBKEY-ID-TA03             COMP      PIC S9(8).
           02  WORK-AMT-TA03                            PIC S9(6)V99.
           02  WORK-ADV-DATE-TA03             COMP-3    PIC S9(8).
           02  HOLD-RCNCLTN-DATE-TA03.
               03  HOLD-YEAR-DATE-TA03                  PIC 9(2).
               03  HOLD-MONTH-DATE-TA03                 PIC 9(2).
               03  HOLD-DAY-DATE-TA03                   PIC 9(2).
           02  HOLD-ADV-DATE-TA03                       PIC 9(6).
           02  SHORT-DESC-TA03                          PIC X(10).
           02  ADVNC-OK-SW-TA03                         PIC X(1).
           02  TMPLT-CHG-FLAG-TA03                      PIC X(1).
           02  EVENT-OK-SW-TA03                         PIC X(1).
           02  SUSP-FLAG-TA03                           PIC X(1).
           02  ERR-FLAG-TA03                            PIC X(1).
           02  SUSP-FLAG-TA03                           PIC X(1).
           02  TEST-DATE-TA03.
               03  YEAR-DATE-TA03                       PIC 9(2).
               03  MONTH-DATE-TA03                      PIC 9(2).
               03  DAY-DATE-TA03                        PIC 9(2).
           02  CONFIRM-ON-SW-TA03                       PIC X(1).
           02  END-APRVL-FLAG-TA03                      PIC X(1).
           02  FINAL-APRVL-FLAG-TA03                    PIC X(1).
           02  HOLD-EVENT-NMBR-TA03                     PIC X(8).
           02  DELETE-FLAG-TA03                         PIC X(1).
           02  APRVL-USED-FLAG-TA03                     PIC X(1).
           02  SHORT-DESC-N-TA03                        PIC X(10).
           02  NMBR-SWITCH-TA03                         PIC X(1).
           02  ADR-TYPE-FOUND-TA03                      PIC X(1).
           02  CHECK-NMBR-TA03                          PIC X(8).
           02  START-DATE-EVENT-TA03          COMP-3    PIC S9(8).
           02  END-DATE-EVENT-TA03            COMP-3    PIC S9(8).
           02  HOLD-RCNCLTN-DATE-TA03.
               03  YEAR-DATE-TA03                       PIC 9(2).
               03  MONTH-DATE-TA03                      PIC 9(2).
               03  DAY-DATE-TA03                        PIC 9(2).
           02  WORK-TEXT-ENTY-CODE-TA03.
               03  WORK-DCMNT-NMBR-TA03                 PIC X(8).
               03  WORK-TEXT-TYPE-TA03                  PIC X(7).
           02  ACCT-WARNING-IND-TA03                    PIC X(1).
           02  FUND-WARNING-IND-TA03                    PIC X(1).
UCSDX      02  WORK-CHK-DATE-TA03             COMP-3    PIC S9(8).
