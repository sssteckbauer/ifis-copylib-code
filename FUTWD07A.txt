      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWD07A                             04/24/00  13:23  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWD07A.
           02  SAVE-MINUS-ONE-UT07            COMP      PIC S9(8).
           02  SAVE-CLAUSE-DESC-UT07        OCCURS 14   PIC X(35).
           02  SYSTEM-DATE-UT07               COMP-3    PIC S9(8).
           02  SAVE-PLUS-ONE-UT07             COMP      PIC S9(8).
           02  SAVE-PAGE-STATUS-UT07                    PIC X(1).
           02  TEXT-TYPE-UT07                           PIC X(7).
           02  FIRST-CLAUSE-FLAG-UT07                   PIC X(1).
           02  FLAG-1-UT07                              PIC X(1).
           02  REFRESH-SW-UT07                          PIC X(1).
           02  COMMENT-START-UT07                       PIC X(1).
           02  COMMENT-END-UT07                         PIC X(1).
           02  DATA-FLAG-UT07                           PIC X(1).
           02  TOTAL-C-O-UT07                           PIC 9(3).
           02  TOTAL-C-O-WK-UT07                        PIC 9(3).
           02  TOTAL-ITEMS-UT07               COMP-3    PIC 9(4).
           02  TOTAL-ITEMS-WK-UT07            COMP-3    PIC 9(4).
           02  SAVE-PAGE-NUM-UT07             COMP-3    PIC S9(4).
           02  SAVE-SUB-UT07                  COMP-3    PIC 9(3).
           02  PRINT-FLAG-UT07                          PIC X(1).
           02  SUB-2-UT07                               PIC 9(3).
           02  SUB-4-UT07                               PIC 9(3).
           02  SUB-5-UT07                     COMP-3    PIC 9(4).
