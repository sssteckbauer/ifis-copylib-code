      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCOWM13                              04/24/00  12:27  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCOWM13.
           02  LONG-DESC-6258-CO13                      PIC X(30).
           02  LONG-DESC-6259-CO13                      PIC X(30).
           02  LONG-DESC-6260-CO13                      PIC X(30).
           02  COMM-PLAN-CODE-6273-CO13                 PIC X(4).
           02  COMM-TYPE-6273-CO13                      PIC X(4).
           02  COMM-DATE-6273-CO13                      PIC X(6).
           02  PRSN-ID-LAST-NINE-6157-CO13  OCCURS 8    PIC X(9).
           02  COMM-NAME-6274-CO13          OCCURS 8    PIC X(35).
           02  COMM-VARIABLE-6274-CO13      OCCURS 8    PIC X(15).
           02  ACTN-CODE-CO13               OCCURS 8    PIC X(01).
           02  FUND-CODE-CO13               OCCURS 8    PIC X(06).
           02  PREPARER-DOC-NBR-CO13        OCCURS 8    PIC X(08).
           02  PREPARER-DOC-SEQ-NBR-CO13    OCCURS 8    PIC 9(04).
