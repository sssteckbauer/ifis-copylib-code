       01  R4370-FAX.
           02  DB-PROC-ID-4370.
               03  USER-CODE-4370                       PIC X(8).
               03  LAST-ACTVY-DATE-4370                 PIC X(5).
               03  SYSTEM-TIME-STAMP-4370               PIC X(8).
               03  TRMNL-ID-4370                        PIC X(8).
               03  PURGE-FLAG-4370                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  FAX-TYPE-KEY-4370.
               03  UNVRS-CODE-4370                      PIC X(2).
               03  FAX-DCMNT-TYPE-4370                  PIC X(3).
               03  FAX-DCMNT-NMBR-4370                  PIC X(8).
               03  CHNG-SEQ-NMBR-4370                   PIC X(3).
               03  FAX-NMBR-4370                        PIC X(8).
           02  CNTCT-PID-4370.
               03  CNTCT-PID-DIGIT-ONE-4370             PIC X(1).
               03  CNTCT-PID-DIGIT-LAST-NINE-4370       PIC X(9).
           02  CNTCT-LINE1-4370                         PIC X(35).
           02  CNTCT-LINE2-4370                         PIC X(35).
           02  CNTCT-LINE3-4370                         PIC X(35).
           02  CNTCT-FAX-TLPHN-ID-4370.
               03  CNTCT-FAX-AREA-CODE-4370             PIC X(3).
               03  CNTCT-FAX-XCHNG-ID-4370              PIC X(3).
               03  CNTCT-FAX-SEQ-ID-4370                PIC X(4).
           02  CNTCT-OFC-TLPHN-ID-4370.
               03  CNTCT-OFC-AREA-CODE-4370             PIC X(3).
               03  CNTCT-OFC-XCHNG-ID-4370              PIC X(3).
               03  CNTCT-OFC-SEQ-ID-4370                PIC X(4).
               03  CNTCT-OFC-XTNSN-ID-4370              PIC X(4).
           02  FAXER-NAME-4370                          PIC X(35).
           02  FAXER-FAX-TLPHN-ID-4370.
               03  FAXER-FAX-AREA-CODE-4370             PIC X(3).
               03  FAXER-FAX-XCHNG-ID-4370              PIC X(3).
               03  FAXER-FAX-SEQ-ID-4370                PIC X(4).
           02  FAXER-OFC-TLPHN-ID-4370.
               03  FAXER-OFC-AREA-CODE-4370             PIC X(3).
               03  FAXER-OFC-XCHNG-ID-4370              PIC X(3).
               03  FAXER-OFC-SEQ-ID-4370                PIC X(4).
               03  FAXER-OFC-XTNSN-ID-4370              PIC X(4).
           02  FAX-RQST-DATE-4370             COMP-3    PIC S9(8).
           02  FAX-STAT-DATE-4370             COMP-3    PIC S9(8).
           02  FAX-STAT-TIME-STAMP-4370                 PIC X(6).
           02  AM-PM-FLAG-4370                          PIC X(1).
           02  FAX-INDEX-4370                           PIC X(10).
           02  FAX-STAT-CODE-4370                       PIC X(8).
           02  FAX-RQST-FLAG-4370                       PIC X(1).
           02  INTL-TLPHN-ID-4370                       PIC X(22).
           02  BATCH-ID-4370                            PIC X(8).
           02  FILLER02                                 PIC X(18).
