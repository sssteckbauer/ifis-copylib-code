      * /************************************************************/  $
      * /*   CHANGE LOG -- CPWSXBH1                                 */  $
      * /*   NAME            DATE      COMMENTS                     */  $
      * /*..........................................................*/  $
      * /*   B. ORTIZ       05/07/98   MODIFIED FOR GFAU (REL027)   */  $
      * /*                                                          */  $
      * /************************************************************/  $
      *01  XBH1-BUDGET-HEADER.                                          CPWSXBH1
           05  XBH1-KEY.                                                CPWSXBH1
      ****     10  XBH1-LOCATION               PIC X(02).               3202RRRR
      ****     10  XBH1-SAU                    PIC X(01).               3202RRRR
      ****     10  XBH1-SUB-CAMPUS             PIC X(01).               3202RRRR
      ****     10  XBH1-ACCOUNT                PIC X(07).               3202RRRR
      ****     10  XBH1-FUND                   PIC X(06).               3202RRRR
      ****     10  XBH1-SUB-BUDGET             PIC X(02).               3202RRRR
000810         10  XBH1-FAU.                                            3202RRRR
000820             15  XBH1-LOCATION           PIC X(02).               3202RRRR
000830             15  XBH1-SAU                PIC X(01).               3202RRRR
000840             15  XBH1-SUB-CAMPUS         PIC X(01).               3202RRRR
000850             15  XBH1-ACCOUNT            PIC X(07).               3202RRRR
000860             15  XBH1-FUND               PIC X(06).               3202RRRR
000870             15  XBH1-SUB-BUDGET         PIC X(02).               3202RRRR
               10  XBH1-SEGMENT-TYPE           PIC X(01).               CPWSXBH1
           05  XBH1-SUB-CAMPUS-ATTR            PIC X(01).               CPWSXBH1
           05  XBH1-ACCOUNT-TITLE              PIC X(35).               CPWSXBH1
           05  XBH1-FUND-ATTR                  PIC X(06).               CPWSXBH1
           05  XBH1-FUND-TITLE                 PIC X(35).               CPWSXBH1
           05  XBH1-FUND-TYPE                  PIC X(01).               CPWSXBH1
           05  XBH1-SUB-BUDGET-ATTR            PIC X(02).               CPWSXBH1
           05  XBH1-FUNCTION-CODE              PIC X(02).               CPWSXBH1
           05  XBH1-COLLEGE-CODE               PIC X(02).               CPWSXBH1
           05  XBH1-DEPARTMENT-CODE            PIC X(02).               CPWSXBH1
           05  XBH1-FILLER                     PIC X(09).               CPWSXBH1
           05  XBH1-IFIS-IFOAPAL.
               10  XBH1-IFIS-INDEX             PIC X(10).
               10  XBH1-IFIS-FUND              PIC X(06).
               10  XBH1-IFIS-ORG               PIC X(06).
               10  XBH1-IFIS-ACCOUNT           PIC X(06).
               10  XBH1-IFIS-PROGRAM           PIC X(06).
               10  XBH1-IFIS-ACTIVITY          PIC X(06).
               10  XBH1-IFIS-LOCATION          PIC X(06).
           05  XBH1-FILLER2                    PIC X(245).
