      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FSYWK00B                             04/24/00  12:55  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FSYWK00B.
           02  INDX-KEY-4019-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  MGR-PID-SY0B.
                   04  MGR-DIGIT-ONE-SY0B               PIC X(1).
                   04  MGR-LAST-NINE-SY0B               PIC X(9).
           02  INDX-KEY-4056-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  AGNCY-PID-SY0B.
                   04  AGNCY-DIGIT-ONE-SY0B             PIC X(1).
                   04  AGNCY-LAST-NINE-SY0B             PIC X(9).
           02  INDX-KEY-4057-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  MGR-PID-SY0B.
                   04  MGR-DIGIT-ONE-SY0B               PIC X(1).
                   04  MGR-LAST-NINE-SY0B               PIC X(9).
               03  RPRT-RQST-ID-SY0B                    PIC X(4).
           02  INDX-KEY-4073-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  VNDR-CODE-SY0B.
                   04  VNDR-ID-DIGIT-ONE-SY0B           PIC X(1).
                   04  VNDR-ID-LAST-NINE-SY0B           PIC X(9).
           02  INDX-KEY-4074-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  CMDTY-DESC-SY0B                      PIC X(35).
           02  INDX-KEY-4085-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  CMDTY-CODE-SY0B                      PIC X(8).
               03  CMDTY-DESC-SY0B                      PIC X(35).
               03  UNIT-MEA-CODE-SY0B                   PIC X(3).
           02  INDX-KEY-4155-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  SEQ-NMBR-SY0B                        PIC 9(4).
           02  INDX-KEY-4158-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  BANK-ACCT-CODE-SY0B                  PIC X(2).
               03  CHECK-NMBR-SY0B                      PIC X(8).
           02  INDX-KEY-4203-SY0B.
               03  UNVRS-CODE-SY0B                      PIC X(2).
               03  DCMNT-TYPE-SY0B                      PIC X(3).
