       01  R4056-AGNC.
           02  DB-PROC-ID-4056.
               03  USER-CODE-4056                       PIC X(8).
               03  LAST-ACTVY-DATE-4056                 PIC X(5).
               03  TRMNL-ID-4056                        PIC X(8).
               03  PURGE-FLAG-4056                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  AGNCY-KEY-4056.
               03  UNVRS-CODE-4056                      PIC X(2).
               03  INTRL-REF-ID-4056          COMP-3    PIC S9(7).
           02  AGNCY-PID-4056.
               03  AGNCY-DIGIT-ONE-4056                 PIC X(1).
               03  AGNCY-LAST-NINE-4056                 PIC X(9).
           02  ACTVY-DATE-4056                COMP-3    PIC S9(8).
           02  CNTCT-NAME-4056                          PIC X(35).
           02  STATUS-4056                              PIC X(1).
