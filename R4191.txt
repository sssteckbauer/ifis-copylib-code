       01  R4191-ENCMBR-HDR.
           02  DB-PROC-ID-4191.
               03  USER-CODE-4191                       PIC X(8).
               03  LAST-ACTVY-DATE-4191                 PIC X(5).
               03  TRMNL-ID-4191                        PIC X(8).
               03  PURGE-FLAG-4191                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ENCMBRNC-KEY-4191.
               03  UNVRS-CODE-4191                      PIC X(2).
               03  ENCMBRNC-NMBR-4191                   PIC X(8).
               03  DCMNT-TYPE-SEQ-NMBR-4191             PIC 9(4).
           02  ACTVY-DATE-4191                COMP-3    PIC S9(8).
           02  ENCMBRNC-DESC-4191                       PIC X(35).
           02  DCMNT-REF-NMBR-4191                      PIC X(10).
           02  ORGNL-ENCMBRNC-AMT-4191        COMP-3    PIC S9(10)V99.
           02  ENCMBRNC-TYPE-4191                       PIC X(1).
           02  ENCMBRNC-STATUS-4191                     PIC X(1).
           02  CMTMNT-TYPE-4191                         PIC X(1).
           02  CMTMNT-PCT-4191                COMP-3    PIC 9(3)V999.
           02  BDGT-DSPSN-4191                          PIC X(1).
           02  ESTBLD-DATE-4191               COMP-3    PIC S9(8).
           02  VNDR-CODE-4191.
               03  VNDR-ID-DIGIT-ONE-4191               PIC X(1).
               03  VNDR-ID-LAST-NINE-4191               PIC X(9).
           02  PO-IND-4191                              PIC X(1).
           02  HDR-ERROR-IND-4191                       PIC X(1).
           02  HDR-CNTR-ACCT-4191                       PIC 9(4).
           02  CMPLT-IND-4191                           PIC X(1).
           02  TRANS-DATE-4191                COMP-3    PIC S9(8).
           02  PSTNG-IND-4191                           PIC X(1).
           02  CHNG-SEQ-NMBR-4191                       PIC X(3).
           02  APRVL-IND-4191                           PIC X(1).
           02  LIEN-CLOSED-CODE-4191                    PIC X(1).
