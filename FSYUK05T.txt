       01  FSYUK05X-RECORD.
           05  FSYUK05X-UNVRS-CODE            PIC X(02).
           05  FSYUK05X-SYSTEM-DATE           PIC 9(08).
           05  FSYUK05X-MSG-TEXT              PIC X(35).
           05  FSYUK05X-DBKEY-4157            PIC S9(08) COMP.
           05  FSYUK05X-DBKEY-4073            PIC S9(08) COMP.
           05  FSYUK05X-PARM-KEY-2343         PIC X(08).
           05  FSYUK05X-PARM-DATA-2343        PIC X(10).
           05  FSYUK05X-FDRL-VNDR-CODE        PIC X(10).
           05  FSYUK05X-STATE-VNDR-CODE       PIC X(10).
           05  FSYUK05X-FDRL-ACCT-CODE        PIC X(10).
           05  FSYUK05X-STATE-ACCT-CODE       PIC X(10).
           05  FSYUK05X-ADDINV-IND            PIC X(01).
           05  FSYUK05X-END-OF-4170-SW        PIC X(01).
           05  FSYUK05X-DCMNT-NMBR            PIC X(10).
           05  FSYUK05X-N-DCMNT-NMBR REDEFINES FSYUK05X-DCMNT-NMBR
                                              PIC 9(10).
           05  FSYUK05X-FDRL-DCMNT-NMBR       PIC X(10).
           05  FSYUK05X-STATE-DCMNT-NMBR      PIC X(10).
           05  FSYUK05X-BANK-CODE             PIC X(02).
           05  FSYUK05X-FDRL-WTHHLD           PIC 9(06)V99 COMP-3.
           05  FSYUK05X-STATE-WTHHLD          PIC 9(06)V99 COMP-3.
           05  FSYUK05X-4152-FDRL-WTHHLD      PIC 9(06)V99 COMP-3.
           05  FSYUK05X-4152-STATE-WTHHLD     PIC 9(06)V99 COMP-3.
           05  FSYUK05X-4152-WTHHLD-AMT       PIC 9(06)V99 COMP-3.
           05  FSYUK05X-CHECK-AMT             PIC 9(10)V99 COMP-3.
           05  FSYUK05X-CMDTY-DESC.
               07  FSYUK05X-CMDTY-DESC-1      PIC X(22).
               07  FSYUK05X-CMDTY-DESC-2      PIC X(13).
           05  FSYUK05X-CHECK-NMBR            PIC X(08).
           05  FSYUK05X-4152-F-WTHHLD         PIC 9(10)V99 COMP-3.
           05  FSYUK05X-4152-S-WTHHLD         PIC 9(10)V99 COMP-3.
           05  FSYUK05X-FW-DIFF               PIC 9(03)V99 COMP-3.
           05  FSYUK05X-SW-DIFF               PIC 9(03)V99 COMP-3.
