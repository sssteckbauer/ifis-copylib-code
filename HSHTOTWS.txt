      *01  BUDGET-MASTER-HASH-AREA.
           05  HASH-INCOME-AMOUNTS     COMP-3.
               10  HASH-INC-INIT-CURR-YR-AMT PIC S9(11) VALUE +0.
               10  HASH-INC-PREV-YR-AMT      PIC S9(11) VALUE +0.
               10  HASH-INC-TRANS-AMT        PIC S9(11) VALUE +0.
               10  HASH-INC-ADJ-BUDGET-AMT   PIC S9(11) VALUE +0.
               10  HASH-INC-ADJ-TO-DTE-AMT   PIC S9(11) VALUE +0.
               10  HASH-INC-INC-TO-DTE-AMT   PIC S9(11) VALUE +0.

           05  HASH-INC-AMOUNTS REDEFINES
               HASH-INCOME-AMOUNTS     COMP-3.
               10  HASH-INC-AMOUNT OCCURS 6  PIC S9(11).

           05  HASH-APPROP-AMOUNTS     COMP-3.
               10  HASH-APR-INIT-CURR-YR-AMT PIC S9(11) VALUE +0.
               10  HASH-APR-PREV-YR-AMT      PIC S9(11) VALUE +0.
               10  HASH-APR-TRANS-AMT        PIC S9(11) VALUE +0.
               10  HASH-APR-ADJ-BUDGET-AMT   PIC S9(11) VALUE +0.
               10  HASH-APR-ADJ-TO-DTE-AMT   PIC S9(11) VALUE +0.
               10  HASH-APR-INC-TO-DTE-AMT   PIC S9(11) VALUE +0.

           05  HASH-APR-AMOUNTS REDEFINES
               HASH-APPROP-AMOUNTS     COMP-3.
               10  HASH-APR-AMOUNT OCCURS 6  PIC S9(11).

           05  HASH-APPROP-FTE         COMP-3.
               10  HASH-APR-INIT-CURR-YR-FTE PIC S9(07)V99 VALUE +0.
               10  HASH-APR-PREV-YR-FTE      PIC S9(07)V99 VALUE +0.
               10  HASH-APR-TRANS-FTE        PIC S9(07)V99 VALUE +0.
               10  HASH-APR-ADJ-BUDGET-FTE   PIC S9(07)V99 VALUE +0.
               10  HASH-APR-ADJ-TO-DTE-FTE   PIC S9(07)V99 VALUE +0.
               10  HASH-APR-INC-TO-DTE-FTE   PIC S9(07)V99 VALUE +0.

           05  HASH-APR-FTES REDEFINES
               HASH-APPROP-FTE         COMP-3.
               10  HASH-APR-FTE OCCURS 6     PIC S9(07)V99.

           05  HASH-INCOME-COUNTS      COMP-3.
               10  HASH-INC-INIT-CNT         PIC S9(07) VALUE +0.
               10  HASH-INC-TRANS-CNT        PIC S9(07) VALUE +0.
               10  HASH-INC-SUMRY-CNT        PIC S9(07) VALUE +0.
               10  HASH-INC-TOTAL-CNT        PIC S9(07) VALUE +0.

           05  HASH-INC-COUNTS REDEFINES
               HASH-INCOME-COUNTS      COMP-3.
               10  HASH-INC-COUNT OCCURS 4   PIC S9(07).

           05  HASH-APPROP-COUNTS      COMP-3.
               10  HASH-APR-INIT-CNT         PIC S9(07) VALUE +0.
               10  HASH-APR-TRANS-CNT        PIC S9(07) VALUE +0.
               10  HASH-APR-SUMRY-CNT        PIC S9(07) VALUE +0.
               10  HASH-APR-TOTAL-CNT        PIC S9(07) VALUE +0.

           05  HASH-APR-COUNTS REDEFINES
               HASH-APPROP-COUNTS      COMP-3.
               10  HASH-APR-COUNT OCCURS 4   PIC S9(07).

           05  HASH-TOTAL-AMOUNT   COMP-3    PIC S9(11) VALUE +0.
           05  HASH-TOTAL-COUNT    COMP-3    PIC S9(07) VALUE +0.
           05  HASH-SUB            COMP-3    PIC S9(02).
       EJECT
       01  HASH-HEAD1.
           05  H1-CC           PIC X(01)   VALUE '1'.
           05  H1-FLD1A        PIC X(08)   VALUE SPACES.
           05  H1-FLD1         PIC X(12)   VALUE '            '.
           05  H1-FLD2         PIC X(20)   VALUE '                    '.
           05  H1-FLD3         PIC X(20)   VALUE 'B U D G E T   M A S '.
           05  H1-FLD4         PIC X(20)   VALUE 'T E R   H A S H   T '.
           05  H1-FLD5         PIC X(20)   VALUE 'O T A L S           '.
           05  H1-FLD6         PIC X(20)   VALUE '                    '.
           05  H1-FLD7         PIC X(20)   VALUE '            '.

       01  HASH-HEAD2.
           05  H2-CC           PIC X(01)   VALUE '0'.
           05  H2-FLD1         PIC X(20)   VALUE '                    '.
           05  H2-FLD2         PIC X(20)   VALUE '               INCOM'.
           05  H2-FLD3         PIC X(20)   VALUE 'E                   '.
           05  H2-FLD4         PIC X(20)   VALUE '  APPROPRIATION     '.
           05  H2-FLD5         PIC X(20)   VALUE '                 TOT'.
           05  H2-FLD6         PIC X(20)   VALUE 'AL                  '.
           05  H2-FLD7         PIC X(20)   VALUE '            '.

       01  HASH-HEAD3.
           05  H3-CC           PIC X(01)   VALUE SPACE.
           05  H3-FLD1         PIC X(20)   VALUE '                    '.
           05  H3-FLD2         PIC X(20)   VALUE '               AMOUN'.
           05  H3-FLD3         PIC X(20)   VALUE 'TS                AM'.
           05  H3-FLD4         PIC X(20)   VALUE 'OUNTS         F.T.E.'.
           05  H3-FLD5         PIC X(20)   VALUE '                 AMO'.
           05  H3-FLD6         PIC X(20)   VALUE 'UNTS                '.
           05  H3-FLD7         PIC X(20)   VALUE '            '.

       01  HASH-AMOUNT-LINE.
           05  HA-CC           PIC X(01).
           05  HA-TITLE        PIC X(21).
           05  HA-FIL1         PIC X(09)   VALUE SPACES.
           05  HA-INC-AMT      PIC ZZ,ZZZ,ZZZ,ZZZ.
           05  HA-INC-AMT-CR   PIC X(02).
           05  HA-FIL2         PIC X(07)   VALUE SPACES.
           05  HA-APR-AMT      PIC ZZ,ZZZ,ZZZ,ZZZ.
           05  HA-APR-AMT-CR   PIC X(02).
           05  HA-FIL3         PIC X(02)   VALUE SPACES.
           05  HA-APR-FTE      PIC ZZ,ZZZ,ZZZ.ZZ.
           05  HA-APR-FTE-CR   PIC X(02).
           05  HA-FIL4         PIC X(07)   VALUE SPACES.
           05  HA-TOT-AMT      PIC ZZ,ZZZ,ZZZ,ZZZ.
           05  HA-TOT-AMT-CR   PIC X(02).
           05  HA-FIL5         PIC X(22)   VALUE SPACES.

       01  HASH-COUNT-LINE.
           05  HC-CC           PIC X(01)   VALUE SPACE.
           05  HC-TITLE        PIC X(21).
           05  HC-FIL1         PIC X(14)   VALUE SPACES.
           05  HC-INC-CNT      PIC Z,ZZZ,ZZZ.
           05  HC-FIL2         PIC X(14)   VALUE SPACES.
           05  HC-APR-CNT      PIC Z,ZZZ,ZZZ.
           05  HC-FIL3         PIC X(31)   VALUE SPACES.
           05  HC-TOT-CNT      PIC Z,ZZZ,ZZZ.

       01  HASH-AMOUNT-TITLE-TABLE.
           05  HASH-AMOUNT-TITLES.
               10  HAT1    PIC X(21) VALUE 'INITIAL CURRENT YEAR:'.
               10  HAT2    PIC X(21) VALUE 'PREVIOUS YEAR       :'.
               10  HAT4    PIC X(21) VALUE 'TRANSACTION         :'.
               10  HAT5    PIC X(21) VALUE 'ADJUSTED BUDGET     :'.
               10  HAT6    PIC X(21) VALUE 'ADJUSTMENTS TO DATE :'.
               10  HAT7    PIC X(21) VALUE 'INCREMENTS TO DATE  :'.
           05  HASH-AMT-TITLES REDEFINES
               HASH-AMOUNT-TITLES.
               10  HASH-AMOUNT-TITLE OCCURS 6 PIC X(21).

       01  HASH-COUNT-TITLE-TABLE.
           05  HASH-COUNT-TITLES.
               10  HC1     PIC X(21) VALUE 'INITIAL             :'.
               10  HC2     PIC X(21) VALUE 'TRANSACTION         :'.
               10  HC3     PIC X(21) VALUE 'SUMMARY             :'.
               10  HC4     PIC X(21) VALUE 'TOTAL               :'.
           05  HASH-CNT-TITLES REDEFINES
               HASH-COUNT-TITLES.
               10  HASH-COUNT-TITLE OCCURS 4 PIC X(21).
       01  HOLD-HASH-HEAD.
           05  FILLER                        PIC X.
           05  HOLD-PROG-NAME                PIC X(7) VALUE SPACES.
           05  FILLER                        PIC X(125) VALUE SPACES.
       01  HOLD-EXTRACT                      PIC X      VALUE SPACES.
