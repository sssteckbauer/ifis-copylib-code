       01  LINKIK20-RECORD.
           03  LINKIK20-UNVRS-CODE            PIC X(02).
           03  LINKIK20-SYSTEM-DATE           PIC 9(08).
           03  LINKIK20-BANK-CODE             PIC X(02).
           03  LINKIK20-DCMNT-NMBR            PIC X(08).
           03  LINKIK20-CHECK-AMT             PIC 9(10)V99 COMP-3.
           03  LINKIK20-CHECK-NMBR            PIC X(08).
           03  LINKIK20-DBKEY-4073            PIC S9(08) COMP.
           03  LINKIK20-DBKEY-4157            PIC S9(08) COMP.
           03  LINKIK20-PARM-KEY-2343         PIC X(08).
           03  LINKIK20-PARM-DATA-2343        PIC X(10).
           03  LINKIK20-ADDINV-IND            PIC X(01).
           03  LINKIK20-END-OF-4170-SW        PIC X(01).
           03  LINKIK20-FDRL-VNDR-CODE        PIC X(10).
           03  LINKIK20-FDRL-DCMNT-NMBR       PIC X(10).
           03  LINKIK20-FDRL-ACCT-CODE        PIC X(10).
           03  LINKIK20-FDRL-WTHHLD           PIC 9(06)V99 COMP-3.
           03  LINKIK20-STATE-VNDR-CODE       PIC X(10).
           03  LINKIK20-STATE-DCMNT-NMBR      PIC X(10).
           03  LINKIK20-STATE-ACCT-CODE       PIC X(10).
           03  LINKIK20-STATE-WTHHLD          PIC 9(06)V99 COMP-3.
           03  LINKIK20-4152-FDRL-WTHHLD      PIC 9(06)V99 COMP-3.
           03  LINKIK20-4152-F-WTHHLD         PIC 9(10)V99 COMP-3.
           03  LINKIK20-FW-DIFF               PIC 9(03)V99 COMP-3.
           03  LINKIK20-4152-STATE-WTHHLD     PIC 9(06)V99 COMP-3.
           03  LINKIK20-4152-S-WTHHLD         PIC 9(10)V99 COMP-3.
           03  LINKIK20-SW-DIFF               PIC 9(03)V99 COMP-3.
           03  LINKIK20-4152-WTHHLD-AMT       PIC 9(06)V99 COMP-3.
           03  LINKIK20-CMDTY-DESC.
               07  LINKIK20-CMDTY-DESC-1      PIC X(27).
               07  LINKIK20-CMDTY-DESC-2      PIC X(08).
           03  LINKIK20-MSG-TEXT              PIC X(35).
