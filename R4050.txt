       01  R4050-ACTV.
           02  DB-PROC-ID-4050.
               03  USER-CODE-4050                       PIC X(8).
               03  LAST-ACTVY-DATE-4050                 PIC X(5).
               03  TRMNL-ID-4050                        PIC X(8).
               03  PURGE-FLAG-4050                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ACTVY-KEY-4050.
               03  UNVRS-CODE-4050                      PIC X(2).
               03  COA-CODE-4050                        PIC X(1).
               03  ACTVY-CODE-4050                      PIC X(6).
           02  ACTVY-DATE-4050                COMP-3    PIC S9(8).
