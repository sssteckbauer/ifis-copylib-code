       01  R4032-COST-APPL.
           02  DB-PROC-ID-4032.
               03  USER-CODE-4032                       PIC X(8).
               03  LAST-ACTVY-DATE-4032                 PIC X(5).
               03  TRMNL-ID-4032                        PIC X(8).
               03  PURGE-FLAG-4032                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  COST-SHARE-FROM-ACCT-4032                PIC X(6).
           02  COST-SHARE-TO-ACCT-4032                  PIC X(6).
           02  COST-SHARE-PCT-4032            COMP-3    PIC 9(3)V999.
