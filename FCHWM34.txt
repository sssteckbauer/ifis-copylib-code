      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM34                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM34.
           02  FUND-TYPE-DESC-4009-CH34                 PIC X(35).
           02  ACTVY-DATE-4008-CH34                     PIC X(6).
           02  TIME-STAMP-4009-CH34                     PIC X(4).
           02  AM-PM-FLAG-CH34                          PIC X(1).
           02  ENCMBRNC-JRNL-TYPE-4009-CH34             PIC X(4).
           02  ENCMBRNC-JRNL-DESC-4180-CH34             PIC X(35).
           02  CMTMNT-TYPE-4009-CH34                    PIC X(1).
           02  ROLL-BDGT-IND-4009-CH34                  PIC X(1).
           02  BDGT-DSPSN-4009-CH34                     PIC X(1).
           02  ENCMBRNC-PCT-4009-CH34                   PIC X(7).
           02  BDGT-JRNL-TYPE-4009-CH34                 PIC X(4).
           02  BDGT-JRNL-DESC-4181-CH34                 PIC X(35).
           02  CARRY-FRWRD-TYPE-4009-CH34               PIC X(1).
           02  BDGT-CLSFN-4009-CH34                     PIC X(1).
           02  BDGT-PCT-4009-CH34                       PIC X(7).
