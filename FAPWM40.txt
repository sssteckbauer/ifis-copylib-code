      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM40                              04/24/00  12:17  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM40.
           02  ACTN-CODE-AP40                           PIC X(1).
           02  SUB-DCMNT-TYPE-AP40                      PIC X(3).
           02  DCMNT-REF-NMBR-AP40                      PIC X(10).
           02  TRANS-DATE-4150-AP40                     PIC X(6).
           02  ADR-TYPE-CODE-4150-AP40                  PIC X(2).
           02  1099-IND-4150-AP40                       PIC X(1).
           02  VNDR-NAME-GP-AP40                        PIC X(35).
S41335     02  LINE-ADR-AP40                OCCURS 3    PIC X(35).
           02  CITY-NAME-AP40                           PIC X(18).
           02  STATE-CODE-AP40                          PIC X(2).
           02  ZIP-CODE-AP40                            PIC X(10).
           02  NO-TAX-IND-AP40                          PIC X(1).
           02  SALES-TAX-IND-AP40                       PIC X(1).
           02  USE-TAX-IND-AP40                         PIC X(1).
           02  APRVD-AMT-AP40                           PIC X(13).
           02  TAX-AMT-AP40                             PIC X(13).
           02  ADDL-CHRG-AP40                           PIC X(13).
           02  NET-AMT-AP40                             PIC X(13).
           02  TEXT-IND-AP40                            PIC X(1).
           02  CHECK-GRPNG-IND-4150-AP40                PIC X(1).
           02  CHECK-NMBR-AP40                          PIC X(8).
           02  CHECK-DATE-AP40                          PIC X(8).
           02  CMPLT-IND-4150-AP40                      PIC X(1).
           02  APRVL-IND-4150-AP40                      PIC X(1).
           02  APRVL-TMPLT-CODE-AP40                    PIC X(3).
           02  CNCL-IND-4150-AP40                       PIC X(1).
           02  DELETE-LIT-AP40                          PIC X(10).
           02  CONFIRM-DELETE-AP40                      PIC X(1).
           02  CHECK-TEXT-AP40              OCCURS 3    PIC X(55).
FP2006     02  FORCE-CHK-AP40                           PIC X(1).
           02  ITEM-ACTN-CODE-AP40                      PIC X(1).
           02  SEQ-NMBR-AP40                  COMP-3    PIC 9(4).
           02  COA-CODE-AP40                            PIC X(1).
           02  ACCT-INDX-CODE-4152-AP40                 PIC X(10).
           02  FUND-CODE-4152-AP40                      PIC X(6).
           02  ORGZN-CODE-4152-AP40                     PIC X(6).
           02  ACCT-CODE-4152-AP40                      PIC X(6).
           02  PRGRM-CODE-4152-AP40                     PIC X(6).
           02  ACTVY-CODE-4152-AP40                     PIC X(6).
           02  LCTN-CODE-4152-AP40                      PIC X(6).
           02  ACCT-APRVD-AMT-AP40                      PIC X(13).
           02  ACCT-TAX-AMT-AP40                        PIC X(13).
           02  ACCT-ADDL-CHRG-AP40                      PIC X(13).
           02  ACCT-NET-AMT-AP40                        PIC X(13).
           02  VNDR-INV-NMBR-4150-AP40                  PIC X(9).
           02  OPEN-PAID-IND-4150-AP40                  PIC X(1).
           02  CLAUSE-CODE-4169-AP40                    PIC X(8).
           02  INCM-TYPE-SEQ-NMBR-4073-AP40             PIC 9(4).
           02  592-IND-AP40                             PIC X(1).
S41335     02  FED-WH-AMT-AP40                          PIC X(13).
S41335     02  FED-RPT-AMT-AP40                         PIC X(13).
           02  CA-WH-AMT-AP40                           PIC X(13).
           02  CA-RPT-AMT-AP40                          PIC X(13).
FP7157     02  TAX-TREATY-IND-AP40                      PIC X(1).
FP7157     02  IRS-INCOME-CD-AP40                       PIC X(2).
FP7157     02  INC-TYP-AP40                             PIC X(2).
