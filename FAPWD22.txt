      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD22                              04/24/00  12:53  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD22.
           02 FAPWD22-SCREEN-KEY.
               03 SAVE-DBKEY-ID-4073-AP22
                                                OCCURS 13 PIC X(05).
               03 ADD-FLAG-AP22                         PIC X(1).
               03 LISTVAL-FLAG-AP22                     PIC X(1).
               03 ACTN-ERR-FLAG-AP22                    PIC X(1).
           02  FAPWD22-SCROLL-KEY.
               03  SAVE-DBKEY-ID-PLUS-ONE-AP22              PIC X(05).
               03  SAVE-DBKEY-ID-MINUS-ONE-AP22             PIC X(05).
