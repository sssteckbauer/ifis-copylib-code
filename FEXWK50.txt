      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWK50                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWK50.
           02  VNDR-CODE-EX50.
               03  VNDR-ID-DIGIT-ONE-EX50               PIC X(1).
               03  VNDR-ID-LAST-NINE-EX50               PIC X(9).
           02  VNDR-NAME-EX50                           PIC X(35).
           02  CMDTY-CODE-EX50                          PIC X(8).
           02  CMDTY-DESC-EX50                          PIC X(35).
           02  ACCT-CODE-EX50                           PIC X(6).
           02  ACCT-CODE-TITLE-EX50                     PIC X(35).
           02  FILE-NMBR-EX50                           PIC 9(5).
           02  ADR-TYPE-CODE-EX50                       PIC X(2).
           02  FILE-DATE-EX50                 COMP-3    PIC S9(8).
           02  STMNT-START-DATE-EX50          COMP-3    PIC S9(8).
           02  STMNT-END-DATE-EX50            COMP-3    PIC S9(8).
           02  HDR-ERROR-IND-EX50                       PIC X(1).
           02  DTL-ERROR-IND-EX50                       PIC X(1).
           02  REL-HDR-ERROR-IND-EX50                   PIC X(1).
           02  REL-DTL-ERROR-IND-EX50                   PIC X(1).
           02  RELEASE-NMBR-EX50                        PIC X(14).
           02  LINE-NMBR-EX50                           PIC 9(4).
