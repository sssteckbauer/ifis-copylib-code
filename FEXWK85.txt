      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWK85                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWK85.
           02  VNDR-CODE-EX85.
               03  VNDR-ID-DIGIT-ONE-EX85               PIC X(1).
               03  VNDR-ID-LAST-NINE-EX85               PIC X(9).
           02  VNDR-NAME-EX85                           PIC X(35).
           02  STMNT-NMBR-EX85                          PIC X(8).
           02  REQ-RELEASE-NMBR-EX85                    PIC X(14).
           02  STMNT-RUN-DATE-EX85            COMP-3    PIC S9(8).
           02  CHECK-NMBR-EX85                          PIC X(8).
