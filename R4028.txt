       01  R4028-INDRT-RCPT.
           02  DB-PROC-ID-4028.
               03  USER-CODE-4028                       PIC X(8).
               03  LAST-ACTVY-DATE-4028                 PIC X(5).
               03  TRMNL-ID-4028                        PIC X(8).
               03  PURGE-FLAG-4028                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  DSTBN-PCT-4028                 COMP-3    PIC 9(3)V999.
           02  ACCT-INDX-CODE-4028                      PIC X(10).
           02  FUND-CODE-4028                           PIC X(6).
           02  ORGZN-CODE-4028                          PIC X(6).
           02  ACCT-CODE-4028                           PIC X(6).
           02  PRGRM-CODE-4028                          PIC X(6).
           02  ACTVY-CODE-4028                          PIC X(6).
           02  LCTN-CODE-4028                           PIC X(6).
           02  INDRT-COST-ACCT-4028                     PIC X(6).
