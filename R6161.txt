       01  R6161-LSTASGNPID.
           02  DB-PROC-ID-6161.
               03  USER-CODE-6161                       PIC X(8).
               03  LAST-ACTVY-DATE-6161                 PIC X(5).
               03  TRMNL-ID-6161                        PIC X(8).
               03  PURGE-FLAG-6161                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  UNVRS-CODE-6161                          PIC X(2).
           02  ALPHA-PREFIX-6161                        PIC X(1).
           02  LAST-ASSGND-PID-6161           COMP-3    PIC S9(7).
