      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPUWM07                              04/24/00  12:39  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPUWM07.
           02  BUYER-NAME-4072-PU07                     PIC X(35).
           02  ACTN-CODE-GROUP-PU07.
               03  ACTN-CODE-PU07           OCCURS 12   PIC X(1).
           02  RQST-CODE-4070-PU07          OCCURS 12   PIC X(8).
           02  ITEM-NMBR-4071-PU07          OCCURS 12   PIC X(4).
           02  CMDTY-CODE-4074-PU07         OCCURS 12   PIC X(8).
           02  CMDTY-DESC-4071-PU07         OCCURS 12   PIC X(35).
           02  PO-NMBR-4083-PU07            OCCURS 12   PIC X(8).
           02  CHNG-SEQ-NMBR-4096-PU07      OCCURS 12   PIC X(3).
           02  ITEM-NMBR-4085-PU07          OCCURS 12   PIC X(4).
           02  CON-IND-PU07                 OCCURS 12   PIC X(1).
           02  TRANS-DATE-PU07       COMP-3 OCCURS 12   PIC S9(8).
           02  DLVRY-DATE-PU07       COMP-3 OCCURS 12   PIC S9(8).
