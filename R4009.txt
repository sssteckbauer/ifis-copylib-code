       01  R4009-FNDT-EFCTV.
           02  DB-PROC-ID-4009.
               03  USER-CODE-4009                       PIC X(8).
               03  LAST-ACTVY-DATE-4009                 PIC X(5).
               03  TRMNL-ID-4009                        PIC X(8).
               03  PURGE-FLAG-4009                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EFCTV-RCRD-KEY-4009.
               03  START-DATE-4009            COMP-3    PIC S9(8).
               03  TIME-STAMP-4009                      PIC X(6).
           02  END-DATE-4009                  COMP-3    PIC S9(8).
           02  STATUS-4009                              PIC X(1).
           02  FUND-TYPE-DESC-4009                      PIC X(35).
           02  INTRL-FUND-TYPE-CODE-4009                PIC X(2).
           02  CPTLZN-FUND-CODE-4009                    PIC X(6).
           02  CPTLZN-ACCT-CODE-4009                    PIC X(6).
           02  ACCT-INDX-BDGT-CNTRL-4009                PIC X(1).
           02  FUND-BDGT-CNTRL-4009                     PIC X(1).
           02  ORGZN-BDGT-CNTRL-4009                    PIC X(1).
           02  ACCT-BDGT-CNTRL-4009                     PIC X(1).
           02  PRGRM-BDGT-CNTRL-4009                    PIC X(1).
           02  CNTRL-PRD-CODE-4009                      PIC X(1).
           02  CNTRL-SVRTY-CODE-4009                    PIC X(1).
           02  PREDCSR-FUND-TYPE-4009                   PIC X(2).
           02  SBRDT-FUND-TYPE-4009                     PIC X(2).
           02  DFLT-FROM-IND-4009                       PIC X(1).
           02  ENCMBRNC-JRNL-TYPE-4009                  PIC X(4).
           02  CMTMNT-TYPE-4009                         PIC X(1).
           02  ROLL-BDGT-IND-4009                       PIC X(1).
           02  BDGT-DSPSN-4009                          PIC X(1).
           02  ENCMBRNC-PCT-4009              COMP-3    PIC 9(3)V999.
           02  BDGT-JRNL-TYPE-4009                      PIC X(4).
           02  BDGT-CLSFN-4009                          PIC X(1).
           02  CARRY-FRWRD-TYPE-4009                    PIC X(1).
           02  BDGT-PCT-4009                  COMP-3    PIC 9(3)V999.
