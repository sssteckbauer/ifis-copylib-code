       01  R4231-RLSE-EQP.
           02  DB-PROC-ID-4231.
               03  USER-CODE-4231                       PIC X(8).
               03  LAST-ACTVY-DATE-4231                 PIC X(5).
               03  TRMNL-ID-4231                        PIC X(8).
               03  PURGE-FLAG-4231                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  MFR-NAME-4231                            PIC X(35).
           02  MODEL-NMBR-4231                          PIC X(30).
