       01  R4230-RLSE-DTL.
           02  DB-PROC-ID-4230.
               03  USER-CODE-4230                       PIC X(8).
               03  LAST-ACTVY-DATE-4230                 PIC X(5).
               03  TRMNL-ID-4230                        PIC X(8).
               03  PURGE-FLAG-4230                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  LINE-NMBR-4230                           PIC 9(4).
           02  CATALOG-NMBR-4230                        PIC X(14).
           02  CATALOG-DESC-4230                        PIC X(35).
           02  QTY-4230                       COMP-3    PIC S9(6)V99.
           02  EXPR-UNIT-MEA-CODE-4230                  PIC X(4).
           02  UNIT-PRICE-4230                COMP-3    PIC S9(10)V9999.
           02  TOTAL-AMT-4230                 COMP-3    PIC S9(10)V99.
           02  DTL-ERROR-IND-4230                       PIC X(1).
           02  PART-NMBR-4230                           PIC X(15).
