      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWK02                              04/24/00  12:45  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWK02.
           02  TRVL-ACCT-LAST-NINE-4400-TA02            PIC X(9).
           02  ADR-TYPE-CODE-6137-TA02                  PIC X(2).
           02  MAP-RQST-DATE-TA02                       PIC X(8).
           02  MAP-RQST-DATE-NMRC-TA02        COMP-3    PIC S9(8).
