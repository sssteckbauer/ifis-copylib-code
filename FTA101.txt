      *======================= START  FTA101 ======================*    01180010
      *         TRAVEL EVENT ACTIVITY RECORD LAYOUT                  *  01180010
      *==============================================================*  01180010
       01  FTA101-EXTRACT-O.                                            02360036
           03  FTA101-SORT-KEY.                                         02360036
               05  FTA101-FIXED-KEY.                                    02360036
                   07  FTA101-UNVRS-ID            PICTURE X(02).        02370036
                   07  FTA101-USER-ID             PICTURE X(08).        02390036
                   07  FTA101-REPORT-ID           PICTURE X(08).        02390036
                   07  FTA101-REQUEST-NO          PICTURE 9(04).        02380036
               05  FTA101-VARIABLE-KEY.                                 02400036
                   07  FTA101-ACCT-NAME-KEY       PICTURE X(35).        02360036
                   07  FTA101-TRVL-ACCT-PID.                            02380036
                       09  FTA101-TRVL-ACCT-DIGIT-ONE                   02380036
                                                  PICTURE X(01).        02400036
                       09  FTA101-TRVL-ACCT-LAST-NINE                   02380036
                                                  PICTURE X(09).        02400036
                   07  FTA101-EVENT-NMBR          PICTURE X(08).        02360036
                   07  FTA101-REC-NMBR            PICTURE X(01).        02360036
                   07  FTA101-SORT-NMBR           PICTURE X(08).        02360036
                   07  FTA101-SEQ-NMBR            PICTURE 9(04).        02360036
                   07  FTA101-REPORT-TYPE         PICTURE X(01).        02360036
               05  FTA101-REC-TYPE                PICTURE X(01).        02400036
       03  FTA101-DATA.                                                 02410036
           05  FTA101-VARIABLE-DATA               PICTURE X(350).       02410036
           05  FTA101-REC-A      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-INST-TITLE              PICTURE X(35).
               07  FTA101-AS-OF-DATE              PICTURE X(08).
               07  FTA101-USER-NAME               PICTURE X(35).
               07  FILLER                         PICTURE X(272).
           05  FTA101-REC-B      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-NAME-KEY                PICTURE X(35).        02410036
               07  FTA101-LINE-ADR                OCCURS 4 TIMES
                                                  PICTURE X(35).
               07  FTA101-CITY-NAME               PICTURE X(18).
               07  FTA101-STATE-CODE              PICTURE X(02).
               07  FTA101-ZIP-CODE                PICTURE X(10).
               07  FTA101-CNTRY-NAME              PICTURE X(30).
               07  FTA101-EMPLY-IND               PICTURE X(01).
               07  FILLER                         PICTURE X(114).
           05  FTA101-REC-C      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-EVENT-DESC              PICTURE X(35).        02410036
               07  FTA101-EVENT-STATUS            PICTURE X(15).
               07  FTA101-EVENT-BAL               PICTURE S9(10)V99.
               07  FTA101-EVENT-START-DATE        PICTURE X(08).
               07  FTA101-EVENT-END-DATE          PICTURE X(08).
               07  FTA101-RCNCLTN-DUE-DATE        PICTURE X(08).
               07  FTA101-AIR-DRCT-BILL-IND       PICTURE X(01).
               07  FTA101-CAR-DRCT-BILL-IND       PICTURE X(01).
               07  FTA101-HOTL-DRCT-BILL-IND      PICTURE X(01).
               07  FTA101-MEAL-DRCT-BILL-IND      PICTURE X(01).
               07  FTA101-OTHR-DRCT-BILL-IND      PICTURE X(01).
               07  FTA101-TOTL-ESTMD-EXPNS-AMT    PICTURE S9(10)V99.
               07  FTA101-TOTL-ADVNC-AMT          PICTURE S9(10)V99.
               07  FTA101-DRCT-BILL-AMT           PICTURE S9(10)V99.
               07  FTA101-TOTL-EXPNS-AMT          PICTURE S9(10)V99.
               07  FTA101-RCNCLTN-TYPE            PICTURE X(04).
               07  FTA101-RCNCLTN-TYPE-DESC       PICTURE X(35).
               07  FTA101-RCNCLTN-STATUS          PICTURE X(15).
               07  FTA101-RCNCLTN-AMT             PICTURE S9(06)V99.
               07  FTA101-RCNCLTN-CHECK-NMBR      PICTURE X(08).
               07  FTA101-RCNCLTN-CHECK-DATE      PICTURE X(08).
               07  FTA101-RECON-COA-CODE          PICTURE X(01).
               07  FTA101-RECON-ACCT-INDX-CODE    PICTURE X(10).
               07  FTA101-RECON-FUND-CODE         PICTURE X(06).
               07  FTA101-RECON-ORGZN-CODE        PICTURE X(06).
               07  FTA101-RECON-ACCT-CODE         PICTURE X(06).
               07  FTA101-RECON-PRGRM-CODE        PICTURE X(06).
               07  FTA101-RECON-ACTVY-CODE        PICTURE X(06).
               07  FTA101-RECON-LCTN-CODE         PICTURE X(06).
               07  FTA101-RECON-CHNG-SEQ-NMBR     PICTURE X(03).
               07  FTA101-RECON-APRVL-IND         PICTURE X(01).
               07  FTA101-RECON-APRVL-ID          PICTURE X(08).
               07  FTA101-RECON-USER-ID           PICTURE X(08).
               07  FTA101-RECON-APRVL-TMPLT-CODE  PICTURE X(03).
               07  FTA101-RECON-LEVEL-SEQ-NMBR    PICTURE 9(04).
               07  FTA101-RECON-APRVL-CODE        PICTURE X(08).
               07  FTA101-RECON-ALTRNT-APRVL-CODE PICTURE X(08).
               07  FTA101-RECON-APRVL-DATE        PICTURE X(08).
               07  FTA101-RECON-APRVL-TIME        PICTURE X(6).
               07  FILLER                         PICTURE X(29).
           05  FTA101-REC-D      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-EVENT-TEXT              PICTURE X(55).        02410036
               07  FILLER                         PICTURE X(295).       02410036
           05  FTA101-REC-E      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-TRIP-DESC               PICTURE X(35).        02410036
               07  FTA101-TRIP-STATUS             PICTURE X(15).
               07  FTA101-TRIP-START-DATE         PICTURE X(08).
               07  FTA101-TRIP-END-DATE           PICTURE X(08).
               07  FTA101-ESTMD-EXPNS-AMT         PICTURE S9(10)V99.
               07  FTA101-TRIP-CNCL-IND           PICTURE X(01).
               07  FTA101-TRIP-CNCL-DATE          PICTURE X(08).
               07  FTA101-TRIP-COA-CODE           PICTURE X(01).
               07  FTA101-TRIP-ACCT-INDX-CODE     PICTURE X(10).
               07  FTA101-TRIP-FUND-CODE          PICTURE X(06).
               07  FTA101-TRIP-ORGZN-CODE         PICTURE X(06).
               07  FTA101-TRIP-ACCT-CODE          PICTURE X(06).
               07  FTA101-TRIP-PRGRM-CODE         PICTURE X(06).
               07  FTA101-TRIP-ACTVY-CODE         PICTURE X(06).
               07  FTA101-TRIP-LCTN-CODE          PICTURE X(06).
               07  FTA101-TRIP-ACCT-ERROR-IND     PICTURE X(01).
               07  FTA101-TRIP-ENCMBRNC-AMT       PICTURE S9(10)V99.
               07  FTA101-TRIP-RULE-CLASS-CODE    PICTURE X(04).
               07  FTA101-TRIP-CHNG-SEQ-NMBR      PICTURE X(03).
               07  FTA101-TRIP-APRVL-IND          PICTURE X(01).
               07  FTA101-TRIP-APRVL-ID           PICTURE X(08).
               07  FTA101-TRIP-USER-ID            PICTURE X(08).
               07  FTA101-TRIP-APRVL-TMPLT-CODE   PICTURE X(03).
               07  FTA101-TRIP-LEVEL-SEQ-NMBR     PICTURE 9(04).
               07  FTA101-TRIP-APRVL-CODE         PICTURE X(08).
               07  FTA101-TRIP-ALTRNT-APRVL-CODE  PICTURE X(08).
               07  FTA101-TRIP-APRVL-DATE         PICTURE X(08).
               07  FTA101-TRIP-APRVL-TIME         PICTURE X(6).
               07  FILLER                         PICTURE X(142).
           05  FTA101-REC-F      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-TRIP-TEXT               PICTURE X(55).        02410036
               07  FILLER                         PICTURE X(295).       02410036
           05  FTA101-REC-G      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-ADVNC-STATUS            PICTURE X(15).        02410036
               07  FTA101-ADVNC-DATE              PICTURE X(08).
               07  FTA101-ADVNC-TYPE              PICTURE X(08).
               07  FTA101-ADVNC-TYPE-DESC         PICTURE X(35).
               07  FTA101-ADVNC-AMT               PICTURE S9(06)V99.
               07  FTA101-ADVNC-CHECK-NMBR        PICTURE X(08).
               07  FTA101-ADVNC-CHECK-DATE        PICTURE X(08).
               07  FTA101-ADVNC-CNCL-IND          PICTURE X(01).
               07  FTA101-ADVNC-CNCL-DATE         PICTURE X(08).
               07  FTA101-ADVNC-RULE-CLASS-CODE   PICTURE X(04).
               07  FTA101-ADVNC-COA-CODE          PICTURE X(01).
               07  FTA101-ADVNC-ACCT-INDX-CODE    PICTURE X(10).
               07  FTA101-ADVNC-FUND-CODE         PICTURE X(06).
               07  FTA101-ADVNC-ORGZN-CODE        PICTURE X(06).
               07  FTA101-ADVNC-ACCT-CODE         PICTURE X(06).
               07  FTA101-ADVNC-PRGRM-CODE        PICTURE X(06).
               07  FTA101-ADVNC-ACTVY-CODE        PICTURE X(06).
               07  FTA101-ADVNC-LCTN-CODE         PICTURE X(06).
               07  FTA101-ADVNC-CHNG-SEQ-NMBR     PICTURE X(03).
               07  FTA101-ADVNC-APRVL-IND         PICTURE X(01).
               07  FTA101-ADVNC-APRVL-ID          PICTURE X(08).
               07  FTA101-ADVNC-USER-ID           PICTURE X(08).
               07  FTA101-ADVNC-APRVL-TMPLT-CODE  PICTURE X(03).
               07  FTA101-ADVNC-LEVEL-SEQ-NMBR    PICTURE 9(04).
               07  FTA101-ADVNC-APRVL-CODE        PICTURE X(08).
               07  FTA101-ADVNC-ALTRNT-APRVL-CODE PICTURE X(08).
               07  FTA101-ADVNC-APRVL-DATE        PICTURE X(08).
               07  FTA101-ADVNC-APRVL-TIME        PICTURE X(6).
               07  FILLER                         PICTURE X(143).
           05  FTA101-REC-H      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-ADVNC-TEXT              PICTURE X(55).        02410036
               07  FILLER                         PICTURE X(295).       02410036
           05  FTA101-REC-I      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-EXPNS-STATUS            PICTURE X(15).        02410036
               07  FTA101-EXPNS-DATE              PICTURE X(08).
               07  FTA101-EXPNS-AMT               PICTURE S9(10)V99.
               07  FTA101-EXPNS-CNCL-IND          PICTURE X(01).
               07  FTA101-EXPNS-CNCL-DATE         PICTURE X(08).
               07  FTA101-EXPNS-COA-CODE          PICTURE X(01).
               07  FTA101-EXPNS-ACCT-INDX-CODE    PICTURE X(10).
               07  FTA101-EXPNS-FUND-CODE         PICTURE X(06).
               07  FTA101-EXPNS-ORGZN-CODE        PICTURE X(06).
               07  FTA101-EXPNS-ACCT-CODE         PICTURE X(06).
               07  FTA101-EXPNS-PRGRM-CODE        PICTURE X(06).
               07  FTA101-EXPNS-ACTVY-CODE        PICTURE X(06).
               07  FTA101-EXPNS-LCTN-CODE         PICTURE X(06).
               07  FTA101-EXPNS-DSTBN-AMT         PICTURE S9(06)V99.
               07  FTA101-EXPNS-ACCT-ERROR-IND    PICTURE X(01).
               07  FTA101-EXPNS-RULE-CLASS-CODE   PICTURE X(04).
               07  FTA101-EXPNS-CHNG-SEQ-NMBR     PICTURE X(03).
               07  FTA101-EXPNS-APRVL-IND         PICTURE X(01).
               07  FTA101-EXPNS-APRVL-ID          PICTURE X(08).
               07  FTA101-EXPNS-USER-ID           PICTURE X(08).
               07  FTA101-EXPNS-APRVL-TMPLT-CODE  PICTURE X(03).
               07  FTA101-EXPNS-LEVEL-SEQ-NMBR    PICTURE 9(04).
               07  FTA101-EXPNS-APRVL-CODE        PICTURE X(08).
               07  FTA101-EXPNS-ALTRNT-APRVL-CODE PICTURE X(08).
               07  FTA101-EXPNS-APRVL-DATE        PICTURE X(08).
               07  FTA101-EXPNS-APRVL-TIME        PICTURE X(6).
               07  FILLER                         PICTURE X(189).
           05  FTA101-REC-J      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-EXPNS-TEXT              PICTURE X(55).        02410036
               07  FILLER                         PICTURE X(295).       02410036
           05  FTA101-REC-K      REDEFINES FTA101-VARIABLE-DATA.        02410036
               07  FTA101-VNDR-CODE.
                   09  FTA101-VNDR-ID-DIGIT-ONE   PICTURE X(01).
                   09  FTA101-VNDR-ID-LAST-NINE   PICTURE X(09).
               07  FTA101-VNDR-NAME               PICTURE X(35).
               07  FTA101-DRCT-BILL-STATUS        PICTURE X(15).
               07  FTA101-CRDT-MEMO               PICTURE X(01).
               07  FTA101-ACCT-INVD-AMT           PICTURE S9(06)V99.
               07  FTA101-PYMT-DUE-DATE           PICTURE X(08).
               07  FTA101-PAID-IND                PICTURE X(01).
               07  FTA101-TIV-CNCL-IND            PICTURE X(01).
               07  FTA101-TIV-CNCL-DATE           PICTURE X(08).
               07  FTA101-TIV-BILL-TYPE-IND       PICTURE X(01).
               07  FTA101-TIV-ACCT-INVD-AMT       PICTURE S9(06)V99.
               07  FTA101-TIV-BANK-ACCT-CODE      PICTURE X(02).
               07  FTA101-TIV-RULE-CLASS-CODE     PICTURE X(04).
               07  FTA101-TIV-COA-CODE            PICTURE X(01).
               07  FTA101-TIV-ACCT-INDX-CODE      PICTURE X(10).
               07  FTA101-TIV-FUND-CODE           PICTURE X(06).
               07  FTA101-TIV-ORGZN-CODE          PICTURE X(06).
               07  FTA101-TIV-ACCT-CODE           PICTURE X(06).
               07  FTA101-TIV-PRGRM-CODE          PICTURE X(06).
               07  FTA101-TIV-ACTVY-CODE          PICTURE X(06).
               07  FTA101-TIV-LCTN-CODE           PICTURE X(06).
               07  FTA101-TIV-ACCT-ERROR-IND      PICTURE X(01).
               07  FTA101-TIV-CHECK-NMBR          PICTURE X(08).
               07  FTA101-TIV-CHECK-DATE          PICTURE X(08).
               07  FTA101-TIV-CHNG-SEQ-NMBR       PICTURE X(03).
               07  FTA101-TIV-APRVL-IND           PICTURE X(01).
               07  FTA101-TIV-APRVL-ID            PICTURE X(08).
               07  FTA101-TIV-USER-ID             PICTURE X(08).
               07  FTA101-TIV-APRVL-TMPLT-CODE    PICTURE X(03).
               07  FTA101-TIV-LEVEL-SEQ-NMBR      PICTURE 9(04).
               07  FTA101-TIV-APRVL-CODE          PICTURE X(08).
               07  FTA101-TIV-ALTRNT-APRVL-CODE   PICTURE X(08).
               07  FTA101-TIV-APRVL-DATE          PICTURE X(08).
               07  FTA101-TIV-APRVL-TIME          PICTURE X(6).
               07  FILLER                         PICTURE X(127).
       01  FTA101-COUNT                           PICTURE S9(08).       02360036
      *======================= END    FTA101 ======================*    01180010
