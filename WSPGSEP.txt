      *--------------------------------------------------------------*
      *  LAYOUT FOR : WSPGSEP - HOLD AREAS FOR PRINT REQUEST PAGE    *
      *                         SEPARATOR LINE.                      *
      *--------------------------------------------------------------*
       01  WS-PAGE-SEPARATOR-AREAS.
           03  WS-PAGE-SEPARATOR-INX        PICTURE 9(003) VALUE ZERO.
      *
           03  WS-PAGE-SEPARATOR-LINE.
               05  FILLER                   PICTURE X(024) VALUE
                   'NEW REQUEST NEW REQUEST '.
               05  FILLER                   PICTURE X(024) VALUE
                   'NEW REQUEST NEW REQUEST '.
               05  FILLER                   PICTURE X(012) VALUE
                   'NEW REQUEST '.
               05  FILLER                   PICTURE X(004) VALUE SPACES.
               05  WPSL-REQUEST-NUMBER      PICTURE X(002).
               05  FILLER                   PICTURE X(004) VALUE SPACES.
               05  FILLER                   PICTURE X(024) VALUE
                   'NEW REQUEST NEW REQUEST '.
               05  FILLER                   PICTURE X(024) VALUE
                   'NEW REQUEST NEW REQUEST '.
               05  FILLER                   PICTURE X(012) VALUE
                   'NEW REQUEST '.
               05  FILLER                   PICTURE X(002) VALUE SPACES.
      *
