       01  R4063-PAY-EFCTV.
           02  DB-PROC-ID-4063.
               03  USER-CODE-4063                       PIC X(8).
               03  LAST-ACTVY-DATE-4063                 PIC X(5).
               03  TRMNL-ID-4063                        PIC X(8).
               03  PURGE-FLAG-4063                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EFCTV-RCRD-KEY-4063.
               03  START-DATE-4063            COMP-3    PIC S9(8).
               03  TIME-STAMP-4063                      PIC X(6).
           02  END-DATE-4063                  COMP-3    PIC S9(8).
           02  STATUS-4063                              PIC X(1).
           02  PAY-PRCS-IND-4063                        PIC X(1).
           02  PAY-MTHD-DESC-4063                       PIC X(35).
           02  RGN-4063                                 PIC X(4).
           02  PAN-ID-4063                              PIC X(5).
           02  FDRL-EMPLR-ID-4063                       PIC X(9).
           02  PAY-PRD-4063                             PIC X(1).
           02  PIN-4063                                 PIC X(9).
           02  PMS-CODE-4063                            PIC X(15).
           02  ADR-TYPE-CODE-4063                       PIC X(2).
           02  BANK-ACCT-CODE-4063                      PIC X(2).
           02  AGNCY-INTRL-REF-ID-4063        COMP-3    PIC S9(7).
