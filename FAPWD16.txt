      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD16                              04/24/00  12:55  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD16.
           02  SAVE-DBKEY-ID-4166-AP16
                                     COMP   OCCURS 12   PIC S9(8).
           02  START-DATE-AP16                COMP-3    PIC S9(8).
           02  SYSTEM-DATE-AP16               COMP-3    PIC S9(8).
           02  END-DATE-AP16                  COMP-3    PIC S9(8).
           02  ACTN-ERR-FLAG-AP16                       PIC X(1).
