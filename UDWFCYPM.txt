      ****************************************************************  00671002
      *  PARM INPUT DETAILS                                          *  00672002
      *  REVISED:  10/02/96  DEVJWM                                  *  00672002
      ****************************************************************  00673002
       01  WSCR-CONTROL-RECORD-DATA.                                    00674002
           03  WSCR-CONTROL-RECORD.                                     00675002
               05  WSCR-UNVRS-CODE-CARD.                                00676002
                   07  WSCR-UNVRS-CODE-AREA          PIC X(17).         00677002
                       88  UNVRS-LITERAL                  VALUE         00678002
                           '*UNIVERSITY CODE '.                         00679002
                   07  WSCR-UNVRS-CODE               PIC X(02).         00679102
                   07  FILLER                        PIC X(61).         00679202
               05  WSCR-PO-EXTRACT-CARD REDEFINES WSCR-UNVRS-CODE-CARD. 00680502
                   07  WSCR-PO-EXT-AREA              PIC X(22).         00680602
                       88  PO-EXT-LITERAL               VALUE           00680702
                           '*PURCHASING VARIABLES '.                    00680802
                   07  WSCR-PO-EXTRACT.                                 11760000
                       09  WSCR-BUYER-TABLE          PIC X(01).         11760000
                       09  WSCR-SHIP-TO-TABLE        PIC X(01).         11800000
                       09  WSCR-VENDOR-TABLE         PIC X(01).         11840000
                       09  WSCR-PO-HEADER-TABLE      PIC X(01).         11880000
                       09  WSCR-PO-ITEM-TABLE        PIC X(01).         11880000
                    07  FILLER                       PIC X(53).         00681002
               05  WSCR-DATE-RANGE-CARD REDEFINES WSCR-UNVRS-CODE-CARD. 00680502
                   07  WSCR-DT-RAN-AREA              PIC X(12).         00680602
                       88  DT-RANGE-LITERAL               VALUE         00680702
                           '*DATE RANGE '.                              00680802
                   07  WSCR-START-DATE.                                 00681002
                       09  WSCR-START-CC             PIC 9(02).         11880000
                       09  WSCR-START-YY             PIC 9(02).         11880000
                       09  WSCR-START-MM             PIC 9(02).         11880000
                       09  WSCR-START-DD             PIC 9(02).         11880000
                   07  WSCR-START-DATE-RE REDEFINES WSCR-START-DATE.    00681002
                       09  WSCR-START-CC-YY          PIC 9(04).         11880000
                       09  FILLER                    PIC 9(04).         11880000
                   07  FILLER                        PIC X(01).         00681002
                   07  WSCR-END-DATE.                                   00681002
                       09  WSCR-END-CC               PIC 9(02).         11880000
                       09  WSCR-END-YY               PIC 9(02).         11880000
                       09  WSCR-END-MM               PIC 9(02).         11880000
                       09  WSCR-END-DD               PIC 9(02).         11880000
                   07  WSCR-END-DATE-RE REDEFINES WSCR-END-DATE.        00681002
                       09  WSCR-END-CC-YY            PIC 9(04).         11880000
                       09  FILLER                    PIC 9(04).         11880000
                   07  FILLER                        PIC X(01).         00681002
                   07  WSCR-TYPE-RUN                 PIC X(01).         00681002
                   07  FILLER                        PIC X(01).         00681002
                   07  WSCR-START-CCYYDDD-JUL        PIC 9(07).         00681002
                   07  FILLER REDEFINES WSCR-START-CCYYDDD-JUL.         00681002
                       09  WSCR-START-CC-JUL         PIC 9(02).         11880000
                       09  WSCR-START-YYDDD-JUL      PIC 9(05).         11880000
                   07  FILLER                        PIC X(01).         00681002
                   07  WSCR-END-CCYYDDD-JUL          PIC 9(07).         00681002
                   07  FILLER REDEFINES WSCR-END-CCYYDDD-JUL.           00681002
                       09  WSCR-END-CC-JUL           PIC 9(02).         11880000
                       09  WSCR-END-YYDDD-JUL        PIC 9(05).         11880000
                   07  FILLER                        PIC X(01).         00681002
                   07  WSCR-CENTURY-YEAR             PIC X(02).         00681002
                   07  WSCR-CENTURY-YEAR-NUM                            00681002
                        REDEFINES WSCR-CENTURY-YEAR  PIC 9(02).         00681002
                   07  FILLER                        PIC X(30).         00681002
               05  WSCR-TESTING-CARD REDEFINES WSCR-UNVRS-CODE-CARD.    00684102
                   07  WSCR-TESTING-AREA             PIC X(08).         00684202
                       88  TESTING-LITERAL                VALUE         00684302
                           '*TESTING'.                                  00684402
                   07  FILLER                        PIC X(72).         00684502
                                                                        00684602
