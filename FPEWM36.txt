      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FPEWM36                              04/24/00  12:36  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FPEWM36.
           02  ACTN-CODE-GROUP-PE36.
               03  ACTN-CODE-PE36                       PIC X(1).
           02  DCSD-DATE-6117-PE36                      PIC X(6).
           02  DCSD-FLAG-6117-PE36                      PIC X(1).
           02  GNDR-FLAG-6117-PE36                      PIC X(1).
           02  BIRTH-DATE-6117-PE36                     PIC X(6).
           02  NAME-SFX-DESC-6117-PE36                  PIC X(4).
           02  PRSN-FULL-NAME-6117-PE36                 PIC X(55).
           02  NAME-SLTN-DESC-6117-PE36                 PIC X(4).
           02  NAME-KEY-6117-PE36                       PIC X(35).
           02  LONG-DESC-2159-PE36          OCCURS 4    PIC X(30).
           02  ETHNC-CODE-2158-PE36                     PIC X(2).
           02  WORK-PRSN-ID-6117-PE36.
               03  WORK-PRSN-ID-ONE-6117-PE36           PIC X(3).
               03  WORK-PRSN-ID-TWO-6117-PE36           PIC X(2).
               03  WORK-PRSN-ID-THREE-6117-PE36         PIC X(4).
           02  RLGN-CODE-2351-PE36                      PIC X(4).
           02  SPCL-NEED-CODE-2159-PE36     OCCURS 4    PIC X(4).
           02  LONG-DESC-2158-PE36                      PIC X(30).
           02  LONG-DESC-2351-PE36                      PIC X(30).
           02  SHORT-DESC-6117-PE36                     PIC X(10).
           02  SOC-SEC-NO-6117-PE36.
               03  SOC-SEC-NO-ONE-6117-PE36             PIC X(3).
               03  SOC-SEC-NO-TWO-6117-PE36             PIC X(2).
               03  SOC-SEC-NO-THREE-6117-PE36           PIC X(4).
           02  NAME-KEY-6157-PE36                       PIC X(35).
           02  CNFDL-TEXT-PE36                          PIC X(11)
                   VALUE 'CONFID INFO'.
           02  MRTL-CODE-2368-PE36                      PIC X.
           02  LONG-DESC-2368-PE36                      PIC X(30).
           02  PAN-NMBR-6117-PE36                       PIC X(4).
           02  PAN-DESC-PE36                            PIC X(6).
           02  PAN-DATE-PE36                            PIC X(6).
           02  ARCHVD-DATE-6117-PE36                    PIC X(6).
