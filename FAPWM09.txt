      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM09                              04/24/00  12:16  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM09.
           02  VNDR-CODE-4073-AP09 OCCURS 11.
               03  VNDR-ID-DIGIT-ONE-AP09               PIC X(1).
               03  VNDR-ID-LAST-NINE-AP09               PIC X(9).
           02  VNDR-NAME-6311-6117-AP09     OCCURS 11   PIC X(35).
           02  NEXT-SUB-DATE-4163-AP09      OCCURS 11   PIC X(6).
           02  MAX-SUB-4163-AP09            OCCURS 11   PIC X(4).
           02  SUB-TO-DATE-4163-AP09        OCCURS 11   PIC X(4).
           02  ACTN-CODE-GROUP-AP09.
               03  ACTN-CODE-AP09           OCCURS 11   PIC X(1).
