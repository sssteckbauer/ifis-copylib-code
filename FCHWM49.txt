      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM49                              04/24/00  12:25  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM49.
           02  RPRT-CODE-4060-CH49          OCCURS 13   PIC X(8).
           02  RPRT-TITLE-4060-CH49         OCCURS 13   PIC X(60).
           02  NMBR-OF-COPIES-4060-CH49     OCCURS 13   PIC X(3).
           02  RQST-CODE-CH49                           PIC X(8).
           02  ACTN-CODE-GROUP-CH49.
               03  ACTN-CODE-CH49           OCCURS 13   PIC X(1).
