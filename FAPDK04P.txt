      *----------------------------------------------------------------*
      *    LAYOUT FOR: FAPDK04P - PRINT RECORD LAYOUT CREATED BY PRO-  *
      *                           GRAM FAPDK04 AND PASSED TO PROGRAM   *
      *                           FAPPK01S                             *
      *    NOTE:                                                       *
      *        ANY MODIFICATIONS/ADDITIONS TO THIS COPY BOOK MUST TAKE *
      *        PROGRAM FAPDK04, FAPPK01S                               *
      *                                                                *
      *                         INTO CONSIDERATION                     *
      *----------------------------------------------------------------*
      ******************************************************************
      *           MODIFICATION      LOG
      *                                                                *
      ** REF  AUTH    DATE             DESCRIPTION               TSIS  *
      ** ---  ----  --------    -------------------------------  ----- *
      ** 001  MCB   10/04/95    ADDED FAPDK04P-CHECK-NMBR-B SO   AP142 *
      **                        CHK # COULD BE PRINTED ON OVERFLW      *
      ** 002  MLJ   10/18/95    NEW REDEFINES FOR CHECK TEXT     AP045 *
      ** 003  MLJ   01/06/97    ADDED FAPDK04P-CHECK-NMBR-D SO   AP045 *
      **                        CHK# PRINTED WHEN TEXT IS OVERFLOW     *
      ** ---  ----  --------    -------------------------------  ----- *
       01  FAPDK04P-RECORD.                                             00001810
           03  FAPDK04P-REC-TYPE                  PICTURE X(001).       00001820
           03  FAPDK04P-VARIABLE-DATA             PICTURE X(255).       00001830
           03  FAPDK04P-STB-NTRY-B REDEFINES FAPDK04P-VARIABLE-DATA.    00001840
               05  FAPDK04P-INVC-NMBR-B           PICTURE X(008).       00001850
               05  FAPDK04P-DCMNT-TYPE-SEQ-NMBR-B PICTURE 9(004).       00001850
               05  FAPDK04P-INVC-DATE-B           PICTURE X(008).       00001860
               05  FAPDK04P-VNDR-INVC-NMBR-B      PICTURE X(009).       00001860
               05  FAPDK04P-GROSS-AMT-B           PICTURE 9(009)V99.    00001870
               05  FAPDK04P-DSCNT-AMT-B           PICTURE 9(006)V99.    00001880
               05  FAPDK04P-TAX-AMT-B             PICTURE 9(006)V99.    00001890
               05  FAPDK04P-ADDL-CHRG-B           PICTURE 9(006)V99.    00001900
               05  FAPDK04P-NET-AMT-B             PICTURE 9(009)V99.    00001910
               05  FAPDK04P-ADJMT-CODE-B          PICTURE X(002).       00001910
               05  FAPDK04P-SALES-USE-TAX-IND-B   PICTURE X(001).       00001850
               05  FAPDK04P-CHECK-NMBR-B          PICTURE X(008).       00001930
               05  FILLER                         PICTURE X(60).        00001910
           03  FAPDK04P-CHK-NTRY-C REDEFINES FAPDK04P-VARIABLE-DATA.    00001840
               05  FAPDK04P-CHECK-NMBR-C          PICTURE X(008).       00001930
               05  FAPDK04P-CHECK-DATE-C          PICTURE X(008).       00001940
               05  FAPDK04P-CHECK-AMT-C           PICTURE 9(009)V99.    00001950
               05  FAPDK04P-PAYEE-NAME-C          PICTURE X(035).       00001960
               05  FAPDK04P-PAYEE-ADDR1-C         PICTURE X(035).       00001970
               05  FAPDK04P-PAYEE-ADDR2-C         PICTURE X(035).       00001980
               05  FAPDK04P-PAYEE-ADDR3-C         PICTURE X(035).       00001990
               05  FAPDK04P-PAYEE-ADDR4-C         PICTURE X(035).       00002000
               05  FAPDK04P-PAYEE-ADDR5-C         PICTURE X(035).       00002000
               05  FILLER                         PICTURE X(018).       00002000
           03  FAPDK04P-TEXT-D REDEFINES FAPDK04P-VARIABLE-DATA.
               05  FAPDK04P-DCMNT-NMBR-D          PICTURE X(008).
               05  FAPDK04P-TEXT-LINE-1-D         PICTURE X(055).
               05  FAPDK04P-TEXT-LINE-2-D         PICTURE X(055).
               05  FAPDK04P-TEXT-LINE-3-D         PICTURE X(055).
               05  FAPDK04P-TEXT-LINE-4-D         PICTURE X(055).
               05  FAPDK04P-CHECK-NMBR-D          PICTURE X(008).       00001930
               05  FILLER                         PICTURE X(010).
                                                                        00002010
                                                                        00002010
      *-----------------------  END OF FAPDK04P  ----------------------*
                                                                        00002010
