           04  DBLINK-RECORD-KEYS.                                      00010000
           05  DBLINK-R4203-DOCUMENT-KEY.                               00011001
               10  DBLINK-R4203-DOCUMENT-01 PIC S9(4)       COMP-3      00012001
                       VALUE ZERO.                                      00013001
           05  DBLINK-R4215-APRV-TMPLT-KEY.                             00014001
               10  DBLINK-R4215-APRV-TMPLT-01                           00015001
                                            PIC S9(4)       COMP-3      00016001
                       VALUE ZERO.                                      00017001
               10  DBLINK-R4215-APRV-TMPLT-02                           00018001
                                            PIC X(8)        VALUE SPACE.00019001
               10  DBLINK-R4215-APRV-TMPLT-03                           00019101
                                            PIC X(3)        VALUE SPACE.00019201
           05  DBLINK-R4204-APRVL-J-KEY.                                00019301
               10  DBLINK-R4204-APRVL-J-01  PIC S9(4)       COMP-3      00019401
                       VALUE ZERO.                                      00019501
               10  DBLINK-R4204-APRVL-J-02  PIC X(8)        VALUE SPACE.00019601
           05  DBLINK-R4205-APRVL-LVL-KEY.                              00019701
               10  DBLINK-R4205-APRVL-LVL-01                            00019801
                                            PIC S9(4)       COMP-3      00019901
                       VALUE ZERO.                                      00020001
               10  DBLINK-R4205-APRVL-LVL-02                            00020101
                                            PIC X(8)        VALUE SPACE.00020201
               10  DBLINK-R4205-APRVL-LVL-03                            00020301
                                            PIC X(3)        VALUE SPACE.00020401
               10  DBLINK-R4205-APRVL-LVL-04                            00020501
                                            PIC S9(4)       COMP-3      00020601
                       VALUE ZERO.                                      00020701
           05  DBLINK-R4216-APRVL-TRAN-KEY.                             00020801
               10  DBLINK-R4216-APRVL-TRAN-01                           00020901
                                            PIC X(8)        VALUE SPACE.00021001
               10  DBLINK-R4216-APRVL-TRAN-02                           00021101
                                            PIC X(3)        VALUE SPACE.00021201
               10  DBLINK-R4216-APRVL-TRAN-03                           00021301
                                            PIC X(3)        VALUE SPACE.00021401
               10  DBLINK-R4216-APRVL-TRAN-04                           00021501
                                            PIC S9(4)       COMP-3      00021601
                       VALUE ZERO.                                      00021701
               10  DBLINK-R4216-APRVL-TRAN-05                           00021801
                                            PIC X(8)        VALUE SPACE.00021901
           05  DBLINK-R4192-USER-PRFL-KEY.                              00022001
               10  DBLINK-R4192-USER-PRFL-01                            00022101
                                            PIC X(8)        VALUE SPACE.00022201
           05  DBLINK-R4211-APRVL-ALT-KEY.                              00022301
               10  DBLINK-R4211-APRVL-ALT-01                            00022401
                                            PIC S9(4)       COMP-3      00022501
                       VALUE ZERO.                                      00022601
               10  DBLINK-R4211-APRVL-ALT-02                            00022701
                                            PIC X(8)        VALUE SPACE.00022801
               10  DBLINK-R4211-APRVL-ALT-03                            00022901
                                            PIC X(3)        VALUE SPACE.00023001
               10  DBLINK-R4211-APRVL-ALT-04                            00023101
                                            PIC S9(4)       COMP-3      00023201
                       VALUE ZERO.                                      00023301
               10  DBLINK-R4211-APRVL-ALT-05                            00023401
                                            PIC X(26)       VALUE ZERO. 00023501
           04  DBLINK-SET-KEYS.                                         00020000
           05  DBLINK-S-4205-4211-KEY.                                  00030001
             08  DBLINK-S-4205-4211-KEY-O.                              00040001
               10  DBLINK-S-4205-4211-01    PIC S9(4)       COMP-3      00050001
                       VALUE ZERO.                                      00060001
               10  DBLINK-S-4205-4211-02    PIC X(8)        VALUE SPACE.00070001
               10  DBLINK-S-4205-4211-03    PIC X(3)        VALUE SPACE.00080001
               10  DBLINK-S-4205-4211-04    PIC S9(4)       COMP-3      00090001
                       VALUE ZERO.                                      00100001
             08  DBLINK-S-4205-4211-KEY-M.                              00110001
             09  DBLINK-S-4205-4211-KEY-S.                              00120001
               10  DBLINK-S-4205-4211-05    PIC X(26)       VALUE ZERO. 00130001
           05  DBLINK-S-INDX-APRVL-ID-KEY.                              00140001
             08  DBLINK-S-INDX-APRVL-ID-KEY-M.                          00150001
             09  DBLINK-S-INDX-APRVL-ID-KEY-U.                          00160001
               10  DBLINK-S-INDX-APRVL-ID-01                            00170001
                                            PIC X(8)        VALUE SPACE.00180001
               10  DBLINK-S-INDX-APRVL-ID-02                            00190001
                                            PIC X(3)        VALUE SPACE.00200001
             09  DBLINK-S-INDX-APRVL-ID-KEY-S.                          00210001
               10  DBLINK-S-INDX-APRVL-ID-03                            00220001
                                            PIC X(26)       VALUE ZERO. 00230001
           05  DBLINK-S-4203-4204-KEY.                                  00240001
             08  DBLINK-S-4203-4204-KEY-O.                              00250001
               10  DBLINK-S-4203-4204-01    PIC S9(4)       COMP-3      00260001
                       VALUE ZERO.                                      00270001
             08  DBLINK-S-4203-4204-KEY-M.                              00280001
             09  DBLINK-S-4203-4204-KEY-U.                              00290001
               10  DBLINK-S-4203-4204-02    PIC X(8)        VALUE SPACE.00300001
           05  DBLINK-S-4204-4215-KEY.                                  00310001
             08  DBLINK-S-4204-4215-KEY-O.                              00320001
               10  DBLINK-S-4204-4215-01    PIC S9(4)       COMP-3      00330001
                       VALUE ZERO.                                      00340001
               10  DBLINK-S-4204-4215-02    PIC X(8)        VALUE SPACE.00350001
             08  DBLINK-S-4204-4215-KEY-M.                              00360001
             09  DBLINK-S-4204-4215-KEY-U.                              00370001
               10  DBLINK-S-4204-4215-03    PIC X(3)        VALUE SPACE.00380001
           05  DBLINK-S-4215-4205-KEY.                                  00390001
             08  DBLINK-S-4215-4205-KEY-O.                              00400001
               10  DBLINK-S-4215-4205-01    PIC S9(4)       COMP-3      00410001
                       VALUE ZERO.                                      00420001
               10  DBLINK-S-4215-4205-02    PIC X(8)        VALUE SPACE.00430001
               10  DBLINK-S-4215-4205-03    PIC X(3)        VALUE SPACE.00440001
             08  DBLINK-S-4215-4205-KEY-M.                              00450001
             09  DBLINK-S-4215-4205-KEY-U.                              00460001
               10  DBLINK-S-4215-4205-04    PIC S9(4)       COMP-3      00470001
                       VALUE ZERO.                                      00480001
           05  DBLINK-S-INDX-DCMNT-KEY.                                 00490001
             08  DBLINK-S-INDX-DCMNT-KEY-M.                             00500001
             09  DBLINK-S-INDX-DCMNT-KEY-U.                             00510001
               10  FILLER                   PIC X(2)        VALUE SPACE.00520001
               10  DBLINK-S-INDX-DCMNT-01   PIC X(3)        VALUE SPACE.00530001
           05  DBLINK-WS-S-4215-4205-KEY.                               00540001
               10  DBLINK-WS-S-4215-4205-04 PIC 9(4)        VALUE ZERO. 00550001
           05  DBLINK-S-4192-4013-KEY.                                  00560001
             08  DBLINK-S-4192-4013-KEY-O.                              00570001
               10  DBLINK-S-4192-4013-01    PIC X(8)        VALUE SPACE.00580001
             08  DBLINK-S-4192-4013-KEY-M.                              00590001
             09  DBLINK-S-4192-4013-KEY-U.                              00600001
               10  DBLINK-S-4192-4013-02    PIC X(1)        VALUE SPACE.00610001
           05  DBLINK-S-4192-4020-KEY.                                  00620001
             08  DBLINK-S-4192-4020-KEY-O.                              00630001
               10  DBLINK-S-4192-4020-01    PIC X(8)        VALUE SPACE.00640001
             08  DBLINK-S-4192-4020-KEY-M.                              00650001
             09  DBLINK-S-4192-4020-KEY-U.                              00660001
               10  FILLER                   PIC X(1)        VALUE SPACE.00670001
               10  DBLINK-S-4192-4020-02    PIC X(2)        VALUE SPACE.00680001
               10  DBLINK-S-4192-4020-03    PIC X(6)        VALUE SPACE.00690001
           05  DBLINK-S-4192-4204-KEY.                                  00700001
             08  DBLINK-S-4192-4204-KEY-O.                              00710001
               10  DBLINK-S-4192-4204-01    PIC X(8)        VALUE SPACE.00720001
             08  DBLINK-S-4192-4204-KEY-M.                              00730001
             09  DBLINK-S-4192-4204-KEY-S.                              00740001
               10  DBLINK-S-4192-4204-02    PIC X(26)       VALUE ZERO. 00750001
           05  DBLINK-S-INDX-USER-PRFL-KEY.                             00760001
             08  DBLINK-S-INDX-USER-PRFL-KEY-M.                         00770001
             09  DBLINK-S-INDX-USER-PRFL-KEY-U.                         00780001
               10  FILLER                   PIC X(2)        VALUE SPACE.00790001
               10  DBLINK-S-INDX-USER-PRFL-01                           00800001
                                            PIC X(8)        VALUE SPACE.00810001
           05  DBLINK-S-INDX-SEQ-NMBR-KEY.                              00820001
             08  DBLINK-S-INDX-SEQ-NMBR-KEY-M.                          00830001
             09  DBLINK-S-INDX-SEQ-NMBR-KEY-U.                          00840001
               10  FILLER                   PIC X(2)        VALUE SPACE.00850001
               10  DBLINK-S-INDX-SEQ-NMBR-01                            00860001
                                            PIC S9(4) COMP-3 VALUE ZERO.00870001
           05  DBLINK-S-4205-4212-KEY.                                  00880001
             08  DBLINK-S-4205-4212-KEY-O.                              00890001
               10  DBLINK-S-4205-4212-01    PIC S9(4)       COMP-3      00900001
                       VALUE ZERO.                                      00910001
               10  DBLINK-S-4205-4212-02    PIC X(8)        VALUE SPACE.00920001
               10  DBLINK-S-4205-4212-03    PIC X(3)        VALUE SPACE.00930001
               10  DBLINK-S-4205-4212-04    PIC S9(4)       COMP-3      00940001
                       VALUE ZERO.                                      00950001
             08  DBLINK-S-4205-4212-KEY-M.                              00960001
             09  DBLINK-S-4205-4212-KEY-S.                              00970001
               10  DBLINK-S-4205-4212-05    PIC X(26)       VALUE ZERO. 00980001
