      ****************************************************************
      *  THIS IS RECORD LAYOUT FOR SUBCONTRACT JV EXTRACT RECORD
      *  WHICH IS BEING USED BY UCHX020A, UCHX020B, UCHX020C, UCHX020D
      ****************************************************************
        01 WS-EXTR-DETL-TRANS-REC.
            03 EXTR-DCMNT-NMBR-4069         PIC X(08).
            03 EXTR-DATA-AREA.
                05 EXTR-DCMNT-REF-NMBR-4068 PIC X(10).
                05 EXTR-ACTVY-DATE-4068     PIC 9(08).
                05 EXTR-SEQ-NMBR-4068       PIC 9(04).
                05 EXTR-TRANS-AMT           PIC S9(10)V99.
                05 EXTR-DEBIT-CRDT-IND-4068 PIC X(01).
                05 EXTR-ACRL-IND-4068       PIC X(01).
                05 EXTR-JRNL-TYPE-4068      PIC X(04).
                05 EXTR-UNVRS-CODE-4069     PIC X(02).
                05 EXTR-FOAPAL-DATA.
                    07 EXTR-COA-CODE-4068   PIC X(01).
                    07 EXTR-FUND-CODE-4068  PIC X(06).
                    07 EXTR-ORGZN-CODE-4068 PIC X(06).
                    07 EXTR-ACCT-CODE-4068  PIC X(06).
                    07 EXTR-PRGRM-CODE-4068 PIC X(06).
                    07 EXTR-ACTVY-CODE-4068 PIC X(06).
                    07 EXTR-LCTN-CODE-4068  PIC X(06).
                    07 EXTR-ACCT-INDX-CODE-4068 PIC X(10).
                05 EXTR-FSCL-YR-4068        PIC X(02).
                05 EXTR-PSTNG-PRD-4068      PIC X(02).
                05 EXTR-IDC-APPL-AMT-4150   PIC S9(10)V99.
                05 EXTR-IDC-ACTL-AMT-4150   PIC S9(10)V99.
                05 FILER                    PIC X(07).

