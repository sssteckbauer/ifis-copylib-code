      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD63                              04/24/00  12:57  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD63.
           02  WORK-SLCTN-DATE-CH63           COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-CH63          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4001-CH63
                                            OCCURS 11   PIC X(06).
           02  WORK-SAVE-DBKEY-ID-4005-CH63
                                            OCCURS 11   PIC X(24).
           02  WORK-LOOP-FLAG-CH63                      PIC X(1).
           02  WORK-SAVE-DBKEY-CH63           COMP      PIC S9(8).
           02  ACTN-FLAG-CH63                           PIC X(1).
           02  WORK-ADD-FLAG-CH63                       PIC X(1).
           02  WORK-LIST-FLAG-CH63                      PIC X(1).
           02  WORK-SEL-FLAG-CH63                       PIC X(1).
           02  SEL-FLAG-CH63                            PIC X(1).
