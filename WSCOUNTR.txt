      *======================= START  WSCOUNTR ======================*  01180010
       01  COUNTERS-ETC                 COMPUTATIONAL-3.
           03  DB-RECS-INPUT            PICTURE S9(007) VALUE +0.       01990036
           03  DB-RECS-OUTPUT           PICTURE S9(007) VALUE +0.       02000036
           03  DB-RECS-DROPPED          PICTURE S9(007) VALUE +0.       02010036
           03  R6001-UNVRS-CNT          PICTURE S9(007) VALUE +0.       02020036
           03  R-2005-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2009-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2010-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2011-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2012-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2017-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2020-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2025-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2027-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2029-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2030-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2031-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2040-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2047-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2048-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2050-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2051-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2052-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2053-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2054-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2056-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2061-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2062-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2066-CNT               PICTURE S9(007) VALUE +0.
           03  R-2088-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2091-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R-2116-CNT               PICTURE S9(007) VALUE +0.       02030036
           03  R6117-PRSN-CNT           PICTURE S9(007) VALUE +0.       02030036
           03  R-2121-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2122-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2198-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R6343-PARM-CNT           PICTURE S9(007) VALUE +0.       02050036
           03  R-2354-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2356-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2357-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2363-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2367-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2369-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2372-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R-2373-CNT               PICTURE S9(007) VALUE +0.       02050036
           03  R6605-READ-CNT           PICTURE S9(007) VALUE +0.       02200036
           03  R6605-UPD-CNT            PICTURE S9(007) VALUE +0.       02200036
           03  R6606-UPD-CNT            PICTURE S9(007) VALUE +0.       02200036
           03  R6607-UPD-CNT            PICTURE S9(007) VALUE +0.       02200036
           03  RECS-OUTPUT              PICTURE S9(007) VALUE +0.       02300036

      *======================= END    WSCOUNTR ======================*  01180010
