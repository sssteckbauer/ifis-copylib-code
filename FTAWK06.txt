      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWK06                              04/24/00  12:46  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWK06.
           02  TRVL-ACCT-LAST-NINE-4400-TA06            PIC X(9).
           02  TRIP-NMBR-4402-TA06                      PIC X(8).
DEVBWS     02  SAVE-DBKEY-ID-PLUS-ONE-TA06              PIC X(11).
DEVBWS     02  SAVE-DBKEY-ID-MINUS-ONE-TA06             PIC X(11).
