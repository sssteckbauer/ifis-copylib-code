      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FEXWK70                              04/24/00  12:30  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FEXWK70.
           02  CHECK-NMBR-EX70                          PIC X(8).
           02  BANK-ACCT-CODE-EX70                      PIC X(2).
           02  CHECK-DATE-EX70                COMP-3    PIC S9(8).
           02  CHECK-RGSTR-DATE-EX70          COMP-3    PIC S9(8).
           02  CHECK-TYPE-CODE-EX70                     PIC X(1).
           02  CHECK-AMT-EX70                 COMP-3    PIC 9(11)V99.
           02  VNDR-CODE-EX70.
               03  VNDR-ID-DIGIT-ONE-EX70               PIC X(1).
               03  VNDR-ID-LAST-NINE-EX70               PIC X(9).
           02  VNDR-NAME-EX70                           PIC X(35).
           02  CITY-NAME-EX70                           PIC X(18).
           02  STATE-CODE-EX70                          PIC X(2).
           02  ZIP-CODE-EX70                            PIC X(10).
           02  CNCL-IND-EX70                            PIC X(1).
           02  BANK-CODE-DESC-EX70                      PIC X(35).
           02  LONG-DESC-EX70                           PIC X(30).
           02  CHECK-STATUS-EX70                        PIC X(10).
           02  STOP-PAID-IND-EX70                       PIC X(1).
           02  CNCL-DATE-EX70                 COMP-3    PIC S9(8).
           02  ORGN-CODE-EX70                           PIC X(4).
           02  LINE-ADR-EX70                OCCURS 4    PIC X(35).
