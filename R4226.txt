       01  R4226-EXPR-VNDR.
           02  DB-PROC-ID-4226.
               03  USER-CODE-4226                       PIC X(8).
               03  LAST-ACTVY-DATE-4226                 PIC X(5).
               03  TRMNL-ID-4226                        PIC X(8).
               03  PURGE-FLAG-4226                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  EXPR-VNDR-KEY-4226.
               03  UNVRS-CODE-4226                      PIC X(2).
               03  VNDR-CODE-4226.
                   04  VNDR-ID-DIGIT-ONE-4226           PIC X(1).
                   04  VNDR-ID-LAST-NINE-4226           PIC X(9).
           02  LAST-RECON-NMBR-4226                     PIC 9(5).
