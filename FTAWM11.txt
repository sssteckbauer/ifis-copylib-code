      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWM11                              04/24/00  12:47  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWM11.
           02  ACTN-CODE-GROUP-TA11.
               03  ACTN-CODE-TA11           OCCURS 12   PIC X(1).
           02  SHORT-DESC-4407-TA11         OCCURS 12   PIC X(10).
           02  LONG-DESC-4407-TA11          OCCURS 12   PIC X(30).
