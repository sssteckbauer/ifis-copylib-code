      *======================= START  WSBANNER ======================*  01180010
       01  BANNER-PAGE.                                                 02360036
           03  BANNER-INFO.                                             02360036
              05  FILLER                  PICTURE X(14).                02360036
              05  BANNER-RPT-NO           PICTURE X(08).                02370036
              05  FILLER                  PICTURE X(01).                02380036
              05  BANNER-SEQ-NMBR         PICTURE X(04).                02380036
              05  FILLER                  PICTURE X(09).                02380036
              05  BANNER-RPT-TITLE        PICTURE X(60).                02390036
              05  FILLER                  PICTURE X(36).                02390036
           03  BANNER-USER      REDEFINES BANNER-INFO.                  02360036
              05  FILLER                  PICTURE X(14).                02360036
              05  BANNER-RPT-USER         PICTURE X(08).                02370036
              05  FILLER                  PICTURE X(75).                02380036
              05  BANNER-USER-NAME        PICTURE X(35).                02390036
           03  BANNER-LOCATION  REDEFINES BANNER-INFO.                  02360036
              05  FILLER                  PICTURE X(49).                02360036
              05  BANNER-RPT-LOCATION     PICTURE X(35).                02370036
              05  FILLER                  PICTURE X(48).                02390036
           03  BANNER-MSG.                                              02360036
              05  FILLER                  PICTURE X(09) VALUE SPACES.   02380036
              05  FILLER                  PICTURE X(18) VALUE           02360036
                  'NO DATA FOR REPORT'.
              05  FILLER                  PICTURE X(105)  VALUE SPACES. 02380036
       01  WS-BANNER-MISC.                                              02360036
           03  WS-USER-ID                 PICTURE X(08).
           03  WS-SEQ-NMBR                PICTURE X(04).
           03  WS-USER-NAME               PICTURE X(35).
           03  WS-INSTITUTION             PICTURE X(35).
      *======================= END    WSBANNER ======================*  01180010
