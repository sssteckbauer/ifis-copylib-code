       01  R4039-USER-TXT.
           02  DB-PROC-ID-4039.
               03  USER-CODE-4039                       PIC X(8).
               03  LAST-ACTVY-DATE-4039                 PIC X(5).
               03  TRMNL-ID-4039                        PIC X(8).
               03  PURGE-FLAG-4039                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  CMNT-TEXT-4039                           PIC X(55).
