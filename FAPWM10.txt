      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM10                              04/24/00  12:16  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM10.
           02  ACTN-CODE-GROUP-AP10.
               03  ACTN-CODE-AP10           OCCURS 13   PIC X(1).
           02  ADJMT-CODE-4156-AP10         OCCURS 13   PIC X(2).
           02  ADJMT-DESC-4156-AP10         OCCURS 13   PIC X(35).
           02  START-DATE-4156-AP10         OCCURS 13   PIC X(6).
           02  END-DATE-4156-AP10           OCCURS 13   PIC X(6).
           02  ACTVY-DATE-4156-AP10         OCCURS 13   PIC X(6).
