       01  R6350-ENTYENT-CD.
           02  DB-PROC-ID-6350.
               03  USER-CODE-6350                       PIC X(8).
               03  LAST-ACTVY-DATE-6350                 PIC X(5).
               03  TRMNL-ID-6350                        PIC X(8).
               03  PURGE-FLAG-6350                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  ENTY-TYPE-CODE-6350                      PIC X(4).
           02  ENTY-CODE-6350                           PIC X(4).
           02  ENTY-ID-6350.
               03  ENTY-ID-DIGIT-ONE-6350               PIC X(1).
               03  ENTY-ID-LAST-NINE-6350               PIC X(9).
