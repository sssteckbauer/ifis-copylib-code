      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWD67                              04/24/00  12:57  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWD67.
           02  WORK-SLCTN-DATE-CH67           COMP-3    PIC S9(8).
           02  WORK-SYSTEM-DATE-CH67          COMP-3    PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4030-CH67
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-SAVE-DBKEY-ID-4064-CH67
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-LOOP-FLAG-CH67                      PIC X(1).
           02  WORK-SAVE-DBKEY-CH67           COMP      PIC S9(8).
           02  ACTN-FLAG-CH67                           PIC X(1).
           02  WORK-ADD-FLAG-CH67                       PIC X(1).
           02  WORK-LIST-FLAG-CH67                      PIC X(1).
           02  WORK-SEL-FLAG-CH67                       PIC X(1).
           02  SEL-FLAG-CH67                            PIC X(1).
