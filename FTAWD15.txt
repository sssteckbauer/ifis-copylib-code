      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD15                              04/24/00  13:18  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD15.
           02  SAVE-DBKEY-ID-4400-TA15        COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-4404-TA15
                                     COMP   OCCURS 11   PIC S9(8).
           02  WORK-LOOP-FLAG-TA15                      PIC X(1).
           02  ACTN-FLAG-TA15                           PIC X(1).
           02  WORK-ADD-FLAG-TA15                       PIC X(1).
           02  WORK-LIST-FLAG-TA15                      PIC X(1).
           02  WORK-SEL-FLAG-TA15                       PIC X(1).
           02  SEL-FLAG-TA15                            PIC X(1).
           02  SAVE-DBKEY-ID-MINUS-ONE-TA15   COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-PLUS-ONE-TA15    COMP      PIC S9(8).
           02  PAGE-STATUS-CODE-TA15                    PIC X(1).
