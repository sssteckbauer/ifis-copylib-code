      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWD28A                             04/24/00  12:51  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWD28A.
           02  GROSS-AMT-AP28A                COMP-3    PIC S9(10)V99.
           02  TAX-AMT-AP28A                  COMP-3    PIC S9(10)V99.
           02  DSCNT-AMT-AP28A                COMP-3    PIC S9(10)V99.
           02  ADDL-CHRG-AP28A                COMP-3    PIC S9(10)V99.
           02  PAID-AMT-AP28A                 COMP-3    PIC S9(10)V99.
           02  FDRL-WTHHLD-AP28A              COMP-3    PIC 9(6)V99.
           02  STATE-WTHHLD-AP28A             COMP-3    PIC 9(6)V99.
           02  FDRL-WTHHLD-PCT-AP28A          COMP-3    PIC 9(3)V999.
           02  STATE-WTHHLD-PCT-AP28A         COMP-3    PIC 9(3)V999.
           02  UNVRS-CODE-AP28A                         PIC X(2).
           02  CHECK-NMBR-AP28A                         PIC X(8).
           02  BANK-ACCT-CODE-AP28A                     PIC X(2).
           02  VNDR-CODE-AP28A.
               03  VNDR-ID-DIGIT-ONE-AP28A              PIC X(1).
               03  VNDR-ID-LAST-NINE-AP28A              PIC X(9).
           02  VNDR-NAME-AP28A                          PIC X(35).
           02  VNDR-ADDR-AP28A              OCCURS 6    PIC X(35).
           02  RCRD-TYPE-AP28A                          PIC X(1).
           02  VNDR-INV-NMBR-AP28A                      PIC X(9).
           02  VNDR-INV-DATE-AP28A            COMP-3    PIC S9(8).
           02  CHECK-DATE-AP28A               COMP-3    PIC S9(8).
           02  DCMNT-NMBR-AP28A                         PIC X(8).
           02  PRNTR-DEST-CODE-AP28A                    PIC X(8).
           02  ADJMT-CODE-AP28A                         PIC X(2).
           02  TEXT-LINE-AP28A              OCCURS 20   PIC X(55).
