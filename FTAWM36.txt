      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWM36                              04/24/00  12:45  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWM36.
           02  ACTN-CODE-GROUP-TA36.
               03  ACTN-CODE-TA36           OCCURS 11   PIC X(1).
           02  DCMNT-NMBR-TA36              OCCURS 11   PIC X(8).
           02  APRVD-AMT-TA36               OCCURS 11   PIC X(12).
           02  CHECK-AMT-TA36               OCCURS 11   PIC X(12).
           02  PYMT-DUE-DATE-TA36           OCCURS 11   PIC X(6).
           02  APRVL-IND-TA36               OCCURS 11   PIC X(1).
           02  CRDT-MEMO-TA36               OCCURS 11   PIC X(1).
           02  OPEN-PAID-IND-TA36           OCCURS 11   PIC X(1).
           02  VNDR-INV-NMBR-TA36           OCCURS 11   PIC X(9).
           02  CNCL-IND-TA36                OCCURS 11   PIC X(1).
           02  CNCL-CHECK-IND-TA36          OCCURS 11   PIC X(1).
           02  CHECK-NMBR-TA36              OCCURS 11   PIC X(8).
           02  WORK-ALPHA-TA36                          PIC X(10).
DMW001     02  SAVE-DBKEY-ID-TA36           OCCURS 11   PIC X(08).
           02  TA-CRDT-BLNC-IND-TA36                    PIC X(1).
