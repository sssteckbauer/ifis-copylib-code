           04  DBLINK-RECORD-KEYS.                                      00010000
           05  DBLINK-R4067-ACTI-EFCTV-KEY.                             00011002
               10  DBLINK-R4067-ACTI-EFCTV-01                           00012002
                                            PIC X(10)       VALUE SPACE.00013002
               10  DBLINK-R4067-ACTI-EFCTV-02                           00014002
                                            PIC X(10)       VALUE ZERO. 00015002
               10  DBLINK-R4067-ACTI-EFCTV-03                           00016002
                                            PIC X(8)        VALUE ZERO. 00017002
           05  DBLINK-R4005-FUND-EFCTV-KEY.                             00018002
               10  DBLINK-R4005-FUND-EFCTV-01                           00019002
                                            PIC X(6)        VALUE SPACE.00019102
               10  DBLINK-R4005-FUND-EFCTV-02                           00019202
                                            PIC X(10)       VALUE ZERO. 00019302
               10  DBLINK-R4005-FUND-EFCTV-03                           00019402
                                            PIC X(8)        VALUE ZERO. 00019502
           05  DBLINK-R4001-FUND-KEY.                                   00019602
               10  DBLINK-R4001-FUND-01     PIC X(6)        VALUE SPACE.00019702
           05  DBLINK-R4002-ORGN-KEY.                                   00019802
               10  DBLINK-R4002-ORGN-01     PIC X(6)        VALUE SPACE.00019902
           05  DBLINK-R4003-ACCT-KEY.                                   00020002
               10  DBLINK-R4003-ACCT-01     PIC X(6)        VALUE SPACE.00020102
           05  DBLINK-R4004-PROG-KEY.                                   00020202
               10  DBLINK-R4004-PROG-01     PIC X(6)        VALUE SPACE.00020302
           05  DBLINK-R4006-LCTN-KEY.                                   00020402
               10  DBLINK-R4006-LCTN-01     PIC X(6)        VALUE SPACE.00020502
           05  DBLINK-R4007-LCTN-EFCTV-KEY.                             00020602
               10  DBLINK-R4007-LCTN-EFCTV-01                           00020702
                                            PIC X(6)        VALUE SPACE.00020802
               10  DBLINK-R4007-LCTN-EFCTV-02                           00020902
                                            PIC X(10)       VALUE ZERO. 00021002
               10  DBLINK-R4007-LCTN-EFCTV-03                           00021102
                                            PIC X(8)        VALUE ZERO. 00021202
           05  DBLINK-R4008-FNDT-KEY.                                   00021302
               10  DBLINK-R4008-FNDT-01     PIC X(2)        VALUE SPACE.00021402
           05  DBLINK-R4009-FNDT-EFCTV-KEY.                             00021502
               10  DBLINK-R4009-FNDT-EFCTV-01                           00021602
                                            PIC X(2)        VALUE SPACE.00021702
               10  DBLINK-R4009-FNDT-EFCTV-02                           00021802
                                            PIC X(10)       VALUE ZERO. 00021902
               10  DBLINK-R4009-FNDT-EFCTV-03                           00022002
                                            PIC X(8)        VALUE ZERO. 00022102
           05  DBLINK-R4010-ORGN-EFCTV-KEY.                             00022202
               10  DBLINK-R4010-ORGN-EFCTV-01                           00022302
                                            PIC X(6)        VALUE SPACE.00022402
               10  DBLINK-R4010-ORGN-EFCTV-02                           00022502
                                            PIC X(10)       VALUE ZERO. 00022602
               10  DBLINK-R4010-ORGN-EFCTV-03                           00022702
                                            PIC X(8)        VALUE ZERO. 00022802
           05  DBLINK-R4034-ACCT-EFCTV-KEY.                             00022902
               10  DBLINK-R4034-ACCT-EFCTV-01                           00023002
                                            PIC X(6)        VALUE SPACE.00023102
               10  DBLINK-R4034-ACCT-EFCTV-02                           00023202
                                            PIC X(10)       VALUE ZERO. 00023302
               10  DBLINK-R4034-ACCT-EFCTV-03                           00023402
                                            PIC X(8)        VALUE ZERO. 00023502
           05  DBLINK-R4045-ACTI-KEY.                                   00023602
               10  DBLINK-R4045-ACTI-01     PIC X(10)       VALUE SPACE.00023702
           05  DBLINK-R4050-ACTV-KEY.                                   00023802
               10  DBLINK-R4050-ACTV-01     PIC X(6)        VALUE SPACE.00023902
           05  DBLINK-R4066-ACTV-EFCTV-KEY.                             00024002
               10  DBLINK-R4066-ACTV-EFCTV-01                           00024102
                                            PIC X(6)        VALUE SPACE.00024202
               10  DBLINK-R4066-ACTV-EFCTV-02                           00024302
                                            PIC X(10)       VALUE ZERO. 00024402
               10  DBLINK-R4066-ACTV-EFCTV-03                           00024502
                                            PIC X(8)        VALUE ZERO. 00024602
           05  DBLINK-R4105-PROG-EFCTV-KEY.                             00024702
               10  DBLINK-R4105-PROG-EFCTV-01                           00024802
                                            PIC X(6)        VALUE SPACE.00024902
               10  DBLINK-R4105-PROG-EFCTV-02                           00025002
                                            PIC X(10)       VALUE ZERO. 00025102
               10  DBLINK-R4105-PROG-EFCTV-03                           00025202
                                            PIC X(8)        VALUE ZERO. 00025302
           04  DBLINK-SET-KEYS.                                         00020000
           05  DBLINK-S-4001-4005-P-KEY.                                00030001
             08  DBLINK-S-4001-4005-P-KEY-O.                            00040001
               10  DBLINK-S-4001-4005-P-01  PIC X(6)        VALUE SPACE.00050001
             08  DBLINK-S-4001-4005-P-KEY-M.                            00060001
             09  DBLINK-S-4001-4005-P-KEY-U.                            00070001
               10  DBLINK-S-4001-4005-P-02  PIC X(10)       VALUE ZERO. 00080001
               10  DBLINK-S-4001-4005-P-03  PIC X(8)        VALUE ZERO. 00090001
           05  DBLINK-S-4002-4010-P-KEY.                                00100001
             08  DBLINK-S-4002-4010-P-KEY-O.                            00110001
               10  DBLINK-S-4002-4010-P-01  PIC X(6)        VALUE SPACE.00120001
             08  DBLINK-S-4002-4010-P-KEY-M.                            00130001
             09  DBLINK-S-4002-4010-P-KEY-U.                            00140001
               10  DBLINK-S-4002-4010-P-02  PIC X(10)       VALUE ZERO. 00150001
               10  DBLINK-S-4002-4010-P-03  PIC X(8)        VALUE ZERO. 00160001
           05  DBLINK-S-4003-4034-P-KEY.                                00170001
             08  DBLINK-S-4003-4034-P-KEY-O.                            00180001
               10  DBLINK-S-4003-4034-P-01  PIC X(6)        VALUE SPACE.00190001
             08  DBLINK-S-4003-4034-P-KEY-M.                            00200001
             09  DBLINK-S-4003-4034-P-KEY-U.                            00210001
               10  DBLINK-S-4003-4034-P-02  PIC X(10)       VALUE ZERO. 00220001
               10  DBLINK-S-4003-4034-P-03  PIC X(8)        VALUE ZERO. 00230001
           05  DBLINK-S-4004-4105-P-KEY.                                00240001
             08  DBLINK-S-4004-4105-P-KEY-O.                            00250001
               10  DBLINK-S-4004-4105-P-01  PIC X(6)        VALUE SPACE.00260001
             08  DBLINK-S-4004-4105-P-KEY-M.                            00270001
             09  DBLINK-S-4004-4105-P-KEY-U.                            00280001
               10  DBLINK-S-4004-4105-P-02  PIC X(10)       VALUE ZERO. 00290001
               10  DBLINK-S-4004-4105-P-03  PIC X(8)        VALUE ZERO. 00300001
           05  DBLINK-S-4006-4007-P-KEY.                                00310001
             08  DBLINK-S-4006-4007-P-KEY-O.                            00320001
               10  DBLINK-S-4006-4007-P-01  PIC X(6)        VALUE SPACE.00330001
             08  DBLINK-S-4006-4007-P-KEY-M.                            00340001
             09  DBLINK-S-4006-4007-P-KEY-U.                            00350001
               10  DBLINK-S-4006-4007-P-02  PIC X(10)       VALUE ZERO. 00360001
               10  DBLINK-S-4006-4007-P-03  PIC X(8)        VALUE ZERO. 00370001
           05  DBLINK-S-4008-4009-P-KEY.                                00380001
             08  DBLINK-S-4008-4009-P-KEY-O.                            00390001
               10  DBLINK-S-4008-4009-P-01  PIC X(2)        VALUE SPACE.00400001
             08  DBLINK-S-4008-4009-P-KEY-M.                            00410001
             09  DBLINK-S-4008-4009-P-KEY-U.                            00420001
               10  DBLINK-S-4008-4009-P-02  PIC X(10)       VALUE ZERO. 00430001
               10  DBLINK-S-4008-4009-P-03  PIC X(8)        VALUE ZERO. 00440001
           05  DBLINK-S-4045-4067-KEY.                                  00450001
             08  DBLINK-S-4045-4067-KEY-O.                              00460001
               10  DBLINK-S-4045-4067-01    PIC X(10)       VALUE SPACE.00470001
             08  DBLINK-S-4045-4067-KEY-M.                              00480001
             09  DBLINK-S-4045-4067-KEY-U.                              00490001
               10  DBLINK-S-4045-4067-02    PIC X(10)       VALUE ZERO. 00500001
               10  DBLINK-S-4045-4067-03    PIC X(8)        VALUE ZERO. 00510001
           05  DBLINK-S-4050-4066-KEY.                                  00520001
             08  DBLINK-S-4050-4066-KEY-O.                              00530001
               10  DBLINK-S-4050-4066-01    PIC X(6)        VALUE SPACE.00540001
             08  DBLINK-S-4050-4066-KEY-M.                              00550001
             09  DBLINK-S-4050-4066-KEY-U.                              00560001
               10  DBLINK-S-4050-4066-02    PIC X(10)       VALUE ZERO. 00570001
               10  DBLINK-S-4050-4066-03    PIC X(8)        VALUE ZERO. 00580001
           05  DBLINK-WS-S-4045-4067-KEY.                               00590002
               10  DBLINK-WS-S-4045-4067-02 PIC S9(8)       COMP-3      00600002
                       VALUE ZERO.                                      00610002
               10  DBLINK-WS-S-4045-4067-03 PIC X(6)        VALUE SPACE.00620002
           05  DBLINK-WS-S-4001-4005-P-KEY.                             00630002
               10  DBLINK-WS-S-4001-4005-P-02                           00640002
                                            PIC S9(8)       COMP-3      00650002
                       VALUE ZERO.                                      00660002
               10  DBLINK-WS-S-4001-4005-P-03                           00670002
                                            PIC X(6)        VALUE SPACE.00680002
           05  DBLINK-WS-S-4002-4010-P-KEY.                             00690002
               10  DBLINK-WS-S-4002-4010-P-02                           00700002
                                            PIC S9(8)       COMP-3      00710002
                       VALUE ZERO.                                      00720002
               10  DBLINK-WS-S-4002-4010-P-03                           00730002
                                            PIC X(6)        VALUE SPACE.00740002
           05  DBLINK-WS-S-4003-4034-P-KEY.                             00750002
               10  DBLINK-WS-S-4003-4034-P-02                           00760002
                                            PIC S9(8)       COMP-3      00770002
                       VALUE ZERO.                                      00780002
               10  DBLINK-WS-S-4003-4034-P-03                           00790002
                                            PIC X(6)        VALUE SPACE.00800002
           05  DBLINK-WS-S-4004-4105-P-KEY.                             00810002
               10  DBLINK-WS-S-4004-4105-P-02                           00820002
                                            PIC S9(8)       COMP-3      00830002
                       VALUE ZERO.                                      00840002
               10  DBLINK-WS-S-4004-4105-P-03                           00850002
                                            PIC X(6)        VALUE SPACE.00860002
           05  DBLINK-WS-S-4050-4066-KEY.                               00870002
               10  DBLINK-WS-S-4050-4066-02 PIC S9(8)       COMP-3      00880002
                       VALUE ZERO.                                      00890002
               10  DBLINK-WS-S-4050-4066-03 PIC X(6)        VALUE SPACE.00900002
           05  DBLINK-WS-S-4006-4007-P-KEY.                             00910002
               10  DBLINK-WS-S-4006-4007-P-02                           00920002
                                            PIC S9(8)       COMP-3      00930002
                       VALUE ZERO.                                      00940002
               10  DBLINK-WS-S-4006-4007-P-03                           00950002
                                            PIC X(6)        VALUE SPACE.00960002
           05  DBLINK-WS-S-4008-4009-P-KEY.                             00970002
               10  DBLINK-WS-S-4008-4009-P-02                           00980002
                                            PIC S9(8)       COMP-3      00990002
                       VALUE ZERO.                                      01000002
               10  DBLINK-WS-S-4008-4009-P-03                           01010002
                                            PIC X(6)        VALUE SPACE.01020002
           05  DBLINK-S-4008-4005-KEY.                                  01030002
             08  DBLINK-S-4008-4005-KEY-O.                              01040002
               10  DBLINK-S-4008-4005-01    PIC X(2)        VALUE SPACE.01050002
             08  DBLINK-S-4008-4005-KEY-M.                              01060002
             09  DBLINK-S-4008-4005-KEY-U.                              01070002
               10  DBLINK-S-4008-4005-02    PIC X(6)        VALUE SPACE.01080002
               10  DBLINK-S-4008-4005-03    PIC X(10)       VALUE ZERO. 01090002
               10  DBLINK-S-4008-4005-04    PIC X(8)        VALUE ZERO. 01100002
