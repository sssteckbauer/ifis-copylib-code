       01  R4089-BID-DTL.
           02  DB-PROC-ID-4089.
               03  USER-CODE-4089                       PIC X(8).
               03  LAST-ACTVY-DATE-4089                 PIC X(5).
               03  TRMNL-ID-4089                        PIC X(8).
               03  PURGE-FLAG-4089                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  UNVRS-CODE-4089                          PIC X(2).
           02  CMDTY-CODE-4089                          PIC X(8).
           02  CMDTY-DESC-4089                          PIC X(35).
           02  ITEM-NMBR-4089                 COMP-3    PIC 9(4).
           02  UNIT-MEA-CODE-4089                       PIC X(3).
           02  QTY-4089                       COMP-3    PIC S9(6)V99.
           02  AWARD-IND-4089                           PIC X(1).
           02  BID-NMBR-4089                            PIC X(8).
