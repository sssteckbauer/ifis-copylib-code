      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWK16                              04/24/00  12:45  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWK16.
           02  TRVL-ACCT-LAST-NINE-4400-TA16            PIC X(9).
           02  RQST-EVENT-NMBR-TA16                     PIC X(8).
           02  SAVE-DBKEY-ID-MINUS-ONE-TA16             PIC X(8).
           02  SAVE-DBKEY-ID-PLUS-ONE-TA16              PIC X(8).
