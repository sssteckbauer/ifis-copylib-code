      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FTAWD13                              04/24/00  13:19  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FTAWD13.
           02  SAVE-DBKEY-ID-6117-T-TA13      COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6117-V-TA13      COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6157-T-TA13      COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6157-V-TA13      COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6186-T-TA13      COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6186-V-TA13      COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6311-T-TA13      COMP      PIC S9(8).
           02  SAVE-DBKEY-ID-6311-V-TA13      COMP      PIC S9(8).
           02  EDIT-IND-TA13                            PIC X(1).
