       01  STANDARD-RPT-HEADING-2.                                      02
           03  SRH-HEADING-2.                                           02
               05  FILLER                  PIC X(01) VALUE SPACES.
               05  SRH2-LIT-TITLE          PIC X(35) VALUE SPACES.
               05  SRH2-REPORT-TITLE       PIC X(60) VALUE SPACES.
               05  FILLER                  PIC X(21) VALUE SPACES.
               05  FILLER                  PIC X(07) VALUE
                   'PAGE: '.
               05  SRH2-PAGE-NMBR          PIC Z(05).
               05  FILLER                  PIC X(04) VALUE SPACES.
