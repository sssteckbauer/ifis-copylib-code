      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FGAWD25                              04/24/00  13:08  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FGAWD25.
           02  TOTAL-PCT-DR-GA25                        PIC 9(3)V999.
           02  TOTAL-PCT-CR-GA25                        PIC 9(3)V999.
           02  TRANS-DATE-GA25                COMP-3    PIC S9(8).
           02  SET-SORT-KEY-4012-GA25.
               03  OPTN-1-CODE-4012-GA25                PIC X(8).
               03  OPTN-2-CODE-4012-GA25                PIC X(8).
               03  LEVEL-NMBR-4012-GA25                 PIC 9(2).
           02  RQST-FLAG-GA25                           PIC X(1).
           02  SYSTEM-DATE-GA25               COMP-3    PIC S9(8).
           02  CMPLT-IND-GA25                           PIC X(1).
           02  SEQ-NMBR-DOC-GA25                        PIC 9(4).
           02  TOTAL-TRANS-GA25               COMP-3    PIC S9(10)V99.
           02  NO-DETL-FLAG-GA25                        PIC X(1).
           02  TRANS-AMT-GA25                 COMP-3    PIC S9(10)V99.
           02  TOTAL-AMT-CR-GA25              COMP-3    PIC S9(10)V99.
           02  TOTAL-AMT-NET-GA25             COMP-3    PIC S9(10)V99.
           02  TOTAL-AMT-DR-GA25              COMP-3    PIC S9(10)V99.
           02  DSTBN-PCT-GA25                 COMP-3    PIC 9(3)V999.
           02  DCMNT-AMT-GA25                 COMP-3    PIC S9(10)V99.
           02  SET-SORT-KEY-4190-GA25.
               03  ITEM-NMBR-GA25             COMP-3    PIC 9(4).
               03  SEQ-NMBR-GA25                        PIC 9(4).
           02  ACCT-ERROR-IND-GA25                      PIC X(1).
           02  ENCMBRNC-SEQ-NMBR-GA25                   PIC 9(4).
           02  PSTNG-PRD-GA25                           PIC X(2).
           02  ERR-FLAG-GA25                            PIC X(1).
           02  SUSP-FLAG-GA25                           PIC X(1).
           02  MAND-ERR-FLAG-GA25                       PIC X(1).
           02  ACCT-WARNING-IND-GA25                    PIC X(1).
           02  FUND-WARNING-IND-GA25                    PIC X(1).
           02  DTL-ACTV-DATE-4068-GA25        COMP-3    PIC S9(8).
           02  ENPET-DOC-FLAG-GA25                      PIC X(1).
           02  ENPET-MAP-FLAG-GA25                      PIC X(1).
