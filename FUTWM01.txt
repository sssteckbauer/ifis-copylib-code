      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FUTWM01                              04/24/00  13:22  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FUTWM01.
           02  ACTN-CODE-GROUP-UT01.
               03  ACTN-CODE-UT01           OCCURS 13   PIC X(1).
           02  DCMNT-TYPE-CODE-4203-UT01    OCCURS 13   PIC X(3).
           02  DCMNT-TYPE-SEQ-NMBR-4203-UT01
                                            OCCURS 13   PIC X(4).
           02  DCMNT-TYPE-DESC-4203-UT01    OCCURS 13   PIC X(35).
           02  START-DATE-4203-UT01         OCCURS 13   PIC X(6).
           02  END-DATE-4203-UT01           OCCURS 13   PIC X(6).
           02  ACTVY-DATE-4203-UT01         OCCURS 13   PIC X(6).
