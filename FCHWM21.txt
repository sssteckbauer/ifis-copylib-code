      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FCHWM21                              04/24/00  12:23  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FCHWM21.
           02  ACTN-CODE-CH21                           PIC X(1).
           02  XTRNL-ENTY-DESC-4052-CH21                PIC X(35).
           02  INTRL-ENTY-DESC-4053-CH21                PIC X(35).
           02  XTRNL-CODE-DESC-4054-CH21                PIC X(35).
           02  STATUS-DESC-4054-CH21                    PIC X(8).
           02  ACTVY-DATE-4054-CH21                     PIC X(6).
