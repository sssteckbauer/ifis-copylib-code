       01  R4232-AP-TRANS.
           02  DB-PROC-ID-4232.
               03  USER-CODE-4232                       PIC X(8).
               03  LAST-ACTVY-DATE-4232                 PIC X(5).
               03  TRMNL-ID-4232                        PIC X(8).
               03  PURGE-FLAG-4232                      PIC X(1).
               03  FILLER01                             PIC X(3).
           02  AP-KEY-4232.
               03  VNDR-CODE-4232.
                   04  VNDR-ID-DIGIT-ONE-4232           PIC X(1).
                   04  VNDR-ID-LAST-NINE-4232           PIC X(9).
               03  DCMNT-NMBR-4232                      PIC X(8).
           02  ADR-TYPE-CODE-4232                       PIC X(2).
           02  DCMNT-TYPE-SEQ-NMBR-4232                 PIC 9(4).
           02  STMNT-RUN-DATE-4232            COMP-3    PIC S9(8).
           02  COA-CODE-4232                            PIC X(1).
           02  ACCT-INDX-CODE-4232                      PIC X(10).
           02  FUND-CODE-4232                           PIC X(6).
           02  ORGZN-CODE-4232                          PIC X(6).
           02  ACCT-CODE-4232                           PIC X(6).
           02  PRGRM-CODE-4232                          PIC X(6).
           02  ACTVY-CODE-4232                          PIC X(6).
           02  LCTN-CODE-4232                           PIC X(6).
           02  APRVD-AMT-4232                 COMP-3    PIC S9(10)V99.
           02  DSCNT-AMT-4232                 COMP-3    PIC S9(10)V99.
           02  TAX-AMT-4232                   COMP-3    PIC S9(10)V99.
           02  ADDL-CHRG-4232                 COMP-3    PIC S9(10)V99.
           02  RULE-CLASS-CODE-4232                     PIC X(4).
           02  DSCNT-RULE-CLASS-4232                    PIC X(4).
           02  TAX-RULE-CLASS-4232                      PIC X(4).
           02  ADDL-RULE-CLASS-4232                     PIC X(4).
           02  DCMNT-REF-NMBR-4232                      PIC X(10).
           02  CHECK-NMBR-4232                          PIC X(8).
           02  RECON-FILE-NMBR-4232                     PIC 9(5).
           02  RECON-IND-4232                           PIC X(1).
           02  CNCL-DATE-4232                 COMP-3    PIC S9(8).
