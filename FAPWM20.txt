      *---------------------------------------------------------------*
      *                                                               *
      *  RECORD FAPWM20                              04/24/00  12:16  *
      *                                                               *
      *---------------------------------------------------------------*
       01  FAPWM20.
           02  ACTN-CODE-GROUP-AP20.
               03  ACTN-CODE-AP20                       PIC X(1).
           02  VNDR-CODE-4073-AP20                      PIC X(10).
           02  NAME-KEY-6311-R-6117-AP20                PIC X(35).
           02  ADR-TYPE-CODE-4073-AP20                  PIC X(2).
           02  END-DATE-6185-R-6139-AP20                PIC X(6).
           02  PYMT-METHOD-TYP-AP20                     PIC X(4).
           02  LINE-ADR-6185-R-6139-AP20    OCCURS 4    PIC X(35).
           02  CITY-NAME-6185-R-6139-AP20               PIC X(18).
           02  STATE-CODE-6151-AP20                     PIC X(2).
           02  ZIP-CODE-6152-AP20                       PIC X(10).
           02  CNTRY-CODE-6153-AP20                     PIC X(2).
SN6132     02  POSTAL-CODE-AP20                         PIC X(10).
           02  FOREIGN-PROV-CD-AP20                     PIC X(2).
           02  TLPHN-ID-AP20.
               03  BASIC-TLPHN-ID-AP20.
                   04  TLPHN-AREA-CODE-AP20             PIC X(3).
                   04  TLPHN-XCHNG-ID-AP20              PIC X(3).
                   04  TLPHN-SEQ-ID-AP20                PIC X(4).
               03  TLPHN-XTNSN-ID-AP20                  PIC X(4).
           02  FULL-NAME-6153-AP20                      PIC X(35).
           02  FOREIGN-PROV-NAME-AP20                   PIC X(35).
           02  MAP-END-DATE-AP20                        PIC X(6).
DEVMJM* ACR46019
           02  UPDT-USER-ID-AP20                        PIC X(08).
DEVMJM* ACR46019
           02  MAP-START-DATE-AP20                      PIC X(6).
           02  ADR-TYPE-DESC-AP20                       PIC X(30).
           02  FAX-TLPHN-ID-6185-R-6139-AP20.
               03  FAX-AREA-CODE-AP20                   PIC X(3).
               03  FAX-XCHNG-ID-AP20                    PIC X(3).
               03  FAX-SEQ-ID-AP20                      PIC X(4).
           02  EMADR-LINE-6185-R-6139-AP20              PIC X(70).
           02  COMMENT-LINE-AP20                        PIC X(70).
