      ******************************************************************
      *  CREATED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.
      *  COPY MODULE:  WSDATEC2
      *  FORMATS:      PRE-EDIT-DATE (YYMMDD), GREGORIAN-DATE (MM/DD/YY)
      ******************************************************************

           05  PRE-EDIT-DATE.
               10  PRE-EDIT-YY              PIC X(002) VALUE SPACES.
               10  PRE-EDIT-MM              PIC X(002) VALUE SPACES.
               10  PRE-EDIT-DD              PIC X(002) VALUE SPACES.
           05  GREGORIAN-DATE.
               10  GREGORIAN-MM             PIC X(002) VALUE SPACES.
               10  FILLER                   PIC X(001) VALUE '/'.
               10  GREGORIAN-DD             PIC X(002) VALUE SPACES.
               10  FILLER                   PIC X(001) VALUE '/'.
               10  GREGORIAN-YY             PIC X(002) VALUE SPACES.

